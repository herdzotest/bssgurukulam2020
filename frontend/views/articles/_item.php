<?php

use yii\helpers\Html;
?>
<div class="article">
    <img class="alumni-img" src="<?= Yii::$app->homeUrl; ?>theme/img/articles/featuredImage/<?= $model::getImage($model->ar_id); ?>" alt="<?= $model->ar_title; ?>">

    <div class="article-body">
        <h4><?= $model->ar_title; ?></h4>
        <div class="details">
            <div class="author">
                <!-- <img src="img/auth.png" alt=""> -->
                <span class="name">
                    <?= $model->ar_created_by; ?>
                </span>
            </div>
            <div class="date">
                <?=  $model->ar_published_on; ?>
            </div>

        </div>
        <div class="article-footer">
            <a href="<?= Yii::$app->homeUrl; ?>articles/<?= $model->ar_slug; ?>" class="readmore">READ MORE</a>
            <!-- <div class="social">
                <a href="#"><img src="img/facebook.svg" alt=""></a>
                <a href="#"><img src="img/whatsapp.svg" alt=""></a>

            </div> -->
        </div>
    </div>

</div>