<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'BSS Gurukulam Higher Secondary School :: About School';
$this->params['breadcrumbs'][] = $this->title;
?>
<main class="page about-page">
    <section class="title" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/titleweb.jpg);">

        <div class="container">
            <h1><?= $this->title;?></h1>
        </div>

    </section>
    <section class="page-content">



        <div class="container-full">

            <div class="tab-section">
                <?php include('about_side_bar.php');?>
                <div class="tab-content school">


                    <h1>School</h1>

                    <div class="page-row">
                        <div class="page-column column1">
                            <p>
                                B.S.S Gurukulam Higher Secondary School is run with the principles of Brahmananda Swami Sivayogi the Founder of “ Ananda Matham “ - the religion of bliss which advocates the importance of pure mind. Being inspired by this doctrine the ardent disciple Swamy Nirmalananda Yogi – Founded this school in the year 1972 to instill the great values in the minds of the people along with the Quality Education to provide better communal Harmony.

                            </p>
                            <p>
                                Let us introduce ourselves as the proudest members of BSS Group of institutions, a very reputed higher secondary school, established with an aim of creating a paradise on earth by encouraging everyone to develop noble thoughts and emotions which are the most effective means of transformation in human society. Emerging in an individual mind, when shared, they begin to spread and pervade many hearts, minds and intellects. The cohesion and harmony they bring about are verily immense. This is the great message of our Swamiji “MANO JAYA AVA MAHA JAYEVA,AHIMSAIVA PARAMO DARMA”
                            </p>
                        </div>
                        <div class="page-column column2">
                            <p>
                                Our character, behaviour and thoughts, everything should be societal. By considering the entire earth as our family we believe in this great concept: “vasudhaivaku umbakam” a time-tested value of India.
                            </p>
                            <p>
                                A leader is the one who is able to lead the minds and intellects of the others around. So as leaders through our integrity and commitment, we will arouse the individuals in the society, and surely when the individual wakes up and becomes value-oriented, the society gets strengthened.
                            </p>
                            <p>
                                Our school through their highly enthusiastic and sincere efforts with the help of our great dedicated group of teachers and Principal had achieved the best school award in Asia’s biggest cultural event, state youth festival of Kerala for the past five years and our children really proved themselves remarkably in the academic front also.
                            </p>
                        </div>
                        <div class="page-column column3">
                            <div class="ab-image">
                                <div class="ybg"></div>
                                <img src="<?= Yii::$app->homeUrl; ?>theme/img/about-school.png" alt="">

                            </div>
                            <p>
                                We know that the country needs able, honest administrators, whose Supreme fondness for the Nation, and will stand for National Fidelity and integrity. We need intelligent people everywhere – scientists, cultural stalwarts, historians, social reformers, and leaders in different walks of life. We assure you that we will take this great responsibility and feel concerned about it by nurturing all the above noble thoughts and create a new Globe.

                            </p>

                        </div>
                        <div class="page-column column4">
                        </div>

                    </div>


                </div>

            </div>

            <!-- swiper2 -->


        </div>
    </section>
</main>