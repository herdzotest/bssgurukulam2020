<?php

/* @var $this yii\web\View */

use admin\models\HpContents;
use admin\models\Slider;
use admin\models\Events;
use admin\models\Achievements;
use admin\models\Alumni;
use admin\models\AlumniProfile;
use admin\models\AlumniCompany;
use admin\models\UserImages;
use admin\models\Articles;
use admin\models\VisitorComments;

$this->title = 'BSS GURUKULAM HIGHER SECONDARY SCHOOL';
?>
<main>
    <section class="intro">
        <div class="container">
            <div class="intro-content">
                <?php $system_model = HpContents::find()->where(['hc_id' => '1'])->one(); ?>
                <h1><?= $system_model->hc_title; ?></h1>
                <p>
                    <?= $system_model->hc_content; ?>
                </p>
                <br>
                <a href="<?= Yii::$app->homeUrl . 'about/' . $system_model->hc_urls ?>">Read More</a>
            </div>
            <div class="intro-slider">
                <div class="swiper-container inner">
                    <div class="swiper-wrapper">
                        <?php $homeslider_model = Slider::find()->where(['s_status' => '1'])->all(); ?>
                        <?php foreach ($homeslider_model as $key => $slider) { ?>

                            <div class="swiper-slide" style="background-image: url(<?= Yii::$app->homeUrl ?>theme/img/main-slider/<?= $slider['s_image']; ?>)">
                            </div>
                        <?php } ?>
                    </div>

                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>


                <div class="intro-bg"></div>

            </div>
        </div>
    </section>
    <section class="numbers">
        <div class="container">
            <div class="number-content">
                <div class="number">
                    <div>
                        <img src="<?= Yii::$app->homeUrl; ?>theme/img/cal.png" alt="">
                    </div>
                    <p>
                        45+<br>
                        Years of<br>
                        Experience
                    </p>
                </div>
                <div class="number">
                    <div>
                        <img src="<?= Yii::$app->homeUrl; ?>theme/img/staf.png" alt="">
                    </div>
                    <p>
                        60+<br>
                        Amazing<br>
                        Teachers
                    </p>
                </div>
                <div class="number">
                    <div>
                        <img src="<?= Yii::$app->homeUrl; ?>theme/img/stu.png" alt="">
                    </div>
                    <p>
                        2000+<br>
                        Students
                    </p>
                </div>
                <div class="number">
                    <div>
                        <img src="<?= Yii::$app->homeUrl; ?>theme/img/awa.png" alt="">
                    </div>
                    <p>
                        12<br>
                        Awards<br>
                        Won
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="tiles">
        <div class="tiles-bg">

        </div>
        <div class="container">
            <div class="tiles-content">
                <div class="left">
                    <div class="mission">
                        <div class="mission-head">
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/target.svg" alt="">
                            <h1>Mission</h1>

                        </div>
                        <p>
                            To be the <span>best educational institution</span> in our country
                        </p>

                    </div>

                </div>
                <div class="right">

                    <div class="princi">
                        <?php $princi_model = HpContents::find()->where(['hc_id' => '2'])->one(); ?>
                        <img src="<?= Yii::$app->homeUrl . 'theme/img/hc_images/' . $princi_model->hc_image; ?>" alt="Bss Gurukualam Principal">
                        <div class="tile-body">
                            <h3><?= $princi_model->hc_title; ?></h3>

                            <p>
                                <?php
                                if (strlen($princi_model->hc_content) > 50) {
                                    $str = substr($princi_model->hc_content, 0, 90) . '...';
                                    echo $str;
                                }
                                ?>

                            </p>
                            <a href="<?= Yii::$app->homeUrl . 'about/' . $princi_model->hc_urls; ?>">Read More</a>
                        </div>

                    </div>

                    <div class="founder">
                        <?php $founder_model = HpContents::find()->where(['hc_id' => '3'])->one(); ?>
                        <img src="<?= Yii::$app->homeUrl . 'theme/img/hc_images/' . $founder_model->hc_image; ?>" alt="Bss Gurukualam Founder">
                        <div class="tile-body">
                            <h3><?= $founder_model->hc_title; ?></h3>

                            <p>
                                <?php
                                if (strlen($founder_model->hc_content) > 50) {
                                    $str = substr($founder_model->hc_content, 0, 90) . '...';
                                    echo $str;
                                }
                                ?>

                            </p>
                            <a href="<?= Yii::$app->homeUrl . 'about/' . $founder_model->hc_urls; ?>">Read More</a>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="about">

        <div class="container">
            <div class="about-content">
                <div class="about-slider">
                    <?php $about_model = HpContents::find()->where(['hc_id' => '4'])->one(); ?>
                    <img src="<?= Yii::$app->homeUrl . 'theme/img/hc_images/' . $about_model->hc_image; ?>" alt="BSS GURUKUALAM SCHOOL">

                </div>
                <div class="about-body">
                    <h2 class="fph2"><?= $about_model->hc_title ?></h2>
                    <p>
                        <?php
                        if (strlen($about_model->hc_content) > 50) {
                            $str = substr($about_model->hc_content, 0, 550) . '...';
                            echo $str;
                        }
                        ?>
                    </p>
                    <h4>Accreditations</h4>
                    <div class="credits">
                        <div class="cr1">
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/BritishCouncilCol.jpg" style="width: 15em">
                        </div>
                        <div class="cr1">
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/isologo.png" style="margin-top: 15px;width: 15em" width="100px">
                        </div>
                    </div>
                    <a href="<?= Yii::$app->homeUrl . $about_model->hc_urls; ?>" class="readmore">Know More</a>
                </div>
            </div>
        </div>
    </section>
    <section class="charity">
        <div class="container">
            <div class="charity-content">
                <div class="charity-slider">
                    <div class="bg"></div>
                    <?php $charity_model = HpContents::find()->where(['hc_id' => '5'])->one(); ?>
                    <img src="<?= Yii::$app->homeUrl; ?>theme/img/hc_images/<?= $charity_model->hc_image; ?>" alt="BSS Gurukualam Higher Secondary School | Charity">
                </div>
                <div class="charity-body">
                    <h2 class="fph2"><?= $charity_model->hc_title; ?></h2>
                    <p>
                        <?php
                        /*if (strlen($charity_model->hc_content) > 50) {
                            $str = substr($charity_model->hc_content, 0, 350) . '...';
                            echo $str;
                        } */
                        echo $charity_model->hc_content;
                        ?>
                    </p>
                    <a href="<?= Yii::$app->homeUrl . $charity_model->hc_urls; ?>" class="readmore">Know More</a>
                </div>
            </div>
        </div>
    </section>

    <section class="events">
        <div class="container">
            <h2>Events</h2>
            <?php
            $event_model = Events::find()->orderBy(['ev_date' => SORT_DESC])->limit(4)->all();
            if (!empty($event_model)) { ?>
                <div class="events-content">
                    <?php foreach ($event_model as $key => $value) {
                        $event_date = date('d-M-Y', strtotime($value->ev_date));
                    ?>
                        <div class="event">
                            <div class="date"><?= $event_date; ?></div>
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/events/featuredImages/<?= $value->ev_featuredImg; ?>" alt="" style="width: 231px;height: 280px;">
                            <h3><a href="<?= Yii::$app->homeUrl; ?>events/<?= $value->ev_slug; ?>"><?= $value->ev_title; ?></a></h3>
                        </div>
                    <?php } ?>
                </div>
                <a href="<?= Yii::$app->homeUrl; ?>events" class="readmore">Know More</a>
            <?php } else { ?>
                <div class="events-content"> No events to display... </div>
            <?php } ?>
        </div>
    </section>
    <section class="achievements">
        <div class="container">
            <h2>Achievements</h2>
            <?php
            $achv_model = Achievements::find()->orderBy(['av_date' => SORT_DESC])->limit(4)->all();
            if (!empty($achv_model)) { ?>
                <div class="ach-content">
                    <?php foreach ($achv_model as $key => $value) {
                        $achv_date = date('d-M-Y', strtotime($value->av_date));
                    ?>
                        <div class="ach">
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/achievements/featuredImages/<?= $value->av_featuredImage; ?>" alt="">
                            <!-- <h4><a href="<?= Yii::$app->homeUrl; ?>achievements/<?= $value->av_slug; ?>"><?= $value->av_title; ?></a></h4> -->
                            <p>
                                <?php $ach_desc = substr($value->av_desc, 0, 30) . '...';
                                echo $ach_desc;
                                ?>
                            </p>
                        </div>
                    <?php } ?>
                </div>
                <a href="<?= Yii::$app->homeUrl; ?>achievements" class="readmore">Know More</a>
            <?php } else { ?>
                <div class="ach-content"> No events to display... </div>
            <?php } ?>
        </div>
    </section>

    <section class="articles">
        <div class="container">
            <h2>Articles </h2>
            <?php
            $article_model = Articles::find()->orderBy(['ar_published_on' => SORT_DESC])->limit(3)->all();
            if (!empty($article_model)) { ?>
                <div class="articles-content">
                    <?php foreach ($article_model as $key => $value) {
                        $article_date = date('d-M-Y', strtotime($value->ar_published_on));
                    ?>
                        <div class="article">
                            <img class="article-img" src="<?= Yii::$app->homeUrl; ?>theme/img/articles/featuredImage/<?= $value->ar_featured_image; ?>" alt="">
                            <div class="article-body">
                                <h4>
                                    <?= $value->ar_title; ?>
                                </h4>
                                <div class="details">
                                    <div class="author">
                                        <span class="name">
                                        </span>
                                    </div>
                                    <div class="date">
                                        <?= $article_date; ?>
                                    </div>
                                </div>
                                <div class="article-footer">
                                    <a href="<?= Yii::$app->homeUrl; ?>articles/<?= $value->ar_slug; ?>">Know More</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <a href="<?= Yii::$app->homeUrl; ?>articles" class="readmore">View More</a>
            <?php } else { ?>
                <div class="articles-content"> No articles to display ... </div>
            <?php } ?>
        </div>
    </section>

    <section class="alumni">
        <div class="albg">
        </div>
        <div class="container">
            <?php
            $alumni_model = Alumni::find()->where(['role' => '40'])->orderBy(['id' => SORT_DESC])->limit(9)->all();
            if (!empty($alumni_model)) { ?>
                <h2>Alumni</h2>
                <div class="swiper-container swiper-container1">
                    <div class="swiper-wrapper">
                        <?php foreach ($alumni_model as $key => $value) { ?>
                            <div class="swiper-slide">
                                <?php $alumni_batch = AlumniProfile::find()->where(['ap_uid' => $value->id])->one();
                                $batch_from = substr($alumni_batch->ap_batch_from, 0, 4);
                                $batch_to = substr($alumni_batch->ap_batch_to, 0, 4);
                                $alumni_image = UserImages::find()->where(['pp_uid' => $value->id])->one();
                                $alumni_company = AlumniCompany::find()->where(['ac_uid' => $value->id])->one();
                                ?>
                                <img src="<?= Yii::$app->homeUrl; ?>theme/img/pro_images/alumni/<?= $alumni_image->pp_image ?>" width="200px" height="200px" alt="">
                                <h4><?= $value->first_name . ' ' . $value->second_name ?></h4>
                                <p><?= $batch_from . ' to ' . $batch_to ?> Batch</p>
                                <i><?= $alumni_company->ac_postion . ' at ' . $alumni_company->ac_name ?></i>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <a href="<?= Yii::$app->homeUrl; ?>alumni" class="readmore">See More</a>
            <?php } else { ?>
                <div class="swiper-container swiper-container1"> No alumnis to display... </div>
            <?php } ?>
        </div>
    </section>
    <section class="contact">
        <div class="container">
            <div class="contact-content">
                <div class="contact-body">
                    <h2>Contact Us</h2>
                    <div>
                        <p>Feel Free to call us at</p>
                        <h3> +91 9746 333 222</h3>
                    </div>
                    <div>
                        <p>Or send a email</p>
                        <a href="" class="sendmail">SEND MAIL</a>
                    </div>


                </div>
                <div class="map">
                    <div class="mapbg"></div>

                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4663.01851213046!2d76.55676270652721!3d10.648130463712388!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xffd8a71f7bafddef!2sBSS%20Gurukulam%20Higher%20Secondary%20School!5e0!3m2!1sen!2sin!4v1572663796176!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </section>
    <section class="visitor-comments">
        <div class="container">
            <div class="vc-content">
                <h2>Visitor Comments</h2>
                <?php
                $visitors_model = VisitorComments::find()->orderBy(['vc_created_on' => SORT_DESC])->all();
                if (!empty($visitors_model)) { ?>
                    <div class="vc-slider">
                        <div class="swiper-wrapper">
                            <?php foreach ($visitors_model as $key => $value) {
                                $visitor_comment_date = date('d-M-Y', strtotime($value->vc_created_on));
                            ?>
                                <div class="swiper-slide">
                                    <img src="<?= Yii::$app->homeUrl; ?>theme/img/<?= $value->vc_image; ?>" alt="">
                                    <p><?= $value->vc_comment; ?></p>
                                    <h4><?= $value->vc_name; ?></h4>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                <?php } else { ?>
                    <div class="vc-slider"> No visitor comments to display ... </div>
                <?php } ?>
            </div>
        </div>
    </section>
</main>