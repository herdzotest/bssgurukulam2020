<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<main class="page con-page">
    <?php include('banner.php'); ?>
    <section class="page-content">
        <div class="pmap">

            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4663.01851213046!2d76.55676270652721!3d10.648130463712388!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xffd8a71f7bafddef!2sBSS%20Gurukulam%20Higher%20Secondary%20School!5e0!3m2!1sen!2sin!4v1572663796176!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

        </div>
        <div class="cform">

            <div class="container">
                <div class="formwrap">
                    <h2>Contact Form</h2>

                    <p>
                        If you have any queries , please fill out the following form to contact us. Thank you.
                    </p>

                    <div class="row">
                        <div class="col-lg-12">
                            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'class' => 'form-control', 'placeholder' => 'Please Enter Your Full Name'])->label(false) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'email')->input('email', ['class' => 'form-control', 'placeholder' => 'Enter Your Email Id'])->label(false) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'subject')->textInput(['class' => 'form-control', 'placeholder' => 'Please Enter The Subject Line'])->label(false) ?>
                                </div>
                                <div class="col-md-12">
                                    <?= $form->field($model, 'body')->textarea(['rows' => 6, 'placeholder' => 'Enter Your Message'])->label(false) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::class, [
                                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                                    ])->hint('Please click the image to refresh the verification code') ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>


                </div>
            </div>

        </div>
        </div>
    </section>

</main>