<?php

use yii\helpers\Html;
?>
<div class="charity-item">
    
<img class="alumni-img" src="<?= Yii::$app->homeUrl;?>theme/img/achievements/featuredImages/<?= $model::getImage($model->av_id);?>" alt="<?= $model->av_title;?>">
    <h3><?= $model->av_title;?></h3>
    <div>
        <a href="<?= Yii::$app->homeUrl;?>achievements/<?=$model->av_slug;?>" class="readmore">READ MORE</a>
    </div>
</div>
