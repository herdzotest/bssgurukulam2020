<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="apple-touch-icon" href="<?= Yii::$app->homeUrl; ?>theme/img/fav_icon.png">
    <link rel="shortcut icon" href="<?= Yii::$app->homeUrl; ?>theme/img/fav_icon.png">
    <link rel="stylesheet" href="<?= Yii::$app->homeUrl; ?>theme/css/reset.css">
    <link rel="stylesheet" href="<?= Yii::$app->homeUrl; ?>theme/css/swiper.min.css">
    <link rel="stylesheet" href="<?= Yii::$app->homeUrl; ?>theme/css/main.css">
    <link rel="stylesheet" href="<?= Yii::$app->homeUrl; ?>theme/css/responsive.css">
    <link rel="stylesheet" href="<?= Yii::$app->homeUrl; ?>theme/css/flexboxgrid.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrapper">
        <header>
            <div class="header-wrap">

                <div class="container">
                    <nav>
                        <div class="logo">
                            <a href="<?= Yii::$app->homeUrl; ?>">
                                <img src="<?= Yii::$app->homeUrl; ?>theme/img/logo.png" alt="">
                            </a>
                        </div>
                        <div class="menuwrap">
                            <div class="menubtn">
                                <div class="l1"></div>
                                <div class="l2"></div>
                            </div>
                            <?php
                            $active = 'active';
                            ?>
                            <ul>
                                <li><a class="<?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') {
                                                    echo 'active';
                                                } ?>" href="<?= Yii::$app->homeUrl; ?>">Home</a></li>
                                <li><a class="<?php if (Yii::$app->controller->id == 'about') {
                                                    echo 'active';
                                                } ?>" href="<?= Yii::$app->homeUrl; ?>about-us">About</a></li>
                                <li><a class="<?php if (Yii::$app->controller->id == 'faculty') {
                                                    echo 'active';
                                                } ?>" href="<?= Yii::$app->homeUrl; ?>faculty">Faculty</a></li>
                                <li><a class="<?php if (Yii::$app->controller->id == 'facilities') {
                                                    echo 'active';
                                                } ?>" href="<?= Yii::$app->homeUrl; ?>facilities">Facilities</a></li>
                                <li><a class="<?php if (Yii::$app->controller->action->id == 'contact') {
                                                    echo 'active';
                                                } ?>" href="<?= Yii::$app->homeUrl; ?>contact-us">Contact Us</a></li>

                                <li><a class="<?php if (Yii::$app->controller->id == 'charity' || Yii::$app->controller->id == 'events' || Yii::$app->controller->id == 'achievements' || Yii::$app->controller->id == 'articles') {
                                                    echo 'active';
                                                } ?>" href="<?= Yii::$app->homeUrl; ?>charity">Life @ BSS</a></li>
                                <li><a class="<?php if (Yii::$app->controller->id == 'alumni') {
                                                    echo 'active';
                                                } ?>" href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a></li>
                                <li><a class="<?php if (Yii::$app->controller->id == 'students/new') {
                                                    echo 'active';
                                                } ?>" href="<?= Yii::$app->homeUrl; ?>applications">Admission</a></li>
                            </ul>
                        </div>

                    </nav>
                </div>
            </div>

        </header>

        <!-- <div class="container"> -->
        <?= $content ?>
        <!-- </div> -->
    </div>

    <footer>
        <div class="container">

            <div class="footer-content">

                <div class="footer-column">
                    <img src="<?= Yii::$app->homeUrl; ?>theme/img/footer-logo.png" alt="">
                    <p>
                        B.S.S Gurukulam Higher Secondary School is run with the principles of Brahmananda Swami Sivayogi the Founder of
                    </p>
                </div>
                <div class="footer-column  ql-column">
                    <h4>QUICK LINKS</h4>
                    <div class="quicklinks">
                        <ul>
                            <li><a href="<?= Yii::$app->homeUrl; ?>">Home</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>about-us">About</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>faculty">Faculty</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>facilities">Facility</a></li>
                            <!--<li><a href="#">Admission</a></li>-->
                            <li><a href="<?= Yii::$app->homeUrl; ?>contact-us">Contact</a></li>


                        </ul>
                        <ul>
                            <li><a href="<?= Yii::$app->homeUrl; ?>achievements">Achievements</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>articles">Articles</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>events">Events</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>charity">Charity</a></li>
                            <li><a href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a></li>

                        </ul>
                    </div>


                </div>
                <div class="footer-column address">
                    <h4>CONTACT</h4>

                    <div class="address-line">
                        <div>
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/location.png" alt="">

                        </div>
                        <address>
                            Thrippalur, Alathur, Palakkad, Kerala.
                            678541
                        </address>
                    </div>

                    <div class="address-line">
                        <div>
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/phone-icon.png" alt="">

                        </div>
                        <p>
                            Main: +91 492 2222315 <br>
                            Office: +91 9447618790
                        </p>
                    </div>

                    <div class="address-line">
                        <div>
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/mail-icon.png" alt="">

                        </div>
                        <p>
                            bssgurukulam.12@gmail.com

                        </p>

                    </div>
                </div>
            </div>
        </div>
        <section>
            <div class="container">
                <div class="copyright">
                    <p>

                        Copyright 2020 BSS Gurukulam Higher Secondary School. All rights reserved
                    </p>
                    <p>


                        Powered by <a href="herdzo.com">Herdzo Innovations Pvt Ltd.</a>

                    </p>
                </div>
            </div>
        </section>
    </footer>
    <script src="<?= Yii::$app->homeUrl; ?>theme/js/swiper.min.js"></script>
    <script src="<?= Yii::$app->homeUrl; ?>theme/js/main.js"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script>
    <script>
        var swiper2 = new Swiper('.inner', {
            // slidesPerView: 1,
            spaceBetween: 30,
            loop: true,
            autoplay: {
                delay: 3000,
            },


            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });

        var alswiper = new Swiper('.swiper-container1', {

            pagination: {
                el: '.swiper-pagination-01',
                clickable: true,
            },
            slidesPerView: 3,
            slidesPerGroup: 3,
            loop: true,
            loopFillGroupWithBlank: true,
            breakpoints: {
                960: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                },
                768: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                },
            },
        });


        var swiper3 = new Swiper('.vc-slider', {
            slidesPerView: 1,
            centeredSlides: true,
            loop: true,
            //   spaceBetween: 30,
            grabCursor: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        });

        // dynamic form
        $(document).ready(function() {
            $("#student_bs_details").hide();
        });
        $("#admissionacademics-student_brother_sisters").change(function() {
            var bscondition = $('#admissionacademics-student_brother_sisters').val();
            if (bscondition == 'yes') {
                $("#student_bs_details").show();
            } else {
                $("#student_bs_details").hide();
            }

        });

        function addRow(tableID) {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
            if (rowCount < 3) { // limit the user from creating fields more than your limits
                var row = table.insertRow(rowCount);
                var colCount = table.rows[0].cells.length;
                for (var i = 0; i < colCount; i++) {
                    var newcell = row.insertCell(i);
                    newcell.innerHTML = table.rows[0].cells[i].innerHTML;
                }
            } else {
                alert("Maximum Student is 3");

            }
        }

        function deleteRow(tableID) {
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
            for (var i = 0; i < rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if (rowCount <= 1) { // limit the user from removing all the fields
                    alert("Please provide atleast one student details");
                    break;
                }
                table.deleteRow(i);
                rowCount--;
                i--;
            }
        }
    </script>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>