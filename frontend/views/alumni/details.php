<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\Alumni */

$this->title = $model->first_name . ' ' . $model->second_name;
$this->params['breadcrumbs'][] = ['label' => 'Alumnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<main class="page ">
    <?php include('banner.php'); ?>
    <section class="page-content charity-single">


        <div class="container-full">
            <div class="tab-section">
                <div class="tab-nav">
                    <div class="tab-nav">
                        <ul class="tabul">
                            <li>
                                <a href="<?= Yii::$app->homeUrl; ?>charity">Charity</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->homeUrl; ?>events">Events</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->homeUrl; ?>articles">Articles</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->homeUrl; ?>achievements">Achievements</a>
                            </li>
                            <li>
                                <a class="active" href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a>
                            </li>



                        </ul>
                    </div>
                    <div class="mob-tab-nav tabswiper">
                        <ul class="tabul swiper-wrapper labmm">
                            <li class="swiper-slide">
                                <a href="<?= Yii::$app->homeUrl; ?>charity">Charity</a>
                            </li>
                            <li class="swiper-slide">
                                <a href="events.html">Events</a>
                            </li>
                            <li class="swiper-slide">
                                <a href="<?= Yii::$app->homeUrl; ?>articles">Articles</a>
                            </li>
                            <li class="swiper-slide">
                                <a href="<?= Yii::$app->homeUrl; ?>achievements">Achievements</a>
                            </li>
                            <li class="swiper-slide">
                                <a class="active" href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a>
                            </li>
                            <li class="swiper-slide dummyli"></li>

                        </ul>
                    </div>
                </div>
                <div class="mob-tab-nav tabswiper">
                    <ul class="tabul swiper-wrapper labmm">
                        <!-- <li class="swiper-slide">
                            <a href="life-at-bss.html">Charity</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="events.html">Events</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="articles.html">Articles</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="achievements.html">Achievements</a>
                        </li> -->
                        <li class="swiper-slide">
                            <a class="active" href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a>
                        </li>
                        <li class="swiper-slide dummyli"></li>

                    </ul>
                </div>
                <div class="tab-content alumni1 ">

                    <div class="row alumnisingle">
                        <div class="head">
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/pro_images/alumni/<?= $model::getImage($model->id); ?>" alt="<?= $model->first_name . '-' . $model->second_name; ?>">
                            <h3><?= $model->first_name . ' ' . $model->second_name; ?></h3>
                        </div>
                        <div class="alumni-content">
                            <?php
                            $start_batch = $model->profile['ap_batch_from'];
                            $start_batch = date('Y', strtotime($start_batch));
                            $end_batch = $model->profile['ap_batch_to'];
                            $end_batch = date('Y', strtotime($end_batch));
                            ?>
                            <table>
                                <tr>
                                    <td>Batch</td>
                                    <td><?= $start_batch . '-' . $end_batch ?></td>
                                </tr>
                                <tr>
                                    <td>Qualification</td>
                                    <td><?= $model->profile['ap_qualification']; ?></td>
                                </tr>
                                <tr>
                                    <td>Current Postion</td>
                                    <td><?= $model->company['ac_postion']; ?></td>
                                </tr>
                                <tr>
                                    <td>Current Company</td>
                                    <td><?= $model->company['ac_name']; ?></td>
                                </tr>
                                <tr>
                                    <td>Joined On</td>
                                    <td><?= date('d-m-Y', strtotime($model->company['ac_joined_date']));  ?></td>
                                </tr>
                                </tr>
                                <tr>
                                    <td>Current Location</td>
                                    <td><?= $model->company['ac_location']; ?></td>
                                </tr>
                                <tr>
                                    <td>Facebook ID</td>
                                    <td><?= ($model->profile['ap_facebook_link'] != '')  ?  $model->profile['ap_facebook_link'] :  'Not Available'; ?></td>
                                </tr>
                                <tr>
                                    <td>LinkedIn ID</td>
                                    <td><?= ($model->profile['ap_linkedin_link'] != '')  ?  $model->profile['ap_linkedin_link'] :  'Not Available'; ?></td>
                                </tr>
                                <tr>
                                    <td>Twitter ID</td>
                                    <td><?= ($model->profile['ap_twitter_link'] != '')  ?  $model->profile['ap_twitter_link'] :  'Not Available'; ?></td>
                                </tr>
                            </table>
                        </div>

                    </div>


                </div>




            </div>


    </section>
</main>