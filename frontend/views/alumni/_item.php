<?php

use yii\helpers\Html;
?>
<div class="alumni-item">
    <img class="alumni-img" src="<?= Yii::$app->homeUrl;?>theme/img/pro_images/alumni/<?= $model::getImage($model->id);?>" alt="<?= $model->first_name;?>">
    <h3><?= $model->first_name.' '.$model->second_name;?></h3>
    <p>
    <?php 
    $start_batch = $model->profile['ap_batch_from'];
    $start_batch = date('Y', strtotime($start_batch));
    $end_batch = $model->profile['ap_batch_to'];
    $end_batch = date('Y', strtotime($end_batch));
    ?>    
    <?= $start_batch.'-'.$end_batch?></p>
    <p><?= $model->company['ac_postion'].' at '.$model->company['ac_name'];?></p>
    <a href="<?= Yii::$app->homeUrl;?>alumni/details/<?=$model->username;?>" class="profile-button">SEE PROFILE</a>
</div>