<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\AlumniSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumnis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumni-index">





    <main class="page lab">
        <?php include('banner.php'); ?>

        <section class="page-content">



            <div class="container-full">

                <div class="tab-section">
                    <div class="tab-nav">
                        <div class="tab-nav">
                            <ul class="tabul">
                                <li>
                                    <a href="<?= Yii::$app->homeUrl; ?>charity">Charity</a>
                                </li>
                                <li>
                                    <a href="<?= Yii::$app->homeUrl; ?>events">Events</a>
                                </li>
                                <li>
                                    <a href="<?= Yii::$app->homeUrl; ?>articles">Articles</a>
                                </li>
                                <li>
                                    <a href="<?= Yii::$app->homeUrl; ?>achievements">Achievements</a>
                                </li>
                                <li>
                                    <a class="active" href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a>
                                </li>



                            </ul>
                        </div>
                        <div class="mob-tab-nav tabswiper">
                            <ul class="tabul swiper-wrapper labmm">
                                <li class="swiper-slide">
                                    <a href="<?= Yii::$app->homeUrl; ?>charity">Charity</a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="<?= Yii::$app->homeUrl; ?>events">Events</a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="<?= Yii::$app->homeUrl; ?>articles">Articles</a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="<?= Yii::$app->homeUrl; ?>achievements">Achievements</a>
                                </li>
                                <li class="swiper-slide">
                                    <a class="active" href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a>
                                </li>
                                <li class="swiper-slide dummyli"></li>

                            </ul>
                        </div>
                    </div>
                    <div class="tab-content alumni1 ">


                        <h1>Alumni</h1>
                        <?php //echo $this->render('_search', ['model' => $searchModel]); 
                        ?>
                        <div class="alumni-row">
                            <a class="alumniregbtn" href="<?= Yii::$app->homeUrl; ?>alumni/register"> Alumni Registration </a>
                            <?= ListView::widget([
                                'dataProvider' => $dataProvider,
                                'itemView' => '_item',
                            ]); ?>


                        </div>


                    </div>




                </div>
        </section>
    </main>



</div>