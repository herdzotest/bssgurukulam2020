<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Alumni */

$this->title = 'Alumni Registration Form';
$this->params['breadcrumbs'][] = ['label' => 'Alumnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumni-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'alumniProfile' => $alumniProfile,
        'profilePic' => $profilePic,
        'alumniCompany' => $alumniCompany,
    ]) ?>

</div>
