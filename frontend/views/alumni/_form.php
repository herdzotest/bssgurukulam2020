<?php

use admin\models\Countries;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model admin\models\Alumni */
/* @var $form yii\widgets\ActiveForm */

$Country = Countries::find()->all();
$listData = ArrayHelper::map($Country, 'id', 'name');
?>
<main class="page con-page">
    <?php include('banner.php'); ?>
    <section class="page-content">
        <div class="cform">

            <div class="container">
                <div class="formwrap">
                    <h2>Alumni Registration Form</h2>

                    <?php $form = ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="col-md-12">
                            <?php echo $form->field($profilePic, 'pp_image')->widget(FileInput::class, [
                                'options' => ['accept' => 'image/*'],
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'First Name'])->label(false) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'second_name')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Last Name'])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'gender')->dropDownList(['Male' => 'Male', 'Female' => 'Female', 'Transgender' => 'Transgender',], ['prompt' => '-- Select Gender --'])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'placeholder' => 'Mobile No'])->label(false)  ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email ID'])->label(false) ?>
                        </div>
                        <div class="col-md-12">
                            <h5>Communication Details</h5>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($alumniProfile, 'ap_address')->textarea(['rows' => 4, 'placeholder' => 'Current Communication Address'])->label(false) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($alumniProfile, 'ap_country')->dropDownList($listData, ['id' => 'country-id', 'prompt' => '--Choose Country--'])->label(false); ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($alumniProfile, 'ap_state')->widget(DepDrop::class, [
                                'options' => ['id' => 'state-id'],
                                'pluginOptions' => [
                                    'depends' => ['country-id'],
                                    'placeholder' => '--Choose State--',
                                    'url' => Url::to(['/alumni/state'])
                                ]
                            ])->label(false); ?>

                        </div>
                        <div class="col-md-3">
                            <?= $form->field($alumniProfile, 'ap_city')->widget(DepDrop::class, [
                                'pluginOptions' => [
                                    'depends' => ['state-id'],
                                    'placeholder' => '--Choose City--',
                                    'url' => Url::to(['/alumni/city'])
                                ]
                            ])->label(false); ?>

                        </div>
                        <div class="col-md-3">
                            <?= $form->field($alumniProfile, 'ap_pincode')->textInput(['maxlength' => true, 'placeholder' => 'Pincode'])->label(false) ?>
                        </div>
                        <div class="col-md-12">
                            <h5>Alumni Batch Details</h5>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($alumniProfile, 'ap_batch_from')->widget(\yii\jui\DatePicker::class, [
                                //'language' => 'ru',
                                'dateFormat' => 'yyyy-MM-dd',
                                'options' => ['class' => 'form-control', 'placeholder' => 'School Joined Date'],
                            ])->label(false) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($alumniProfile, 'ap_batch_to')->widget(\yii\jui\DatePicker::class, [
                                //'language' => 'ru',
                                'dateFormat' => 'yyyy-MM-dd',
                                'options' => ['class' => 'form-control', 'placeholder' => 'School Releaved Date'],
                            ])->label(false) ?>
                        </div>
                        <div class="col-md-12">
                            <h5>Alumni Qualification Details</h5>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($alumniProfile, 'ap_qualification')->textInput(['maxlength' => true, 'placeholder' => 'Qualification'])->label(false) ?>
                        </div>
                        <div class="col-md-12">
                            <h5>Alumni Social Media Links</h5>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($alumniProfile, 'ap_linkedin_link')->textInput(['maxlength' => true, 'placeholder' => 'Please provide your linkedin Id'])->label(false)->hint('www.linkedin.com/your-linkedin-id') ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($alumniProfile, 'ap_facebook_link')->textInput(['maxlength' => true, 'placeholder' => 'Please provide your facebook Id'])->label(false)->hint('www.facebook.com/your-facebook-id')  ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($alumniProfile, 'ap_twitter_link')->textInput(['maxlength' => true, 'placeholder' => 'Please provide your twitter Id'])->label(false)->hint('www.twitter.com/your-twitter-id')  ?>
                        </div>
                        <div class="col-md-12">
                            <h5>Company Profile Details</h5>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($alumniCompany, 'ac_postion')->textInput(['maxlength' => true, 'placeholder' => 'Your Current Postion'])->label(false) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($alumniCompany, 'ac_name')->textInput(['maxlength' => true, 'placeholder' => 'Name of your current company'])->label(false) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($alumniCompany, 'ac_joined_date')->widget(\yii\jui\DatePicker::class, [
                                //'language' => 'ru',
                                'dateFormat' => 'yyyy-MM-dd',
                                'options' => ['class' => 'form-control', 'placeholder' => 'Joined Date'],
                            ])->label(false) ?>

                        </div>
                        <div class="col-md-3">
                            <?= $form->field($alumniCompany, 'ac_relieved_date')->widget(\yii\jui\DatePicker::class, [
                                //'language' => 'ru',
                                'dateFormat' => 'yyyy-MM-dd',
                                'options' => ['class' => 'form-control', 'placeholder' => 'Releaved Date'],
                            ])->label(false) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($alumniCompany, 'ac_location')->textInput(['placeholder' => 'Company Location'])->label(false) ?>

                        </div>
                        <div class="col-md-3">
                            <?= $form->field($alumniCompany, 'ac_status')->dropDownList(['Active' => 'Yes', 'Inactive' => 'No'], ['prompt' => '--Current Company--'])->label(false) ?>
                        </div>

                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>

            </div>
        </div>
    </section>

</main>