<?php

use admin\models\EventGallery;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\Alumni */

$this->title = $model->ev_title;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<main class="page single">
    <?php include('banner.php'); ?>
    <section class="page-content charity-single">

        <div class="container-full">
            <div class="tab-section">
                <div class="tab-nav">
                    <ul class="tabul">
                        <li>
                            <a href="<?= Yii::$app->homeUrl; ?>charity">Charity</a>
                        </li>
                        <li>
                            <a class="active" href="<?= Yii::$app->homeUrl; ?>events">Events</a>
                        </li>
                        <li>
                            <a href="<?= Yii::$app->homeUrl; ?>articles">Articles</a>
                        </li>
                        <li>
                            <a href="<?= Yii::$app->homeUrl; ?>achievements">Achievements</a>
                        </li>
                        <li>
                            <a href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a>
                        </li>
                    </ul>
                </div>
                <div class="mob-tab-nav tabswiper">
                    <ul class="tabul swiper-wrapper labmm">
                        <li class="swiper-slide">
                            <a href="<?= Yii::$app->homeUrl; ?>charity">Charity</a>
                        </li>
                        <li class="swiper-slide">
                            <a class="active" href="<?= Yii::$app->homeUrl; ?>events">Events</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="<?= Yii::$app->homeUrl; ?>articles">Articles</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="<?= Yii::$app->homeUrl; ?>achievements">Achievements</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a>
                        </li>
                        <li class="swiper-slide dummyli"></li>
                    </ul>
                </div>
                <div class="container">
                    <h2>
                        <?= $this->title; ?>
                    </h2>
                    <?php $event_date = date('d-M-Y', strtotime($model->ev_date)); ?>
                    <h4><?= $event_date; ?></h4>
                    <div class="fac-slider">
                        <div class="col-md-7">
                            <div class="cs-img">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php $galleryModel = EventGallery::find()->where(['eg_eid' => $model->ev_id, 'eg_status' => 'Active'])->all();

                                        if (!empty($galleryModel)) {
                                            foreach ($galleryModel as $key => $value) {
                                                $image = $value->eg_img;
                                        ?>
                                                <div class="swiper-slide">
                                                    <img src="<?= Yii::$app->homeUrl; ?>theme/img/gallery/events/<?= $image; ?>" alt="<?= $model->ev_title; ?>">
                                                </div>
                                            <?php     }
                                        } else { ?>
                                            <div class="swiper-slide">
                                                <img src="<?= Yii::$app->homeUrl; ?>theme/img/gallery/events/no-image.png" alt="No-Image-Available">
                                            </div>
                                        <?php }
                                        ?>
                                    </div>
                                    <!-- Add Arrows -->
                                </div>
                            </div>
                            <?php if (!empty($galleryModel)) { ?>
                                <div class="swiper-button-next swipe-next" style="left: 545px;"></div>
                                <div class="swiper-button-prev swipe-prev" style="left: 25px;"></div>
                            <?php } ?>
                        </div>

                    </div>
                    <div class="cs-body">
                        <div class="col-md-7">
                            <p>
                                <?= $model->ev_desc; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</main>