<?php

use yii\helpers\Html;
//use yii\widgets\ListView;
use admin\models\Events;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\AlumniSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charity-index">
    <main class="page lab">
        <?php include('banner.php'); ?>
        <section class="page-content">
            <div class="container-full">
                <div class="tab-section">
                    <div class="tab-nav">
                        <ul class="tabul">
                            <li>
                                <a href="<?= Yii::$app->homeUrl; ?>charity">Charity</a>
                            </li>
                            <li>
                                <a class="active" href="<?= Yii::$app->homeUrl; ?>events">Events</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->homeUrl; ?>articles">Articles</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->homeUrl; ?>achievements">Achievements</a>
                            </li>
                            <li>
                                <a href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a>
                            </li>
                        </ul>
                    </div>
                    <div class="mob-tab-nav tabswiper">
                        <ul class="tabul swiper-wrapper labmm">
                            <li class="swiper-slide">
                                <a href="<?= Yii::$app->homeUrl; ?>charity">Charity</a>
                            </li>
                            <li class="swiper-slide">
                                <a class="active" href="<?= Yii::$app->homeUrl; ?>events">Events</a>
                            </li>
                            <li class="swiper-slide">
                                <a href="<?= Yii::$app->homeUrl; ?>articles">Articles</a>
                            </li>
                            <li class="swiper-slide">
                                <a href="<?= Yii::$app->homeUrl; ?>achievements">Achievements</a>
                            </li>
                            <li class="swiper-slide">
                                <a href="<?= Yii::$app->homeUrl; ?>alumni">Alumni</a>
                            </li>
                            <li class="swiper-slide dummyli"></li>
                        </ul>
                    </div>
                    <div class="tab-content events1 ">
                        <h1>Events</h1>
                        <div class="evroll">
                            <form method="post" action="<?= Yii::$app->homeUrl; ?>events" id="search_form_submit">
                                <div class="evrhead">
                                    <select name="month" id="month">
                                        <?php for ($i = 01; $i <= 12; $i++) {  ?>
                                            <option value="<?= $i ?>"><?= date('M', strtotime('2020-' . $i . '-01')) ?></option>
                                        <?php }  ?>
                                    </select>
                                    <select name="year" id="year">
                                        <?php $years = range(date('Y'), 2000); ?>
                                        <?php foreach ($years as $key => $value) { ?>
                                            <option value="<?= $value; ?>"><?= $value; ?></option>
                                        <?php } ?>
                                    </select>
                                    <button type="submit" id="event">
                                        Search
                                    </button>
                            </form>
                        </div>
                        <?php if (!empty($model)) { ?>
                            <div class="evrbody">
                                <div class="timeline">
                                    <?php $div = 0;
                                    foreach ($model as $key => $value) { ?>
                                        <div class="tl-item <?= (++$div % 2 ? " " : "tli2"); ?>">
                                            <div class="tl-body">
                                                <h3><?= substr($value->ev_title, 0, 30); ?>...</h3>
                                                <p>
                                                    <?php
                                                    $ev_descp = $value->ev_desc;
                                                    $sub_desc = substr($ev_descp, 0, 70);
                                                    ?>
                                                    <?= $sub_desc; ?>...
                                                </p>
                                                <a href="<?= Yii::$app->homeUrl; ?>events/<?= $value->ev_slug; ?>">Read More</a>
                                            </div>
                                            <div class="date">
                                                <?php
                                                $ev_dt = $value->ev_date;
                                                $sub_dt = substr($ev_dt, 8, 2);
                                                $unixTimestamp = strtotime($ev_dt);
                                                $dayOfWeek = date("l", $unixTimestamp);
                                                ?>
                                                <?= $sub_dt . ' ' . $dayOfWeek; ?>
                                                <img class="eventImg" src="<?= Yii::$app->homeUrl; ?>theme/img/events/featuredImages/<?= $value->ev_featuredImg; ?>" width="200" height="180">
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } else { ?>
                            <p>No events to display</p>
                        <?php } ?>
                        <div class="evlc">
                        </div>
                    </div>.
                </div>
            </div>
            <!-- swiper2 -->
</div>
</section>
</main>
</div>