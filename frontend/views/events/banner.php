<?php

use admin\models\PageBanners;

$page_banner = PageBanners::find()->where(['pb_id' => 6])->one();
$banner_header = 'Life @ BSS';
?>
<section class="title" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/page_banners/<?= $page_banner->pb_image;?>);">

<div class="container">
<h1><?= $banner_header;?></h1>
</div>

</section>