<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Students */

$this->title = 'New Admission Form';
$this->params['breadcrumbs'][] = ['label' => 'Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="students-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'parentModel' => $parentModel,
        'achievementModel' => $achievementModel,
        'academicModel' => $academicModel,
        'studentProfileImage' => $studentProfileImage,
        'studentBSList' => $studentBSList,
    ]) ?>

</div>
