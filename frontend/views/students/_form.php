<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model admin\models\Students */
/* @var $form yii\widgets\ActiveForm */
?>

<main class="page con-page">
    <?php include('banner.php'); ?>
    <section class="page-content">
        <div class="cform">
            <div class="container">
                <div class="formwrap">
                    <h2>Student Admission Form</h2>
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo $form->field($studentProfileImage, 'sp_file_name')->widget(FileInput::class, [
                                'options' => ['accept' => 'image/*'],
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'First Name'])->label(false) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'second_name')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Last Name'])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'gender')->dropDownList(['Male' => 'Male', 'Female' => 'Female', 'Transgender' => 'Transgender',], ['prompt' => '-- Select Gender --'])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'placeholder' => 'Student Mobile No or Parent Mobile No'])->label(false)  ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Student Email ID'])->label(false) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Parent Details</h5>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($parentModel, 'pp_father_name')->textInput(['maxlength' => true, 'placeholder' => "Father's Name" ])->label(false) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($parentModel, 'pp_father_qualification')->textInput(['maxlength' => true, 'placeholder' => "Father's Qualification"])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($parentModel, 'pp_father_occupation')->textInput(['maxlength' => true, 'placeholder' => "Father's Occupation"])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($parentModel, 'pp_father_email')->textInput(['maxlength' => true, 'placeholder' => "Father's Email ID"])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($parentModel, 'pp_father_mobile')->textInput(['maxlength' => true, 'placeholder' => "Father's Mobile No"])->label(false) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($parentModel, 'pp_mother_name')->textInput(['maxlength' => true, 'placeholder' => "Mother's Name"])->label(false) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($parentModel, 'pp_mother_qualification')->textInput(['maxlength' => true, 'placeholder' => "Mother's Qualification" ])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($parentModel, 'pp_mother_occupation')->textInput(['maxlength' => true, 'placeholder' => "Mother's Occupation"])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($parentModel, 'pp_mother_email')->textInput(['maxlength' => true, 'placeholder' => "Mother's Email ID"])->label(false) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($parentModel, 'pp_mother_mobile')->textInput(['maxlength' => true, 'placeholder' => "Mother's Mobile No"])->label(false) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($parentModel, 'pp_address')->textarea(['rows' => 4, 'placeholder' => 'Communication Address with Pincode'])->label(false) ?>
                        </div>
                    </div>
                    <div class="row academics">
                        <div class="col-md-12">
                            <h5>Academic Details</h5>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($academicModel, 'admission_class')->dropDownList(['LKG' => 'LKG', 'UKG' => 'UKG', 'Class I' => 'Class I', 'Class II' => 'Class II', 'Class III' => 'Class III', 'Class IV' => 'Class IV', 'Class V' => 'Class V', 'Class VI' => 'Class VI', 'Class VII' => 'Class VII', 'Class VIII' => 'Class VIII', 'Class IX' => 'Class IX', 'Class X' => 'Class X', 'Class XI' => 'Class XI', 'Class XII' => 'Class XII',], ['prompt' => '-- Select admission class --'])->label(false) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($academicModel, 'student_last_school')->textInput(['maxlength' => true, 'placeholder' => 'Name of your school last studied'])->label(false) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($academicModel, 'student_brother_sisters')->dropDownList(['yes' => 'Yes', 'no' => 'No',], ['prompt' => '-- Brother/sister studying in this school --'])->label(false) ?>
                        </div>
                    </div>
                    <div id="student_bs_details" class="row">
                        <table id="dataTable" class="form" border="1">
                            <tbody>
                                <tr>
                                    <p>
                                        <td>
                                            <?= $form->field($studentBSList, 'list_bs_name[]')->textInput(['maxlength' => true, 'placeholder' => 'Name of your brother/ sister'])->label(false) ?>
                                        </td>
                                        <td>
                                            <?= $form->field($studentBSList, 'list_bs_class[]')->dropDownList(['LKG' => 'LKG', 'UKG' => 'UKG', 'Class I' => 'Class I', 'Class II' => 'Class II', 'Class III' => 'Class III', 'Class IV' => 'Class IV', 'Class V' => 'Class V', 'Class VI' => 'Class VI', 'Class VII' => 'Class VII', 'Class VIII' => 'Class VIII', 'Class IX' => 'Class IX', 'Class X' => 'Class X', 'Class XI' => 'Class XI', 'Class XII' => 'Class XII',], ['prompt' => '-- Select brother/sister class --'])->label(false) ?>
                                        </td>
                                        <td>
                                            <input class="col-md-12 btn-success btn" type="button" value="Add More" onClick="addRow('dataTable')" />
                                        </td>
                                        <td>
                                            <input class="col-md-12 btn-danger btn" type="button" value="Remove Students" onClick="deleteRow('dataTable')" />
                                        </td>
                                    </p>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="row achievements">
                        <div class="col-md-12 more-btn">
                            <div class="more-btn">
                                <h5>Achievement Details
                                    <span class="pull-right">
                                        <a href="#" class="btn-success btn" onClick="addRow('dataTable1')">Add More</a>
                                        <a href="#" class="btn-danger btn" onClick="deleteRow('dataTable1')">Remove</a>
                                    </span>
                                </h5>
                            </div>
                        </div>
                        <table id="dataTable1" style="margin: 10px;" class="form" border="1">
                            <tbody>
                                <tr>
                                    <p>
                                        <td>
                                            <?= $form->field($achievementModel, 'competition_level[]')->dropDownList(['School Level' => 'School Level', 'Sub District Level' => 'Sub District Level', 'District Level' => 'District Level', 'State Level' => 'State Level',], ['prompt' => '-- Select a level --'])->label(false) ?>
                                        </td>
                                        <td>
                                            <?= $form->field($achievementModel, 'competition_items[]')->textInput(['maxlength' => true, 'placeholder' => 'Items'])->label(false) ?>
                                        </td>
                                        <td>
                                            <?= $form->field($achievementModel, 'competition_position[]')->textInput(['maxlength' => true, 'placeholder' => 'Position'])->label(false) ?>
                                        </td>
                                        <td>
                                            <?= $form->field($achievementModel, 'competition_grade[]')->textInput(['maxlength' => true, 'placeholder' => 'Grade'])->label(false) ?>
                                        </td>
                                    </p>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row ">

                        </div>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>
</main>