<?php

use yii\helpers\Html;
?>
<main class="page con-page">
  <?php include('banner.php'); ?>
  <section class="page-content">
    <div class="cform">
      <div class="container">
        <div class="formwrap">
          <div class="jumbotron text-center">
            <?php if ($userDetails != 'empty') { ?>
              <center>
                <h1 class="display-3">Thank You!</h1>
                <p class="lead"><strong>Please check your email ID (<?= $userDetails['email']; ?>) for further updates and we wish all the best</strong></p>
                <hr>
              <?php } else { ?>
                <center>
                  <h1 class="display-3">Oops! Something went wrong</h1>
                  <p class="lead"><strong>Please Contact our Administartor!!!</strong></p>
                  <hr>
                <?php } ?>
                <p>
                  Having trouble? <a href="<?= Yii::$app->homeUrl; ?>contact-us">Contact us</a>
                </p>
                <p class="lead">
                  <a class="btn btn-primary btn-sm" href="<?= Yii::$app->homeUrl; ?>" role="button">Continue to homepage</a>
                </p>
                </center>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>