<?php

use yii\helpers\Html;
?>
<div class="charity-item">
    
<img class="alumni-img" src="<?= Yii::$app->homeUrl;?>theme/img/charity/featuredImages/<?= $model::getImage($model->ch_id);?>" alt="<?= $model->ch_title;?>">
    <h3><?= $model->ch_title;?></h3>
    <div>
        <a href="<?= Yii::$app->homeUrl;?>charity/<?=$model->ch_slug;?>" class="readmore">READ MORE</a>
    </div>
</div>
