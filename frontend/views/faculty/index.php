<?php

/* @var $this yii\web\View */

use admin\models\Faculty;
use yii\helpers\Html;

$this->title = 'BSS GURUKULAM HIGHER SECONDARY SCHOOL :: Faculty';
$banner_header = 'Our faculty';
$this->params['breadcrumbs'][] = $this->title;
?>
<main class="page faculty">
  <?php include('banner.php'); ?>
  <section class="page-content">
    <div class="container">
      <h2 class="xs-hidden">Staffs</h2>

      <div class="faculty-content">
        <div class="staff-list">
          <ul>
            <li>
              <a class="stafftabbtn active" onclick="showSection(0)" href="#">Kindergarten</a>
            </li>
            <li>
              <a class="stafftabbtn" href="#" onclick="showSection(1)">Primary</a>
            </li>
            <li>
              <a class="stafftabbtn" href="#" onclick="showSection(2)">Upper Primary
              </a>
            </li>
            <li>
              <a class="stafftabbtn" href="#" onclick="showSection(3)">Secondary
              </a>
            </li>
            <li>
              <a class="stafftabbtn" href="#" onclick="showSection(4)">Higher Secondary
              </a>
            </li>
            <li>
              <a class="stafftabbtn" href="#" onclick="showSection(5)">
                Non Teaching
              </a>
            </li>
          </ul>
        </div>

        <div class="mob-tab-nav tabswiper mob-staftab">
          <ul class="tabul swiper-wrapper ">
            <li class="swiper-slide">
              <a class="stafftabbtn active" onclick="showSection(0)" href="#">Kindergarten</a>
            </li>
            <li class="swiper-slide">
              <a class="stafftabbtn" href="#" onclick="showSection(1)">Primary</a>
            </li>
            <li class="swiper-slide">
              <a class="stafftabbtn" href="#" onclick="showSection(2)">Upper Primary
              </a>
            </li>
            <li class="swiper-slide">
              <a class="stafftabbtn" href="#" onclick="showSection(3)">Secondary
              </a>
            </li>
            <li class="swiper-slide">
              <a class="stafftabbtn" href="#" onclick="showSection(4)">Higher Secondary
              </a>
            </li>
            <li class="swiper-slide">
              <a class="stafftabbtn" href="#" onclick="showSection(5)">
                Non Teaching
              </a>
            </li>
            <li class="swiper-slide dummyli"></li>
          </ul>
        </div>

        <div class="staff-content">
          <div class="staff-pane active">
            <?php if (!empty($facultyKG)) { ?>
              <?php foreach ($facultyKG as $key => $value) { ?>
                <div class="staff">
                  <div class="staff-head">
                    <div class="staff-img" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/pro_images/faculty/<?= Faculty::getImage($value->pd_res_id); ?>)"></div>
                    <h3><?= Faculty::getfacultyName($value->pd_res_id) ?></h3>
                  </div>
                  <div class="stafftab">
                    <button onclick="openExp(event)" class="stf-btn exp-btn">
                      Experience</button><button onclick="openQua(event)" class="stf-btn qua-btn active">
                      Details
                    </button>
                    <div class="staffcon quals">
                      <p>Subject : <?= Faculty::getfacultySubject($value->pd_res_id); ?></p>
                      <p>Email : <?= Faculty::getfacultyEmail($value->pd_res_id); ?></p>
                      <h3>Qualification</h3>
                      <?= Faculty::getfacultyQualification($value->pd_res_id); ?>
                    </div>

                    <div class="staffcon exp">
                      <?php $facultyExperience = Faculty::getfacultyExperience($value->pd_res_id);
                      foreach ($facultyExperience as $key => $experience) { ?>
                        <p>
                          <?= $experience->re_role . ', ' . $experience->re_institute . ', From ' . date('d-m-Y', strtotime($experience->re_start_date)) . ' to ' . date('d-m-Y', strtotime($experience->re_end_date)); ?>
                        </p>
                      <?php } ?>
                    </div>
                  </div>
                </div>
            <?php }
            }else{
              echo "No Data Available";
            } ?>
          </div>

          <div class="staff-pane">
            <?php if (!empty($facultyPrimary)) { ?>
              <?php foreach ($facultyPrimary as $key => $value) { ?>
                <div class="staff">
                  <div class="staff-head">
                    <div class="staff-img" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/pro_images/faculty/<?= Faculty::getImage($value->pd_res_id); ?>)"></div>
                    <h3><?= Faculty::getfacultyName($value->pd_res_id) ?></h3>
                  </div>
                  <div class="stafftab">
                    <button onclick="openExp(event)" class="stf-btn exp-btn">
                      Experience</button><button onclick="openQua(event)" class="stf-btn qua-btn active">
                      Details
                    </button>
                    <div class="staffcon quals">
                      <p>Subject : <?= Faculty::getfacultySubject($value->pd_res_id); ?></p>
                      <p>Email : <?= Faculty::getfacultyEmail($value->pd_res_id); ?></p>
                      <h3>Qualification</h3>
                      <?= Faculty::getfacultyQualification($value->pd_res_id); ?>
                    </div>

                    <div class="staffcon exp">
                      <?php $facultyExperience = Faculty::getfacultyExperience($value->pd_res_id);
                      foreach ($facultyExperience as $key => $experience) { ?>
                        <p>
                          <?= $experience->re_role . ', ' . $experience->re_institute . ', From ' . date('d-m-Y', strtotime($experience->re_start_date)) . ' to ' . date('d-m-Y', strtotime($experience->re_end_date)); ?>
                        </p>
                      <?php } ?>
                    </div>
                  </div>
                </div>
            <?php }
            }else{
              echo "No Data Available";
            } ?>
          </div>

          <div class="staff-pane">
            <?php if (!empty($facultyUP)) { ?>
              <?php foreach ($facultyUP as $key => $value) { ?>
                <div class="staff">
                  <div class="staff-head">
                    <div class="staff-img" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/pro_images/faculty/<?= Faculty::getImage($value->pd_res_id); ?>)"></div>
                    <h3><?= Faculty::getfacultyName($value->pd_res_id) ?></h3>
                  </div>
                  <div class="stafftab">
                    <button onclick="openExp(event)" class="stf-btn exp-btn">
                      Experience</button><button onclick="openQua(event)" class="stf-btn qua-btn active">
                      Details
                    </button>
                    <div class="staffcon quals">
                      <p>Subject : <?= Faculty::getfacultySubject($value->pd_res_id); ?></p>
                      <p>Email : <?= Faculty::getfacultyEmail($value->pd_res_id); ?></p>
                      <h3>Qualification</h3>
                      <?= Faculty::getfacultyQualification($value->pd_res_id); ?>
                    </div>

                    <div class="staffcon exp">
                      <?php $facultyExperience = Faculty::getfacultyExperience($value->pd_res_id);
                      foreach ($facultyExperience as $key => $experience) { ?>
                        <p>
                          <?= $experience->re_role . ', ' . $experience->re_institute . ', From ' . date('d-m-Y', strtotime($experience->re_start_date)) . ' to ' . date('d-m-Y', strtotime($experience->re_end_date)); ?>
                        </p>
                      <?php } ?>
                    </div>
                  </div>
                </div>
            <?php }
            }else{
              echo "No Data Available";
            } ?>
          </div>
          <div class="staff-pane">
            <?php if (!empty($facultySecondary)) { ?>
              <?php foreach ($facultySecondary as $key => $value) { ?>
                <div class="staff">
                  <div class="staff-head">
                    <div class="staff-img" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/pro_images/faculty/<?= Faculty::getImage($value->pd_res_id); ?>)"></div>
                    <h3><?= Faculty::getfacultyName($value->pd_res_id) ?></h3>
                  </div>
                  <div class="stafftab">
                    <button onclick="openExp(event)" class="stf-btn exp-btn">
                      Experience</button><button onclick="openQua(event)" class="stf-btn qua-btn active">
                      Details
                    </button>
                    <div class="staffcon quals">
                      <p>Subject : <?= Faculty::getfacultySubject($value->pd_res_id); ?></p>
                      <p>Email : <?= Faculty::getfacultyEmail($value->pd_res_id); ?></p>
                      <h3>Qualification</h3>
                      <?= Faculty::getfacultyQualification($value->pd_res_id); ?>
                    </div>

                    <div class="staffcon exp">
                      <?php $facultyExperience = Faculty::getfacultyExperience($value->pd_res_id);
                      foreach ($facultyExperience as $key => $experience) { ?>
                        <p>
                          <?= $experience->re_role . ', ' . $experience->re_institute . ', From ' . date('d-m-Y', strtotime($experience->re_start_date)) . ' to ' . date('d-m-Y', strtotime($experience->re_end_date)); ?>
                        </p>
                      <?php } ?>
                    </div>
                  </div>
                </div>
            <?php }
            }else{
              echo "No Data Available";
            } ?>
          </div>
          <div class="staff-pane">
            <?php if (!empty($facultyHigherSecondary)) { ?>
              <?php foreach ($facultyHigherSecondary as $key => $value) { ?>
                <div class="staff">
                  <div class="staff-head">
                    <div class="staff-img" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/pro_images/faculty/<?= Faculty::getImage($value->pd_res_id); ?>)"></div>
                    <h3><?= Faculty::getfacultyName($value->pd_res_id) ?></h3>
                  </div>
                  <div class="stafftab">
                    <button onclick="openExp(event)" class="stf-btn exp-btn">
                      Experience</button><button onclick="openQua(event)" class="stf-btn qua-btn active">
                      Details
                    </button>
                    <div class="staffcon quals">
                      <p>Subject : <?= Faculty::getfacultySubject($value->pd_res_id); ?></p>
                      <p>Email : <?= Faculty::getfacultyEmail($value->pd_res_id); ?></p>
                      <h3>Qualification</h3>
                      <?= Faculty::getfacultyQualification($value->pd_res_id); ?>
                    </div>

                    <div class="staffcon exp">
                      <?php $facultyExperience = Faculty::getfacultyExperience($value->pd_res_id);
                      foreach ($facultyExperience as $key => $experience) { ?>
                        <p>
                          <?= $experience->re_role . ', ' . $experience->re_institute . ', From ' . date('d-m-Y', strtotime($experience->re_start_date)) . ' to ' . date('d-m-Y', strtotime($experience->re_end_date)); ?>
                        </p>
                      <?php } ?>
                    </div>
                  </div>
                </div>
            <?php }
            }else{
              echo "No Data Available";
            } ?>
          </div>
          <div class="staff-pane">
            <?php if (!empty($others)) { ?>
              <?php foreach ($others as $key => $value) { ?>
                <div class="staff">
                  <div class="staff-head">
                    <div class="staff-img" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/pro_images/faculty/<?= Faculty::getImage($value->pd_res_id); ?>)"></div>
                    <h3><?= Faculty::getfacultyName($value->pd_res_id) ?></h3>
                  </div>
                  <div class="stafftab">
                    <button onclick="openExp(event)" class="stf-btn exp-btn">
                      Experience</button><button onclick="openQua(event)" class="stf-btn qua-btn active">
                      Details
                    </button>
                    <div class="staffcon quals">
                      <p>Subject : <?= Faculty::getfacultySubject($value->pd_res_id); ?></p>
                      <p>Email : <?= Faculty::getfacultyEmail($value->pd_res_id); ?></p>
                      <h3>Qualification</h3>
                      <?= Faculty::getfacultyQualification($value->pd_res_id); ?>
                    </div>

                    <div class="staffcon exp">
                      <?php $facultyExperience = Faculty::getfacultyExperience($value->pd_res_id);
                      if (!empty($facultyExperience)){
                      foreach ($facultyExperience as $key => $experience) { ?>
                        <p>
                          <?= $experience->re_role . ', ' . $experience->re_institute . ', From ' . date('d-m-Y', strtotime($experience->re_start_date)) . ' to ' . date('d-m-Y', strtotime($experience->re_end_date)); ?>
                        </p>
                      <?php } }else{
                        echo 'No Data Available';
                      } ?>
                    </div>
                  </div>
                </div>
            <?php }
            }else{
              echo "No Data Available";
            } ?>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>