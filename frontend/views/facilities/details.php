<?php

/* @var $this yii\web\View */

use admin\models\FacilityGallery;
use yii\helpers\Html;
use admin\models\PageBanners;

$this->title = 'BSS GURUKULAM HIGHER SECONDARY SCHOOL :: Facility ' . $model->f_title;
$banner_header = 'Facilities';
$this->params['breadcrumbs'][] = $this->title;

$page_banner = PageBanners::find()->where(['pb_page_name' => 'Facilities'])->one();
// $banner_header = $model->f_title;
?>
<main class="page facility">
    <section class="title" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/page_banners/<?= $page_banner->pb_image; ?>);">

        <div class="container">
            <h1><?= $banner_header; ?></h1>
        </div>

    </section>
    <section class="page-content">


        <div class="container-full">

            <div class="row">
                <div class="col-md-2">
                    <div class="tab-section">
                        <?php include('facility_side_bar.php'); ?>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-8">

                    <h2>
                        <?= $model->f_title; ?>
                    </h2>
                    <div class="fac-slider">

                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php $galleryModel = FacilityGallery::find()->where(['fg_fid' => $model->f_id, 'fg_status' => 'Active'])->all();

                                if (!empty($galleryModel)) {
                                    foreach ($galleryModel as $key => $value) {
                                        $image = $value->fg_img;
                                ?>
                                        <div class="swiper-slide">
                                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/gallery/facility/<?= $image; ?>" alt="<?= $model->f_title; ?>">
                                        </div>
                                    <?php     }
                                } else { ?>
                                    <div class="swiper-slide">
                                        <img src="<?= Yii::$app->homeUrl; ?>theme/img/gallery/facility/no-image.png" alt="No-Image-Available">
                                    </div>
                                <?php }
                                ?>
                            </div>
                            <!-- Add Arrows -->

                        </div>
                        <?php if (!empty($galleryModel)) { ?>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        <?php } ?>
                    </div>

                    <?= $model->f_desc; ?>
                </div>
                <!-- <div class="col-md-1"></div> -->


            </div>

        </div>

    </section>
</main>