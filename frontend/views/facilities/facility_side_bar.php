<?php

use admin\models\Facilities;

?>

<div class="tab-nav list">
    <ul class="tabul">
        <?php
        if (isset($_GET['slug'])) { ?>
            <?php $facility_model = Facilities::find()->where(['f_status' => 'Active'])->all();
            foreach ($facility_model as $key => $value) { ?>
                <li class="swiper-slide">
                    <?php if ($_GET['slug'] == $value->f_slug) {
                        $active = 'active';
                    } else {
                        $active = '';
                    } ?>
                    <a class="<?php echo $active; ?>" href="<?= Yii::$app->homeUrl . 'facilities/' . $value->f_slug; ?>"><?= $value->f_title; ?></a>
                </li>
            <?php }
        } else {
            $facility_model = Facilities::find()->where(['f_status' => 'Active'])->all();
            foreach ($facility_model as $key => $value) { $i = 0;?>
                <li class="swiper-slide">
                    <?php if ($key == $i) {
                        $active = 'active';
                    } else {
                        $active = '';
                    } ?>
                    <a class="<?php echo $active; ?>" href="<?= Yii::$app->homeUrl . 'facilities/' . $value->f_slug; ?>"><?= $value->f_title; ?></a>
                </li>
        <?php   }
        } ?>

    </ul>

</div>