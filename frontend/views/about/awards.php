<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'BSS Gurukulam Higher Secondary School :: Accolades and Awards';
$this->params['breadcrumbs'][] = $this->title;
?>

<main class="page about-page">
    <section class="title" style="background-image:url(<?= Yii::$app->homeUrl;?>theme/img/titleweb.jpg);">

        <div class="container">
            <h1>Accolades and Awards</h1>
        </div>

    </section>
    <section class="page-content">
        <div class="container-full">
            <!-- swiper1 -->
            <div class="tab-section">
                <?php include('about_side_bar.php'); ?>
            <div class="tab-content accolades">
                <div class="page-row">
                    <div class="page-column c111">
                    </div>
                    <div class="page-column c12">
                        <h1>Accolades and Awards</h1>

                        <h3>
                            ISO 9001-2008 Certified School 2010 to 2017
                        </h3>
                        <img src="<?= Yii::$app->homeUrl; ?>theme/img/doc.jpg" alt="ISO 9001-2008 Certified School 2010 to 2017">
                    </div>
                    <div class="page-column cg1">
                    </div>
                </div>
            </div>
        </div>

        <!-- swiper2 -->


        </div>
    </section>
</main>