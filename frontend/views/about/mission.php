
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'BSS Gurukulam Higher Secondary School :: Our Mission & Vision';
$this->params['breadcrumbs'][] = $this->title;
?>
<main class="page about-page">
    <section class="title" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/titleweb.jpg);">

        <div class="container">
            <h1>Mission & Vision</h1>
        </div>

    </section>
    <section class="page-content">
        <div class="container-full">
            <!-- swiper1 -->
            <div class="tab-section">
                <?php include('about_side_bar.php'); ?>
                <div class="tab-content mission">
                    <div class="page-row">
                        <div class="page-column column1">
                            <div class="missioncol">
                                <div class="content">
                                    <div class="mcol-wrap">

                                        <div class="mcol-title">
                                            <div>
                                                <img src="<?= Yii::$app->homeUrl; ?>theme/img/missio.png" alt="">

                                            </div>
                                            <h2>Mission</h2>
                                        </div>
                                        <div class="desc">
                                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/quote.png" alt="">
                                            <p>
                                                To be the best educational institution in our country
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="image">
                                    <img src="<?= Yii::$app->homeUrl; ?>theme/img/missionimg.png" alt="">
                                </div>
                            </div>
                            <div class="missioncol vision">
                                <div class="content">
                                    <div class="mcol-wrap">

                                        <div class="mcol-title">
                                            <div>
                                                <img src="<?= Yii::$app->homeUrl; ?>theme/img/vision.png" alt="">

                                            </div>
                                            <h2>Vision</h2>
                                        </div>
                                        <div class="desc">
                                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/quote.png" alt="">
                                            <p>
                                                Providing Quality Education to mould. Self reliant,

                                                Self restraint and Selfless human beings for the entire Universe
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="image">
                                    <img src="<?= Yii::$app->homeUrl; ?>theme/img/visionimg.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="page-column column2">


                        </div>



                    </div>


                </div>

            </div>

            <!-- swiper2 -->


        </div>
    </section>
</main>