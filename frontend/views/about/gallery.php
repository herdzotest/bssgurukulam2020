<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'BSS Gurukulam Higher Secondary School :: Gallery';
$this->params['breadcrumbs'][] = $this->title;
?>
<main class="page about-page">
    <section class="title" style="background-image:url(img/titleweb.jpg);">

        <div class="container">
            <h1>Gallery</h1>
        </div>

    </section>
    <section class="page-content">



        <div class="container-full">
            <!-- swiper1 -->
            <div class="tab-section">
            <?php include('about_side_bar.php'); ?>
                <div class="tab-content gallery">


                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="gwrap">

                                <div class="swiper-container gallery-top">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide" style="background-image:url(img/1.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/2.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/4.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/5.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/6.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/7.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/9.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/10.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/11.jpg)"></div>
                                    </div>
                                    <!-- Add Arrows -->
                                    <div class="swiper-button-next swiper-button-white"></div>
                                    <div class="swiper-button-prev swiper-button-white"></div>
                                </div>
                                <div class="swiper-container gallery-thumbs">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide" style="background-image:url(img/1.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/2.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/4.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/5.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/6.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/7.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/9.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/10.jpg)"></div>
                                        <div class="swiper-slide" style="background-image:url(img/11.jpg)"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-1">

                        </div>
                    </div>



                </div>

            </div>

            <!-- swiper2 -->


        </div>
    </section>
</main>