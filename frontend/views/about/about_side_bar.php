<?php

use admin\models\AboutPages;
error_reporting(0);
?>
<?php $URL = Yii::$app->request->url;
$URL = explode('/', $URL);
if ($URL[2] == 'system-of-school') { ?>
    <style>
        .bluebg {
            position: absolute !important;
            width: 200% !important;
            background: #ebf3f3 !important;
            height: 100% !important;
            left: -50% !important;
            top: 0 !important;
        }

        .bluesec.page-row {
            position: relative !important;
            padding-top: 40px !important;
            padding-bottom: 40px !important;
        }

        .bluesec .page-column {
            position: relative !important;
            flex: 4 !important;
        }

        .bluesec .c4th {
            flex: 1 !important;
        }
    </style>
<?php } ?>

<div class="tab-nav">
    <ul>
        <?php
        if (isset($_GET['slug'])) { ?>
            <?php $about_model = AboutPages::find()->where(['ap_status' => 'Active'])->all();
            foreach ($about_model as $key => $value) { ?>
                <li class="swiper-slide">
                    <?php if ($_GET['slug'] == $value->ap_slug) {
                        $active = 'active';
                    } else {
                        $active = '';
                    } ?>
                    <a class="<?php echo $active; ?>" href="<?= Yii::$app->homeUrl . 'about/' . $value->ap_slug; ?>"><?= $value->ap_title; ?></a>
                </li>
            <?php }
        } else {
            $about_model = AboutPages::find()->where(['ap_status' => 'Active'])->all();
            foreach ($about_model as $key => $value) {
                $i = 0; ?>
                <li class="swiper-slide">
                    <?php if ($key == $i) {
                        $active = 'active';
                    } else {
                        $active = '';
                    } ?>
                    <a class="<?php echo $active; ?>" href="<?= Yii::$app->homeUrl . 'about/' . $value->ap_slug; ?>"><?= $value->ap_title; ?></a>
                </li>
        <?php   }
        } ?>

    </ul>

</div>