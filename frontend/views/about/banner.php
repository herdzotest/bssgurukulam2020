<?php

use admin\models\PageBanners;

$page_banner = PageBanners::find()->where(['pb_page_name' => 'About'])->one();
$banner_header = 'About School';
?>
<section class="title" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/page_banners/<?= $page_banner->pb_image;?>);">

<div class="container">
<h1><?= $banner_header;?></h1>
</div>

</section>