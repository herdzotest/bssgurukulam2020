<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'BSS Gurukulam Higher Secondary School :: Hall Of Fame';
$this->params['breadcrumbs'][] = $this->title;
?>
<main class="page about-page">
    <section class="title" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/titleweb.jpg);">

        <div class="container">
            <h1>Hall Of Fame</h1>
        </div>

    </section>
    <section class="page-content">



        <div class="container-full">
            <!-- swiper1 -->
            <div class="tab-section">
                <?php include('about_side_bar.php'); ?>
                <div class="tab-content hall">




                    <div class="page-row">
                        <div class="page-column column1">
                            <h1>Hall of Fame</h1>

                            <p>
                                We are achieving results 100 % in High school section and Higher secondary section for the past 16 years and good number of A+ holders in High school section and Higher secondary section, our students scored 100% marks in all subjects (1200/1200) .

                                We were awarded the best high school trophy for the consecutive 5 years in Kerala State youth Festival (5000 schools are participating in this event).

                                Sports- our Children are in Indian Team , Achieved Gold medal, silver and bronze medals in National Level competetions

                                Our Children awarded first in Yuva Master Mind award by Malayala Manorama for new Inventions
                            </p>
                        </div>
                        <div class="page-column column2">
                            <img src="<?= Yii::$app->homeUrl; ?>theme/img/hall.png" alt="">


                        </div>
                        <div class="page-column c111">
                        </div>



                    </div>


                </div>

            </div>

            <!-- swiper2 -->


        </div>
    </section>
</main>