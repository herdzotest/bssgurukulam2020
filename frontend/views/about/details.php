<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use admin\models\PageBanners;

$this->title = 'BSS GURUKULAM HIGHER SECONDARY SCHOOL :: About ' . $model->ap_title;
$banner_header = 'About ' . $model->ap_title;
$this->params['breadcrumbs'][] = $this->title;

$page_banner = PageBanners::find()->where(['pb_page_name' => 'About'])->one();
$banner_header = $model->ap_title;
?>
<main class="page about-page">
    <section class="title" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/page_banners/<?= $page_banner->pb_image; ?>);">

        <div class="container">
            <h1><?= $banner_header; ?></h1>
        </div>

    </section>
    <section class="page-content">
        <div class="container-full">
            <div class="tab-section">
                <?php include('about_side_bar.php'); ?>
                <?= $model->ap_content; ?>
            </div>
        </div>
    </section>
</main>