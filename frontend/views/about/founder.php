<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'BSS Gurukulam Higher Secondary School :: Founder';
$this->params['breadcrumbs'][] = $this->title;
?>
<main class="page about-page">
    <section class="title" style="background-image:url(<?= Yii::$app->homeUrl; ?>theme/img/titleweb.jpg);">

        <div class="container">
            <h1>Founder</h1>
        </div>

    </section>
    <section class="page-content">
        <div class="container-full">
            <!-- swiper1 -->
            <div class="tab-section">
                    <?php include('about_side_bar.php'); ?>
                    <div class="tab-content founder">
                        <div class="row">
                            <div class="col-md-6">
                                <h1>Founder</h1>

                                <h4>Swami Nirmalananda Yogi(Swamiji)</h4>
                                <p>
                                    Philantrophist, Humanitarian, Visionary & a great leader Swami Nirmalananda Yogi, known simply as “Swamiji, the founder of BSS Educational Society has served the societal community for decades, imparting wisdom, strength, motivation, consciousness, insightness and inspiration. He was an educationist, orator & a person who fought against superstitions prevailing in society during those olden periods in 1970’s & 80’s.Through his extraordinary acts of love, inner strength and self-sacrifice, Swamiji has endeared himself to the whole society and inspired thousands of people to follow in his path of selfless service.
                                </p>
                                <p>
                                    It doesn’t take much time to get to know a person when his action speaks for him. It becomes his Mirror reflecting with personality even whoever steps into this Institutions feel the vibrant presence of this Serene Soul.
                                </p>
                                <p>
                                    Swamy Nirmalananda Yogi with far sightedness founded this Kalashethra (BSS Gurukulam Higher Secondary School ) just four decades ago with not even a penny in his hand.“ Manushyan Nannavan Manassu Nannayaal mathy” (in Regional Language- Malayalam) implies “To be a real human-being, only his mind is sufficient to be pure”. Hence the implication of this fundamental ideology with regard to this powerful mind was not by physique but by mind, advocated the principle of his Guru Brahmananda Swami Sivayogi, one of the social reformers in Kerala.
                                </p>
                            </div>
                            <div class="col-md-5">
                                <img src="<?= Yii::$app->homeUrl; ?>theme/img/founder.jpg" alt="">

                            </div>
                            <div class="col-md-1">

                            </div>
                        </div>



                    </div>

                </div>

                <!-- swiper2 -->


            </div>
    </section>
</main>