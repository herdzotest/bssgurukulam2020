<?php

namespace frontend\controllers;

use admin\models\AboutPages;
use yii\web\NotFoundHttpException;

class AboutController extends \yii\web\Controller
{
    // public $layout = 'main';
    public function actionIndex()
    {
        $templateName = 'details';
        $model = AboutPages::find()->where(['ap_status' => 'Active'])->one();
        return $this->render($templateName, [
            'model' => $model,
        ]);
    }
    public function actionMission()
    {
        return $this->render('mission');
    }
    public function actionHall()
    {
        return $this->render('hall');
    }
    public function actionAwards()
    {
        return $this->render('awards');
    }
    public function actionFounder()
    {
        return $this->render('founder');
    }
    public function actionGallery()
    {
        return $this->render('gallery');
    }

    public function actionDetails($slug)
    {
        $model = AboutPages::find()->where(['ap_slug' => $slug])->one();
        if ($model !== null) {
            $templateName = 'details';
            return $this->render($templateName, [
                'model' => $model,
            ]);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
       
    }

}
