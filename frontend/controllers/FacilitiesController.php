<?php

namespace frontend\controllers;

use admin\models\Facilities;
use yii\web\NotFoundHttpException;

class FacilitiesController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = Facilities::find()->where(['f_status' => 'Active'])->one();
        if ($model !== null) {
            $templateName = 'details';
            return $this->render($templateName, [
                'model' => $model,
            ]);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
       
    }
    public function actionDetails($slug)
    {
        $model = Facilities::find()->where(['f_slug' => $slug])->one();
        if ($model !== null) {
            $templateName = 'details';
            return $this->render($templateName, [
                'model' => $model,
            ]);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
       
    }

}
