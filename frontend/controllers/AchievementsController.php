<?php

namespace frontend\controllers;

use admin\models\Achievements;
use admin\models\AchievementsSearch;
use Yii;

class AchievementsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new AchievementsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDetails($slug)
    {
        $model = Achievements::find()->where(['av_slug' => $slug])->one();
        return $this->render('details', [
            'model' => $model,
        ]);

    }
}
