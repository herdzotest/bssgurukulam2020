<?php

namespace frontend\controllers;

use admin\models\Articles;
use admin\models\ArticlesSearch;
use Yii;

class ArticlesController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new ArticlesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDetails($slug)
    {
        $model = Articles::find()->where(['ar_slug' => $slug])->one();
        return $this->render('details', [
            'model' => $model,
        ]);
    }
}
