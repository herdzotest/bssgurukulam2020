<?php

namespace frontend\controllers;

use admin\models\Charity;
use admin\models\CharitySearch;
use Yii;

class CharityController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $searchModel = new CharitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDetails($slug)
    {
        $model = Charity::find()->where(['ch_slug' => $slug])->one();
        return $this->render('details', [
            'model' => $model,
        ]);

    }

}
