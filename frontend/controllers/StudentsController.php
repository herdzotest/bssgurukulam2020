<?php

namespace frontend\controllers;

use admin\models\AdmissionAcademics;
use admin\models\AdmissionAchievement;
use admin\models\StudentbsList;
use admin\models\StudentParentProfile;
use admin\models\StudentProfileImage;
//use admin\models\Address;
use Yii;
use admin\models\Students;
use admin\models\StudentsSearch;
use Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * StudentsController implements the CRUD actions for Students model.
 */
class StudentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Students models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StudentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Students model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionSuccess($slug)
    {
        $applicationDetails = AdmissionAcademics::find()->where(['ac_application_no' => $slug])->one();
        if (isset($applicationDetails)) {
            $userDetails = Students::find()->where(['id' => $applicationDetails->ac_student_id])->one();
        } else {
            $userDetails = 'empty';
        }
        return $this->render('success', [
            'applicationDetails' => $applicationDetails,
            'userDetails' => $userDetails,
        ]);
    }

    /**
     * Creates a new Students model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $to      = 'iqbal.khan@yopmail.com';
        $subject = 'the subject';
        $message = 'hello';
        $headers = 'From: noreply@bssgurukulam.com' . "\r\n" .
            'Reply-To: noreply@bssgurukulam.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        
        mail($to, $subject, $message, $headers);
        die();
        $model = new Students();
        $parentModel = new StudentParentProfile();
        $achievementModel = new AdmissionAchievement();
        $academicModel = new AdmissionAcademics();
        $studentProfileImage = new StudentProfileImage();
        $studentBSList = new StudentbsList();
        if ($model->load(Yii::$app->request->post()) && $parentModel->load(Yii::$app->request->post()) && $academicModel->load(Yii::$app->request->post()) && $achievementModel->load(Yii::$app->request->post()) && $studentBSList->load(Yii::$app->request->post())) {
            $model->role = '30';
            $model->auth_key = Yii::$app->security->generateRandomString();
            $password = '@Student>123';
            $model->password_hash = Yii::$app->security->generatePasswordHash($password);
            $model->status = '9';
            $model->created_at = strtotime(date('Y-m-d H:i:s'));
            $model->updated_at = strtotime(date('Y-m-d H:i:s'));
            $uploadedFile = UploadedFile::getInstance($studentProfileImage, 'sp_file_name');
            if (!empty($uploadedFile)) {
                $profileName = preg_replace('/\s+/', '', $model->first_name);
                $profileImg = $profileName . '_' . $uploadedFile;
            } else {
                $profileImg = 'user.png';
            }
            if ($model->save()) {
                if (!empty($profileImg)) {
                    $studentProfileImage->sp_student_id  = $model->id;
                    $studentProfileImage->sp_file_name = $profileImg;
                    $studentProfileImage->sp_added_on = date('Y-m-d H:i:s');
                    if ($studentProfileImage->save()) {
                        if (!empty($uploadedFile)) {
                            $uploadedFile->saveAs('theme/img/pro_images/student/' . $profileImg);
                        }
                    }
                }

                $parentModel->pp_student_id = $model->id;
                $parentModel->pp_adminission_status = 'Applied';
                $academicModel->ac_student_id  = $model->id;
                $academicModel->ac_application_no  = 'bss-adm-21-22-' . $model->id;
                $achievementModel->student_id   = $model->id;
                $parentModel->save(false);
                $academicModel->save(false);


                $studentBSDetails = Yii::$app->request->post()['StudentbsList']['list_bs_name'];
                $studentBSDetails1 = Yii::$app->request->post()['StudentbsList']['list_bs_class'];
                $combainedDetails = array_combine($studentBSDetails, $studentBSDetails1);
                foreach ($combainedDetails as $key => $value) {
                    if (!empty($value)) {
                        $studentBSList = new StudentbsList();
                        $studentBSList->list_student_id = $model->id;
                        $studentBSList->list_bs_name    = $key;
                        $studentBSList->list_bs_class   = $value;
                        $studentBSList->save(false);
                    }
                }
                $Achievement = Yii::$app->request->post()['AdmissionAchievement']['competition_level'];
                $Achievement1 = Yii::$app->request->post()['AdmissionAchievement']['competition_items'];
                $Achievement2 = Yii::$app->request->post()['AdmissionAchievement']['competition_position'];
                $Achievement3 = Yii::$app->request->post()['AdmissionAchievement']['competition_grade'];
                for ($i = 0; $i < count($Achievement); $i++) {
                    if ($i > 0) {
                        $achievementModel = new AdmissionAchievement();
                        $achievementModel->student_id = $model->id;
                        $achievementModel->competition_level = $Achievement[$i];
                        $achievementModel->competition_items = $Achievement1[$i];
                        $achievementModel->competition_position = $Achievement2[$i];
                        $achievementModel->competition_grade = $Achievement3[$i];
                        $achievementModel->save(false);
                    }
                }

                $mail_content = 'Hello ' . $model->first_name . ' ' . $model->second_name . "<br>";
                $mail_content .= 'Your application has been submitted successfully. Once the application is approved, you will be shared a username and password to access your account.' . "<br>" . "<br>";
                $mail_content .= "<table style='width:700px;border:1px #e5e5e5 solid;background:#f5f5f5;color:#000000;padding:10px;'> <tbody> <tr style='height: 100px;'> 
                <td style='border:0; padding:5px;background:#f5f5f5;color:#000000;font-size: 20px;'> <img src='http://bssgurukulam.herdzo.com/theme/img/logo.png'> </td> 
                <td style='padding-left:3px;border:0;background:#f5f5f5;color:#000000;font-size: 20px;text-align: right;'> <strong>Admission 2021-22<br>Application Reference No:" . $academicModel->ac_application_no . "</strong></td>
                </tr> 
                <tr style='background:#f5f5f5;height: 30px;'> <td></td></tr> 

                
                <tr style='height: 100px;'> <td style='border:1px #e5e5e5 solid; padding:5px;background:#FFFFFF;color:#000000;font-size: 20px;text-align: center;'> Name: </td> 
                <td style='padding-left:3px;border:1px #e5e5e5 solid;background:#fff;color:#000000;font-size: 20px;text-align: center;'> <strong>" . $model->first_name . ' ' . $model->second_name . "</strong></td> </tr> 
                
                <tr style='height: 100px;'> <td style='border:1px #e5e5e5 solid; padding:5px;background:#FFFFFF;color:#000000;font-size: 20px;text-align: center;'> Father's Name: </td>
                <td style='padding-left:3px;border:1px #e5e5e5 solid;background:#fff;color:#000000;font-size: 20px;text-align: center;'> <strong>" . $parentModel->pp_father_name . "</strong></td> </tr>
                
                <tr style='height: 100px;'> <td style='border:1px #e5e5e5 solid; padding:5px;background:#FFFFFF;color:#000000;font-size: 20px;text-align: center;'> Mother's Name: </td>
                <td style='padding-left:3px;border:1px #e5e5e5 solid;background:#fff;color:#000000;font-size: 20px;text-align: center;'> <strong>" . $parentModel->pp_mother_name . "</strong></td> </tr>
                
                <tr style='height: 100px;'> <td style='border:1px #e5e5e5 solid; padding:5px;background:#FFFFFF;color:#000000;font-size: 20px;text-align: center;'> Mobile: </td>
                <td style='padding-left:3px;border:1px #e5e5e5 solid;background:#fff;color:#000000;font-size: 20px;text-align: center;'> <strong>" . $model->username . "</strong></td> </tr>
                
                <tr style='height: 100px;'> <td style='border:1px #e5e5e5 solid; padding:5px;background:#FFFFFF;color:#000000;font-size: 20px;text-align: center;'> Email: </td>
                <td style='padding-left:3px;border:1px #e5e5e5 solid;background:#fff;color:#000000;font-size: 20px;text-align: center;'> <strong>" . $model->email . "</strong></td> </tr>
                
                <tr style='height: 100px;'> <td style='border:1px #e5e5e5 solid; padding:5px;background:#FFFFFF;color:#000000;font-size: 20px;text-align: center;'> Communication Address: </td>
                <td style='padding-left:3px;border:1px #e5e5e5 solid;background:#fff;color:#000000;font-size: 20px;text-align: center;'> <strong>" . $parentModel->pp_address . "</strong></td> </tr>
                
                <tr style='height: 100px;'> <td style='border:1px #e5e5e5 solid; padding:5px;background:#FFFFFF;color:#000000;font-size: 20px;text-align: center;'> Admission Class: </td>
                <td style='padding-left:3px;border:1px #e5e5e5 solid;background:#fff;color:#000000;font-size: 20px;text-align: center;'> <strong>" . $academicModel->admission_class . "</strong></td> </tr>
                
                <tr style='background:#f5f5f5;height: 30px;'> <td></td> </tr> 
                </tbody></table><br>";
                $mail_content .= "<br><br><br>" . 'Best Regards' . "<br>" . 'BSS Gurukulam.';
                $to = $model->email;
                $from = 'admissions@bssgurukulam.com';
                $subject = "Admission 2021-22";

                $headers =  'MIME-Version: 1.0' . "\r\n";
                $headers .= 'From: ' . $from . ' <' . $from . '>' . "\r\n";
                $headers .= 'From: BSS Gurukulam < noreply@bssgurukulam.com>' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= 'Cc:' . $from . "\r\n";
                mail($to, $subject, $mail_content, $headers);


                /*$transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($parentModel->save() && $academicModel->save() && $achievementModel->save() && $studentBSList->save()) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                } */
                Yii::$app->session->setFlash('success', 'Thank you for registering with us. We will respond to you as soon as possible.');
                return $this->redirect(['success',
                    'slug' => $academicModel->ac_application_no,
                ]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'parentModel' => $parentModel,
                    'achievementModel' => $achievementModel,
                    'academicModel' => $academicModel,
                    'studentProfileImage' => $studentProfileImage,
                    'studentBSList' => $studentBSList
                ]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'parentModel' => $parentModel,
            'academicModel' => $academicModel,
            'achievementModel' => $achievementModel,
            'studentProfileImage' => $studentProfileImage,
            'studentBSList' => $studentBSList
        ]);
    }

    /**
     * Updates an existing Students model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Students model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Students model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Students the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Students::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
