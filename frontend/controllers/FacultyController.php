<?php

namespace frontend\controllers;

use admin\models\Faculty;
use admin\models\ResproDetails;

class FacultyController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $facultyKG = ResproDetails::find()->where(['pd_dp_id' => 1,'pd_status' => 'Active'])->all();
        $facultyPrimary = ResproDetails::find()->where(['pd_dp_id' => 2,'pd_status' => 'Active'])->all();
        $facultyUP = ResproDetails::find()->where(['pd_dp_id' => 3,'pd_status' => 'Active'])->all();
        $facultySecondary = ResproDetails::find()->where(['pd_dp_id' => 4,'pd_status' => 'Active'])->all();
        $facultyHigherSecondary = ResproDetails::find()->where(['pd_dp_id' => 5,'pd_status' => 'Active'])->all();
        $others = ResproDetails::find()->where(['pd_dp_id' => 6,'pd_status' => 'Active'])->all();
        return $this->render('index',[
            'facultyKG' => $facultyKG,
            'facultyPrimary' => $facultyPrimary,
            'facultyUP' => $facultyUP,
            'facultySecondary' => $facultySecondary,
            'facultyHigherSecondary' => $facultyHigherSecondary,
            'others' => $others,
        ]);
    }

}
