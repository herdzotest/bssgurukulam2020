<?php

namespace frontend\controllers;
use admin\models\Events;
use admin\models\EventsSearch;
use yii\web\NotFoundHttpException;

use Yii;
class EventsController extends \yii\web\Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        if(isset($_POST['month']) && isset($_POST['year'])){
            $month = sprintf("%02d", $_POST['month']);
            $current_dt = $_POST['year'].'-'.$month;
            $model = Events::find()->where(['like', 'ev_date', $current_dt])->all();
            if(!empty($model)){
                return $this->render('index', [
                    'model' => $model,
                ]); 
            } else {
                $model = "";
                return $this->render('index', [
                    'model' => $model,
                ]); 
            }
        } else {
            $current_dt = date("Y").'-'.date("m");
            $model = Events::find()->where(['like', 'ev_date', $current_dt])->all();
            if(!empty($model)){
                return $this->render('index', [
                    'model' => $model,
                ]); 
            } else {
                $max = Events::find()->max('ev_date'); 
                $yr = substr($max,0,4);
                $mon = substr($max,5,2);
                $current_dt = $yr.'-'.$mon;
                $model = Events::find()->where(['like', 'ev_date', $current_dt])->all();
                if(!empty($model)){
                    return $this->render('index', [
                        'model' => $model,
                    ]); 
                } else {
                    $model = "";
                    return $this->render('index', [
                        'model' => $model,
                    ]); 
                }
            }
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDetails($slug)
    {
        $model = Events::find()->where(['ev_slug' => $slug])->one();
        return $this->render('details', [
            'model' => $model,
        ]);

    }

}
