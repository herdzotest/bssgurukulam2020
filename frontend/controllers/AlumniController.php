<?php

namespace frontend\controllers;

use Yii;
use admin\models\Alumni;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use admin\models\AlumniCompany;
use admin\models\AlumniProfile;
use admin\models\Cities;
use admin\models\States;
use admin\models\UserImages;
use yii\web\UploadedFile;
use Behat\Gherkin\Exception\Exception;
use frontend\models\AlumniSearch;

/**
 * AlumniController implements the CRUD actions for Alumni model.
 */
class AlumniController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Alumni models.
     * @return mixed
     */
    public function actionIndex()
    {
     
        $searchModel = new AlumniSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Alumni model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }
    public function actionDetails($id)
    {
        $model = Alumni::find()->where(['username' => $id])->one();
        return $this->render('details', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Alumni model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister()
    {
        $model = new Alumni();
        $profilePic = new UserImages();
        $alumniProfile = new AlumniProfile();
        $alumniCompany = new AlumniCompany();
        // echo '<pre>';print_r(Yii::$app->request->post('AlumniProfile'));die();
        if ($model->load(Yii::$app->request->post()) && $alumniProfile->load(Yii::$app->request->post()) && $alumniCompany->load(Yii::$app->request->post())) {
            $model->role = '40';
            $model->auth_key = Yii::$app->security->generateRandomString();
            $password = '@Alumni>123';
            $model->password_hash = Yii::$app->security->generatePasswordHash($password);
            $model->status = '9';
            $model->created_at = strtotime(date('Y-m-d H:i:s'));
            $model->updated_at = strtotime(date('Y-m-d H:i:s'));
            $uploadedFile = UploadedFile::getInstance($profilePic, 'pp_image');
            if (!empty($uploadedFile)) {
                $profileName = preg_replace('/\s+/', '', $model->first_name);
                $profileImg = $profileName . '_' . $uploadedFile;
            } else {
                $profileImg = 'user.png';
            }
            if ($model->save()) {
                if (!empty($profileImg)) {
                    $profilePic->pp_uid = $model->id;
                    $profilePic->pp_image = $profileImg;
                    $profilePic->pp_status = 'Active';
                    if ($profilePic->save()) {
                        if (!empty($uploadedFile)) {
                            $uploadedFile->saveAs('theme/img/pro_images/alumni/' . $profileImg);
                        }
                    }
                }

                $alumniProfile->ap_uid = $model->id;
                $alumniProfile->ap_approved_uid = '0';
                $alumniProfile->ap_class = '0';
                $alumniCompany->ac_uid = $model->id;

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($alumniProfile->save() && $alumniCompany->save()) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
                Yii::$app->session->setFlash('success', 'Thank you for registering with us. We will respond to you as soon as possible.');
                return $this->redirect(['details', 'id' => $model->username]);
            } else {
                return $this->render('register', [
                    'model' => $model,
                    'alumniProfile' => $alumniProfile,
                    'profilePic' => $profilePic,
                    'alumniCompany' => $alumniCompany,
                ]);
            }
        }

        return $this->render('register', [
            'model' => $model,
            'profilePic' => $profilePic,
            'alumniCompany' => $alumniCompany,
            'alumniProfile' => $alumniProfile,
        ]);
    }

    /**
     * Updates an existing Alumni model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }

    //     return $this->render('update', [
    //         'model' => $model,
    //     ]);
    // }

    /**
     * Deletes an existing Alumni model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    public static function actionState()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $country_id = $parents[0];
                $out = States::find()->where(['country_id' => $country_id])->all();
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    public static function actionCity()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $state_id = $parents[0];
                $out = Cities::find()->where(['state_id' => $state_id])->all();
                // the getSubCatList function will query the database based on the
                // cat_id and return an array like below:
                // [
                //    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
                //    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
                // ]
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }
    /**
     * Finds the Alumni model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Alumni the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Alumni::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
