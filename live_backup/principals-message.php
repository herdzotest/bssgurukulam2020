<!-- ##### Breadcumb Area Start ##### -->
<main class="page about-page">
    <section class="title" style="background-image:url(<?php echo base_url(); ?>/public/img/titleweb.jpg);">

        <div class="container">

            <h1>Principal's Message</h1>

        </div>

    </section>
    <section class="page-content">



        <div class="container-full">

            <div class="tab-section">
                <div class="tab-nav ">
                    <ul class="tabul ">
                        <li class="">
                            <a class="active" href="<?php echo base_url(); ?>pages/school">School</a>
                        </li>
                        <li class="">
                            <a href="<?php echo base_url(); ?>pages/mission-vision">Mission and Vision</a>
                        </li>
                        <li class="">
                            <a href="<?php echo base_url(); ?>pages/system-of-school">System of School</a>
                        </li>
                        <li class="">
                            <a href="<?php echo base_url(); ?>pages/hall-of-fame">Hall of Fame</a>
                        </li>
                        <li class="">
                            <a href="<?php echo base_url(); ?>pages/accolades-awards">Accolades and Awards</a>
                        </li>
                        <li class="">
                            <a href="<?php echo base_url(); ?>pages/founder">Founder</a>
                        </li>
                        <!--<li class="">
                                    <a href="gallery.html">Gallery</a>
                                </li>-->
                        <li class="">
                            <a href="<?php echo base_url(); ?>pages/management">Management</a>
                        </li>

                    </ul>

                </div>
                <div class="mob-tab-nav tabswiper">
                    <ul class="tabul swiper-wrapper">
                        <li class="swiper-slide">
                            <a class="active" href="<?php echo base_url(); ?>pages/school">School</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="<?php echo base_url(); ?>pages/mission-vision">Mission and Vision</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="<?php echo base_url(); ?>pages/system-of-school">System of School</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="<?php echo base_url(); ?>pages/hall-of-fame">Hall of Fame</a>
                        </li>
                        <li class="swiper-slide">
                            <a href="<?php echo base_url(); ?>pages/accolades-awards">Accolades and Awards</a>
                        </li>
                        <li class="swiper-slide swiper-slide-active">
                            <a href="<?php echo base_url(); ?>pages/founder">Founder</a>
                        </li>
                        <!--<li class="swiper-slide">
                                            <a href="gallery.html">Gallery</a>
                                        </li>-->
                        <li class="swiper-slide">
                            <a href="<?php echo base_url(); ?>pages/management">Management</a>
                        </li>
                        <li class="swiper-slide dummyli"></li>

                    </ul>
                </div>
                <div class="tab-content school">


                    <h2> PRINCIPAL'S MESSAGE</h2>

                    <div class="page-row">
                        <div class="page-column column3">


                            <!-- Added by Iqbal to show title as page title instead of menu title as title on 27 May 2020 -->
                            <!-- <h2> </h2>  -->


                            <p><img alt="" src="<?php echo base_url();?>assets/template_media/images/Vijayan%20Maash(1).jpg"></p>

                            <p><span style="font-size:14px"><strong>When we&nbsp;Gurukulam look forward we could see innumerable shining stars with their strings in Gurukulam glowing various walks of Life. Of course we bring out full marks, full A+ holders even Unbeatable and Unbelievable results in Co-curricular activities. But the most important Hall-Mark of human being is being human. Gurukulam children are experiencing the most virtuous deeds which leads them to become value integrated personalities having great commitment to maintain the cosmos of the entire Universe.</strong></span></p>



                        </div>

                    </div>


                </div>

            </div>

            <!-- swiper2 -->


        </div>
    </section>
</main>