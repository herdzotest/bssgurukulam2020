<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
        $this->load->library('session');
        $this->load->model('Students_model');
        $this->load->model('Home_model');
        $this->load->helper('url');
    }

    private function show_display($template, $vars = array()) {
        $this->load->view('header', $vars);
        $this->load->view($template, $vars);
        $this->load->view('footer', $vars);
    }

    function showfrontPage($data, $pagename) {

        $about_menu_id = (int) $this->Home_model->get_first_submenu(2);
        $data['about'] = $this->Home_model->main_menu_content($about_menu_id);
        $data['about_Submenus'] = $this->Home_model->static_submenu_list(2);

        $faculty_menu_id = (int) $this->Home_model->get_first_submenu(4);
        $data['faculty'] = $this->Home_model->main_menu_content($faculty_menu_id);
        $data['faculty_Submenus'] = $this->Home_model->static_submenu_list(4);

        $academic_menu_id = (int) $this->Home_model->get_first_submenu(5);
        $data['academic'] = $this->Home_model->main_menu_content($academic_menu_id);
        $data['academic_Submenus'] = $this->Home_model->static_submenu_list(5);

        $admission = (int) $this->Home_model->get_first_submenu(6);
        $data['admission'] = $this->Home_model->main_menu_content($admission);
        $data['admission_Submenus'] = $this->Home_model->static_submenu_list(6);
        
        $achievement_menu_id = (int) $this->Home_model->get_first_submenu(8);
        $data['achievement'] = $this->Home_model->main_menu_content($achievement_menu_id);
        $data['achievement_Submenus'] = $this->Home_model->static_submenu_list(8);
        
        $management_menu_id = (int) $this->Home_model->get_first_submenu(3);
        $data['management'] = $this->Home_model->main_menu_content($achievement_menu_id);
        $data['management_Submenus'] = $this->Home_model->static_submenu_list(3);

        $calender_menu_id = (int) $this->Home_model->get_first_submenu(9);
        $data['calender'] = $this->Home_model->main_menu_content($calender_menu_id);
        $data['calender_Submenus'] = $this->Home_model->static_submenu_list(9);

        $contact_menu_id = (int) $this->Home_model->get_first_submenu(11);
        $data['contact'] = $this->Home_model->main_menu_content($contact_menu_id);
        $data['contact_Submenus'] = $this->Home_model->static_submenu_list(11);

        $facility = (int) $this->Home_model->get_first_submenu(7);
        $data['facility'] = $this->Home_model->main_menu_content($facility);
        $data['facility_Submenus'] = $this->Home_model->static_submenu_list(7);
        
        $highlights = (int) $this->Home_model->get_first_submenu(74);
        $data['highlights'] = $this->Home_model->main_menu_content($highlights);

        $data['events'] = $this->Home_model->event_details();
        $data['calender_details'] = $this->Home_model->calender_details();
        $data['facilities'] = $this->Home_model->facility_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();

        $data['princy_msg'] = $this->Home_model->princy_msg();
        $data['articles'] = $this->Home_model->articles_details();
        $data['star'] = $this->Home_model->star_details();
        $data['active'] = "1";
        $data['home_submenu'] = $this->Home_model->static_submenu_list(1);

        $data['main_menus'] = $this->Home_model->main_menu_details();
		$data['founder_content'] = $this->Home_model->static_submenu_details(array('submenu_id' => 103, 'parent_id' => 2));
		
        $this->show_display($pagename, $data);
    }

    public function index() {
        $this->load->helper('url');


        $data['banneraboveNews'] = $this->Home_model->banner_details(2);
        $data['bannerWelcomeBSS'] = $this->Home_model->banner_details(1);
        $data['arts'] = $this->Home_model->arts_details();
		
		$data['arts_home_details'] = $this->Home_model->arts_details_home();
		
        $data['photo'] = $this->Home_model->photo_details();
        $data['video'] = $this->Home_model->video_details();



        $data['events'] = $this->Home_model->event_details();
        $data['facilities'] = $this->Home_model->facility_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['book_of_the_week'] = $this->Home_model->book_details();
        $data['management_committe'] = $this->Home_model->management_committe();
        $data['visiters_comments'] = $this->Home_model->visiters_comments();
        $data['alumni'] = $this->Home_model->AlumniList(0,10);
        $data['scroll_news'] = $this->Home_model->scroll_news();
        //var_dump($data['alumni']);
        $data['calender_details'] = $this->Home_model->calender_details();
        //var_dump( $data['management_committe']);exit;
        $data['princy_msg'] = $this->Home_model->princy_msg();
        $data['articles'] = $this->Home_model->articles_details();
        $data['star'] = $this->Home_model->star_details();
        $data['active'] = "1";
        $data['home_submenu'] = $this->Home_model->static_submenu_list(1);
        $data['mainbanner'] = $this->Home_model->main_banner_details();

        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['system_of_school'] = $this->Home_model->static_submenu_details(array('submenu_id' => 15, 'parent_id' => 2));

        //var_dump( $data);exit;
        // $this->show_display("Home", $data);
        $this->showfrontPage($data, 'Home');
    }

    public function home_submenu() {
        $data['parent_id'] = $this->input->get('parent_id');
        $data['submenu_id'] = $this->input->get('submenu_id');
        $data['active'] = (int) $data['parent_id'];
        $data['active_name'] = $this->Home_model->get_active_menu_name($data['submenu_id']);
        $data['active_slug'] = $this->Home_model->get_active_slug_name($data['parent_id']);
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['submenu_detail'] = $this->Home_model->static_submenu_details($data);
        //$this->show_display("innerstatic", $data);
        $this->showfrontPage($data, 'innerstatic');
    }

    public function academics($submenu_id) {
        // var_dump(func_get_args());exit;
        $data['submenu_id'] = $submenu_id;
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['active'] = 5;
        $data['active_submenu'] = (int) $submenu_id;
        $data['details'] = $this->Home_model->academics_management($data['submenu_id']);
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['calender_details'] = $this->Home_model->calender_details();
        $data['articles'] = $this->Home_model->articles_details();
        $data['active_submenu_name'] = $this->Home_model->get_section_name($data['active_submenu']);
		$data['photo'] = $this->Home_model->photo_details();
		$data['submenus'] = $this->Home_model->static_submenu_list(5);
		 
        $this->showfrontPage($data, 'academics');
    }

    public function Manage_committee() {
        // echo"i,mmm here";exit;
        $data['active'] = ' ';
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();
        $data['heading'] = "Management Committee";
        $data['calender_details'] = $this->Home_model->calender_details();
        $data['Mcommittee'] = $this->Home_model->managementCom_details();
		$data['photo'] = $this->Home_model->photo_details();
		
        //var_dump($data['Mcommittee'] );exit;
        // $this->show_display("management_committee", $data);
        $this->showfrontPage($data, 'management_committee');
    }

    public function Manage_committeeDetails($slug = NULL) {
        // echo"i,mmm here";exit;

        $pieces = explode('-', $slug);
        //  var_dump($pieces);exit;
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('pagination');
        $teachers_committee_id = $pieces[1];
        $data['active'] = ' ';
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();
        $data['heading'] = "Management Committee";
        $data['management_committeenames'] = $this->Home_model->management_committee_names();



        /* $config["base_url"] = base_url() . "managecommittee/".$slug;
          $config['total_rows'] = $this->Home_model->gettotal_committeeActivities($teachers_committee_id);
          $config['per_page'] = 2; */
        $config["per_page"] = 3;
        $config["total_rows"] = $this->Home_model->gettotal_committeeActivities($teachers_committee_id);
        $config['first_url'] = base_url() . "managecommittee/" . $slug . "/0";
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['uri_segment'] = 3;
        $config["base_url"] = base_url() . "managecommittee/" . $slug;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['Mcommittee'] = $this->Home_model->managementCom_details($teachers_committee_id, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
		$data['photo'] = $this->Home_model->photo_details();
		
        // var_dump( $page);
        //var_dump( $data['Mcommittee']);exit; 
        // $this->show_display("management_committee", $data);
		
		$data['sub_menu'] = $this->Home_model->static_submenu_list(4);
        $this->showfrontPage($data, 'management_committee');
    }

    public function Visiters_comments() {
        // echo"i,mmm here";exit;
        $data['active'] = ' ';
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();
        $data['calender_details'] = $this->Home_model->calender_details();
        $data['heading'] = "Management Committee";
        $data['viisters_com'] = $this->Home_model->visitersCom_details();
		$data['photo'] = $this->Home_model->photo_details();
		
        //($data['Mcommittee'] );exit;
        //$this->show_display("visitersComments", $data);
        $this->showfrontPage($data, 'visitersComments');
    }

    public function Search() {
        $data['search'] = $this->input->post('search');
        $data['search'] = $this->Home_model->search_details($data['search']);
        if ($data['search']) {
            redirect('Home/submenu_management/' . $data['search']['slug']);
        } else {
            redirect('Home/submenu_management/home');
        }
    }

    public function book_of_the_week() {
        // echo"i,mmm here";exit;
        $data['active'] = ' ';
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();
        $data['heading'] = "Management Committee";
        $data['books'] = $this->Home_model->bookofweek_details();
        $data['calender_details'] = $this->Home_model->calender_details();
		$data['photo'] = $this->Home_model->photo_details();
		
        //($data['Mcommittee'] );exit;
        //  $this->show_display("books_of_the_week", $data);
        $this->showfrontPage($data, 'books_of_the_week');
    }

    public function Useful_links() {
        $data['active'] = ' ';
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();
        $data['heading'] = "Management Committee";
        $data['usefullinks'] = $this->Home_model->usefullinks_details();
        $data['calender_details'] = $this->Home_model->calender_details();
        //($data['Mcommittee'] );exit;
        // $this->show_display("usefullinks", $data);
        $this->showfrontPage($data, 'usefullinks');
    }

    public function event_details($slug) {
        $data['slug'] = $slug;
        $pieces = explode('_', $slug);
        $data['calender_details'] = $this->Home_model->calender_details();
        /* var_dump($pieces);
          exit; */
        switch ($pieces[0]) {
            case 'Events':
                $mode = 'Events';
                $data['active_submenu_name'] = 'Events';
                $data['submenu_id'] = 4;
                $data['eventsdetails'] = $this->Home_model->events_fulldetails($pieces[1], $mode);

                break;
            case 'arts' :
                $data['submenu_id'] = 1;
                $data['active_submenu_name'] = 'Charity';
                $data['eventsdetails'] = $this->Home_model->arts_fulldetails($pieces[1]);

                break;
            case 'Articles' :
                $data['active_submenu_name'] = 'Articles';
                $data['submenu_id'] = 2;
                $mode = 'Articles';
                $data['eventsdetails'] = $this->Home_model->events_fulldetails($pieces[1], $mode);

                break;
            case 'BloominBuds':
                $data['active_submenu_name'] = 'Blooming Buds';
                $data['submenu_id'] = 3;
                $mode = 'Bloomin Buds';
                $data['eventsdetails'] = $this->Home_model->events_fulldetails($pieces[1], $mode);

                break;
            case'News':
                $mode = 'News';
                $data['active_submenu_name'] = 'News';
                $data['submenu_id'] = 5;
                $data['eventsdetails'] = $this->Home_model->events_fulldetails($pieces[1], $mode);

                break;
            case 'photo':
                $data['submenu_id'] = 6;
                $data['active_submenu_name'] = 'Photos';
                $data['eventsdetails'] = $this->Home_model->photo_fulldetails($pieces[1]);

                break;
            case 'Videos':
                $data['submenu_id'] = 8;
                $data['active_submenu_name'] = 'Videos';
                $data['eventsdetails'] = $this->Home_model->Video_fulldetails($pieces[1]);
                break;
            case 'stars':
                $data['submenu_id'] = 7;
                $data['active_submenu_name'] = 'Achievements';
                $data['eventsdetails'] = $this->Home_model->star_fulldetails($pieces[1]);
                break;
            case 'upload':
                $data['submenu_id'] = 1;
                $data['active_submenu_name'] = 'Arts';
                $data['eventsdetails'] = $this->Home_model->uploadstudent($pieces[1]);
                break;
        }

        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['active'] = 'img';
        $data['active_submenu'] = $data['submenu_id'];
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();
		$data['photo'] = $this->Home_model->photo_details();
		
        // $this->show_display("eventdetails", $data);
        // var_dump($data['eventsdetails']);exit;
        $this->showfrontPage($data, 'eventdetails');

        // var_dump($data);exit;
    }

    public function faculty_managemnt($slug) {
        $data['slug'] = $slug;
        $data['active'] = 4;
        $data['active_submenu'] = 27;
        $data['parent_id'] = 4;
        $data['submenu_id'] = 27;
        $data['active_name'] = $this->Home_model->get_active_menu_name($data['parent_id']);
        $data['active_slug'] = $this->Home_model->get_active_slug_name($data['parent_id']);
        $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(27);
        $data['calender_details'] = $this->Home_model->calender_details();

        $data['sub_menu'] = $this->Home_model->static_submenu_list(4);
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();

        $pieces = explode('-', $slug);
        $data['teacher_dtls'] = $this->Home_model->teachers_details(1, $pieces[1]);
		$data['photo'] = $this->Home_model->photo_details();
		
        //var_dump($data['teacher_dtls'][0]);exit;
        //$this->show_display("facultyDetails", $data);
        $this->showfrontPage($data, 'facultyDetails');
    }

    public function adminstration_management($slug) {
        $data['slug'] = $slug;
        $data['active'] = 3;
        $data['active_submenu'] = 23;
        $data['parent_id'] = 3;
        $data['submenu_id'] = 23;
        $data['calender_details'] = $this->Home_model->calender_details();
        $data['active_name'] = $this->Home_model->get_active_menu_name($data['parent_id']);
        $data['active_slug'] = $this->Home_model->get_active_slug_name($data['parent_id']);
        $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(23);

        $data['sub_menu'] = $this->Home_model->static_submenu_list(3);
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();

        $pieces = explode('-', $slug);
        $data['teacher_dtls'] = $this->Home_model->teachers_details(2, $pieces[1]);
		$data['photo'] = $this->Home_model->photo_details();
		
        //var_dump($data);exit;
        // $this->show_display("admindetails", $data);
        $this->showfrontPage($data, 'admindetails');
    }

    public function submenu_management($slug) {
		
        $this->load->library('recaptcha');
        $this->load->helper('form');
        $data['slug'] = $slug;
		
		if($slug == 'comingSoon') {
			$data['parent_id'] = '';
			$data['submenu_id'] = '';
			
			$this->showfrontPage($data, 'comingSoon');
        } else {
		
			$slug_db = $this->Home_model->get_id_fromSlug($slug);
			if ($slug_db) {
				foreach ($slug_db as $key => $value) {
					$data['parent_id'] = (int) $value['parent_id'];
					$data['submenu_id'] = (int) $value['menu_id'];
				}
			} else {
				redirect('Pagenotfound/index');
			}
		}
        $data['calender_details'] = $this->Home_model->calender_details();


        $data['active_name'] = $this->Home_model->get_active_menu_name($data['parent_id']);
        $data['active_slug'] = $this->Home_model->get_active_slug_name($data['parent_id']);
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();
		$data['photo'] = $this->Home_model->photo_details();
		
        /* var_dump($data['parent_id']);
          var_dump($data['submenu_id']);exit; */
		if (($data['parent_id'] == 0) && ($data['submenu_id'] == 1)) {
            $this->index();
        } else if (($data['parent_id'] == 6) && ($data['submenu_id'] == 34)) {
            $data['active'] = 6;
            $data['active_submenu'] = 34;
            $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(34);
            $data['sub_menu'] = $this->Home_model->static_submenu_list(6);
            $data['appform'] = $this->Home_model->application_formdata();
            //$this->show_display("application_form", $data);
            $this->showfrontPage($data, 'application_form');
        } else if (($data['parent_id'] == 11) && ($data['submenu_id'] == 65)) {
            $data['active'] = 11;
            $data['active_submenu'] = 65;
            $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(65);
            $data['sub_menu'] = $this->Home_model->static_submenu_list(11);
            $data['submenu_detail'] = $this->Home_model->static_submenu_details($data);
            //$this->show_display("googlemap", $data);
            $this->showfrontPage($data, 'googlemap');
        } else if (($data['parent_id'] == 11) && ($data['submenu_id'] == 62)) {
            $data['active'] = 11;
            $data['active_submenu'] = 62;
            $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(62);
            $data['sub_menu'] = $this->Home_model->static_submenu_list(11);
            // var_dump($data);exit;
            //$this->show_display("googlemap", $data);
            $data['recaptcha'] = $this->recaptcha->recaptcha_get_html();
            // var_dump($data['recaptcha']);exit;
            $this->showfrontPage($data, 'contact-us-form');
        } else if (($data['parent_id'] == 3) && ($data['submenu_id'] == 23)) {
            $data['active'] = 3;
            $data['active_submenu'] = 23;
            $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(23);
            $data['sub_menu'] = $this->Home_model->static_submenu_list(3);
            $data['submenu_detail'] = $this->Home_model->static_submenu_details($data);

            $data['principal'] = $this->Home_model->administration_management('President');
            $data['vice_principal'] = $this->Home_model->administration_management('Treasurer');
            $data['adminofficer'] = $this->Home_model->administration_management('Secretary');
            $data['financeofficer'] = $this->Home_model->administration_management('Executive Officer');
            $this->showfrontPage($data, 'administration');
        } else if (($data['parent_id'] == 11) && ($data['submenu_id'] == 64)) {
            $data['active'] = 11;
            $data['active_submenu'] = 64;
            $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(64);
            $data['sub_menu'] = $this->Home_model->static_submenu_list(11);
            $data['submenu_detail'] = $this->Home_model->static_submenu_details($data);
            $data['directory'] = $this->Home_model->directory_management(2);

            $data['recaptcha'] = $this->recaptcha->recaptcha_get_html();
            $this->showfrontPage($data, 'directory');
        } else if (($data['parent_id'] == 3) && ($data['submenu_id'] == 22)) {
            $data['active'] = 3;

            $data['active_submenu'] = 22;
            $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(22);
            $data['sub_menu'] = $this->Home_model->static_submenu_list(3);
            $data['submenu_detail'] = $this->Home_model->static_submenu_details($data);
            $data['details'] = $this->Home_model->faculty_management(2);
            $this->showfrontPage($data, 'corecommitte');
        } else if (($data['parent_id'] == 4) && ($data['submenu_id'] == 27)) {
            $data['active'] = 4;
            $data['active_submenu'] = 27;
            $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(27);
            $data['sub_menu'] = $this->Home_model->static_submenu_list(4);
            $data['submenu_detail'] = $this->Home_model->static_submenu_details($data);
            $data['dtls_kg'] = $this->Home_model->teachers_management(1);
            $data['dtls_primary'] = $this->Home_model->teachers_management(2);
            $data['dtls_UprPrmry'] = $this->Home_model->teachers_management(3);
            $data['dtls_seconadry'] = $this->Home_model->teachers_management(4);
            $data['dtls_sscdry'] = $this->Home_model->teachers_management(5);
            $data['dtls_nonteach'] = $this->Home_model->teachers_management(6);
            $this->showfrontPage($data, 'faculty');
        } else if (($data['parent_id'] == 4) && ($data['submenu_id'] == 28)) {
            $data['active'] = 4;
            $data['active_submenu'] = 28;
            $data['active_submenu_name'] = $this->Home_model->get_active_menu_name(28);
            $data['sub_menu'] = $this->Home_model->static_submenu_list(4);
            $data['submenu_detail'] = $this->Home_model->static_submenu_details($data);
            $data['management_committe'] = $this->Home_model->management_committe();
            $this->Manage_committeeDetails($data['management_committe'][0]['slug']);
        } else if (($data['parent_id'] == 0) && ($data['submenu_id'] > 0)) {
            $data['active_submenu'] = (int) $this->Home_model->get_first_submenu($data['submenu_id']);
            $data['active'] = $data['submenu_id'];
            if ($data['active_submenu']) {
                $data['active_submenu'] = (int) $data['active_submenu'];
                $data['active_submenu_name'] = $this->Home_model->get_active_menu_name($data['active_submenu']);
                $data['sub_menu'] = $this->Home_model->static_submenu_list($data['submenu_id']);
                $data['parent_id'] = $data['submenu_id'];
                $data['submenu_id'] = $data['active_submenu'];
                $data['active_name'] = $this->Home_model->get_active_menu_name($data['parent_id']);
                $data['active_slug'] = $this->Home_model->get_active_slug_name($data['parent_id']);
                $data['submenu_detail'] = $this->Home_model->static_submenu_details($data);
            } else {
                $data['menu_content'] = $this->Home_model->main_menu_content($data['submenu_id']);
                $data['heading'] = $this->Home_model->get_active_menu_name($data['submenu_id']);
            }
            $this->showfrontPage($data, 'innerstatic');
        } else if (($data['parent_id'] > 0) && ($data['submenu_id'] > 0)) {
            $data['active'] = $data['parent_id'];
            $data['active_submenu'] = $data['submenu_id'];

            $data['active_submenu_name'] = $this->Home_model->get_active_menu_name($data['active_submenu']);
            $data['sub_menu'] = $this->Home_model->static_submenu_list($data['parent_id']);
            $data['submenu_detail'] = $this->Home_model->static_submenu_details($data);
			$data['heading'] = $this->Home_model->get_active_menu_name($data['submenu_id']);

            $this->showfrontPage($data, 'innerstatic');
        }
    }

    public function Images_management($submenu_id, $options = NULL) {
        $data['submenu_id'] = $submenu_id;
        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['active'] = 'img';
        // var_dump($data);exit;
        $data['calender_details'] = $this->Home_model->calender_details();
        $data['events'] = $this->Home_model->event_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();
        $data['articles'] = $this->Home_model->articles_details();
        $data['active_submenu'] = (int) $submenu_id;

        $config = array();
        $config['total_rows'] = '';
        //PAGINATION

        $options = 6;

        $data['filter'] = 'desc';
        $filter = 'desc';

        $config["per_page"] = $options;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        //  var_dump($page);exit;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul><!--pagination-->';
        $config['first_link'] = ' First ';
        $config['first_tag_open'] = '<li class="page-item prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = ' Last ';
        $config['last_tag_open'] = '<li class="page-item next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = ' Next ';
        $config['next_tag_open'] = '<li class="page-item next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = ' Prev ';
        $config['prev_tag_open'] = '<li class="page-item prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['uri_segment'] = 3;
		
        switch ($submenu_id) {
            case '1': $data['active_submenu_name'] = 'Charity';
                $data['orderby'] = 'art_image_id';
                $order = $data['orderby'];
                $total = $this->Home_model->get_total_data('arts', '1');
                $config["total_rows"] = $total;
                $config['first_url'] = base_url() . 'pages/arts/0';
                $config["base_url"] = base_url() . "pages/arts";
                $data['details'] = $this->Home_model->get_all_imagedtls($config["per_page"], $page, $data, 'arts', 'arts');
                break;
            case '2':$data['active_submenu_name'] = 'Articles';
                $data['orderby'] = 'event_images_id';
                $order = $data['orderby'];
                $total = $this->Home_model->get_total_data('widget', 'Articles');
                $config["total_rows"] = $total;
                $config['first_url'] = base_url() . 'pages/articles/0';
                $config["base_url"] = base_url() . "pages/articles";
                $data['details'] = $this->Home_model->get_all_imagedtls($config["per_page"], $page, $data, 'widget', 'Articles');
                break;
            case '3':$data['active_submenu_name'] = 'Blooming Buds';
                $data['orderby'] = 'event_images_id';
                $order = $data['orderby'];
                $total = $this->Home_model->get_total_data('widget', 'Bloomin Buds');
                $config["total_rows"] = $total;
                $config['first_url'] = base_url() . 'pages/bloomingbuds/0';
                $config["base_url"] = base_url() . "pages/bloomingbuds";
                $data['details'] = $this->Home_model->get_all_imagedtls($config["per_page"], $page, $data, 'widget', 'Bloomin Buds');

                break;
            case '4':$data['active_submenu_name'] = 'Events';
                $data['orderby'] = 'event_images_id';
                $order = $data['orderby'];
                $total = $this->Home_model->get_total_data('widget', 'Events');
                $config["total_rows"] = $total;
                $config['first_url'] = base_url() . 'pages/events/0';
                $config["base_url"] = base_url() . "pages/events";
                $data['details'] = $this->Home_model->get_all_imagedtls($config["per_page"], $page, $data, 'widget', 'Events');
                break;
            case '5':$data['active_submenu_name'] = 'News';
                $data['orderby'] = 'event_images_id';
                $order = $data['orderby'];
                $total = $this->Home_model->get_total_data('widget', 'News');
                $config["total_rows"] = $total;
                $config['first_url'] = base_url() . 'pages/news/0';
                $config["base_url"] = base_url() . "pages/news";
                $data['details'] = $this->Home_model->get_all_imagedtls($config["per_page"], $page, $data, 'widget', 'News');
                break;
            case '6':$data['active_submenu_name'] = 'Photos';
                $data['orderby'] = 'photo_images_id';
                $order = $data['orderby'];
                $total = $this->Home_model->get_total_data('photo', '1');
                $config["total_rows"] = $total;
                $config['first_url'] = base_url() . 'pages/photos/0';
                $config["base_url"] = base_url() . "pages/photos";
                $data['details'] = $this->Home_model->get_all_imagedtls($config["per_page"], $page, $data, 'photo', 'photo');
                break;
            case '7':$data['active_submenu_name'] = 'Achievements';
                $data['orderby'] = 'star_id';
                $order = $data['orderby'];
                $total = $this->Home_model->get_total_data('stars', '1');
                $config["total_rows"] = $total;
                $config['first_url'] = base_url() . 'pages/starofweek/0';
                $config["base_url"] = base_url() . "pages/starofweek";
                $data['details'] = $this->Home_model->get_all_imagedtls($config["per_page"], $page, $data, 'stars', 'stars');
                break;
            case '8':$data['active_submenu_name'] = 'Videos';
                $data['orderby'] = 'video_id';
                $order = $data['orderby'];
                $total = $this->Home_model->get_total_data('Videos', '1');
                $config["total_rows"] = $total;
                $config['first_url'] = base_url() . 'pages/videos/0';
                $config["base_url"] = base_url() . "pages/videos";
                $data['details'] = $this->Home_model->get_all_imagedtls($config["per_page"], $page, $data, 'Videos', 'Videos');
                break;
        }

        // $data['details'] = $this->Home_model->Image_management($data['submenu_id']);
		
		$config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
		$data['photo'] = $this->Home_model->photo_details();

        // var_dump($config);
        // var_dump($data["links"]);exit;
        // var_dump($data);exit;
        //var_dump( $data['details']);exit;
        //$this->show_display("innerdynamic", $data);
        $this->showfrontPage($data, 'innerdynamic');



        $this->pagination->initialize($config);
    }

    public function login_management() {
        $this->load->helper('form');

        $about_menu_id = (int) $this->Home_model->get_first_submenu(2);
        $data['about'] = $this->Home_model->main_menu_content($about_menu_id);
        $admission = (int) $this->Home_model->get_first_submenu(6);
        $data['admission'] = $this->Home_model->main_menu_content($admission);
        $facility = (int) $this->Home_model->get_first_submenu(7);
        $data['facility'] = $this->Home_model->main_menu_content($facility);
        $highlights = (int) $this->Home_model->get_first_submenu(74);
        $data['highlights'] = $this->Home_model->main_menu_content($highlights);
        $data['events'] = $this->Home_model->event_details();
        $data['calender_details'] = $this->Home_model->calender_details();
        $data['facilities'] = $this->Home_model->facility_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();

        $data['princy_msg'] = $this->Home_model->princy_msg();
        $data['articles'] = $this->Home_model->articles_details();
        $data['star'] = $this->Home_model->star_details();
        $data['active'] = "1";
        $data['home_submenu'] = $this->Home_model->static_submenu_list(1);

        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['login'] = 1;
        //$this->show_display("student_login", $data);
        $this->showfrontPage($data, 'student_login');
    }

    public function AlumniDtls_managemnt($slug) {
        $data['slug'] = $slug;
        $pieces = explode('-', $slug);
        $data['alumni_dtls'] = $this->Home_model->AlumniDtls_managemnt($pieces[1]);
       // var_dump($data['alumni_dtls']);exit;
        $this->showfrontPage($data, 'Alumni-details');
    }

    public function SaveContactform() {
		
        $this->load->library('recaptcha');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('email');
        $insert = $this->input->post('contact');
        $data=array();
        $recaptcha = $this->input->post('recaptcha_response_field');
        if (!empty($recaptcha)) {
            $this->recaptcha->recaptcha_check_answer(
                    $_SERVER['REMOTE_ADDR'], $this->input->post('recaptcha_challenge_field'), $this->input->post('recaptcha_response_field'));
        }
        $response = $this->recaptcha->getIsValid();
        if ($response) {
            $data['success'] = $this->Home_model->insert_contactForms($insert, 'Contact');
        }

        // isset($data['success'])

        if ($this->input->post('contact')) {

            //echo '111111111'; die();

            $this->session->set_flashdata('contact_success', 'Contact Saved Successfully.');

            /*
            $config = Array(
                'protocol' => 'sendmail',
                    'smtp_host' => 'your domain SMTP host',
                    'smtp_port' => 25,
                    'smtp_user' => 'jatayu_uat@saturn.in',
                    'smtp_pass' => 'saturn.in',
                    'smtp_timeout' => '4',
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1'
            );
            $data1 = array(
                'name' => $insert['first_name'] . " " . $insert['last_name'],
                'Mobile' => $insert['mobile'],
                'Email' => $insert['email_id'],
                'Subject' => $insert['enquiry_subject'],
                'Comments' => $insert['comments']
            );
            $this->load->library('email', $config);
            //$emailto = 'contact@bssgurukulam.com';
			
            $emailto = 'satheesh.b@saturn.in ';

            $this->email->set_newline("\r\n");
            $this->email->from('support@saturn.in', 'Bss Gurukulam');
            $this->email->to($emailto);
            $this->email->subject('Enquiry through Contact Us form: Bss Portal');
            $message = $this->load->view('contact-email', $data1, TRUE);
            $this->email->message($message);
            $mailtrue = $this->email->send();

            if (!$mailtrue){
                var_dump($this->email->print_debugger());
            }
            */
        } else {

            //echo '2222222222'; die();
			
           // $this->session->set_flashdata('contact_failure', 'Sorry an error occcured.');
        }
        //$data['recaptcha'] = $this->recaptcha->recaptcha_get_html();
             
        $this->showfrontPage($data, 'contact-us-form');
    }

    public function SaveDirectoryform() {
        $this->load->library('recaptcha');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('email');
        $data='';
        $insert = $this->input->post('directory');
        $recaptcha = $this->input->post('recaptcha_response_field');
        if (!empty($recaptcha)) {
            $this->recaptcha->recaptcha_check_answer(
                    $_SERVER['REMOTE_ADDR'], $this->input->post('recaptcha_challenge_field'), $this->input->post('recaptcha_response_field'));
        }
        $response = $this->recaptcha->getIsValid();
        if ($response) {
            $data['success'] = $this->Home_model->insert_contactForms($insert, 'Contact');
        }
        if ($data['success']) {
            $config = Array(
                'protocol' => 'sendmail',
                'smtp_host' => 'your domain SMTP host',
                'smtp_port' => 25,
                'smtp_user' => 'bss_uat@saturn.in',
                'smtp_pass' => 'saturn.in',
                'smtp_timeout' => '4',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1'
            );
            $data1 = array(
                'name' => $insert['first_name'] . " " . $insert['last_name'],
                'Mobile' => $insert['mobile'],
                'Email' => $insert['email_id'],
                'Subject' => $insert['enquiry_subject'],
                'Comments' => $insert['comments']
            );
            $this->load->library('email', $config);
            $emailto = 'teachers@bssgurukulam.com';
            $this->email->set_newline("\r\n");
            $this->email->from('support@saturn.in', 'Bss Gurukulam');
            // $emailto = 'almoayyedusedcars@gmail.com';
			
            $emailto = 'satheesh.b@saturn.in ';
			 
           // $this->email->to($emailto);
            $this->email->subject('Enquiry through Directory contact us form: Bss Portal');
            $message = $this->load->view('contact-email', $data1, TRUE);
            $this->email->message($message);
            $mailtrue = $this->email->send();
            $this->session->set_flashdata('directory_success', 'Contact Saved Successfully.');
        } else {
            $this->session->set_flashdata('directory_failed', 'Sorry an error occcured.');
        }
         $data['recaptcha'] = $this->recaptcha->recaptcha_get_html();
        $this->showfrontPage($data, 'directory');
    }

    public function Alumni_form() {
        $this->load->helper('form');

        $about_menu_id = (int) $this->Home_model->get_first_submenu(2);
        $data['about'] = $this->Home_model->main_menu_content($about_menu_id);
        $admission = (int) $this->Home_model->get_first_submenu(6);
        $data['admission'] = $this->Home_model->main_menu_content($admission);
        $facility = (int) $this->Home_model->get_first_submenu(7);
        $data['facility'] = $this->Home_model->main_menu_content($facility);
        $highlights = (int) $this->Home_model->get_first_submenu(74);
        $data['highlights'] = $this->Home_model->main_menu_content($highlights);
        $data['events'] = $this->Home_model->event_details();
        $data['calender_details'] = $this->Home_model->calender_details();
        $data['facilities'] = $this->Home_model->facility_details();
        $data['news'] = $this->Home_model->news_details();
        $data['blooming_buds'] = $this->Home_model->blloming_details();

        $data['princy_msg'] = $this->Home_model->princy_msg();
        $data['articles'] = $this->Home_model->articles_details();
        $data['star'] = $this->Home_model->star_details();
        $data['active'] = "1";
        $data['home_submenu'] = $this->Home_model->static_submenu_list(1);

        $data['main_menus'] = $this->Home_model->main_menu_details();
        $data['login'] = 1;
		
		$data['photo'] = $this->Home_model->photo_details();

        //$this->show_display("alumni-form", $data);
        $this->showfrontPage($data, 'alumni-form');
    }

    public function Alumni_List() {
        $this->load->helper('form');
        $data = '';
        $this->load->library('pagination');
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $options = 2;

        $total = $this->Home_model->gettotal_alumniList();
        if ($page == 0) {
            $options = 0;
        }
        $config["per_page"] = $options;
        $config["total_rows"] = $total;
        $config['first_url'] = base_url() . 'alumni/list/0';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['uri_segment'] = 3;
        $config["base_url"] = base_url() . "alumni/list";
        $this->pagination->initialize($config);
        
        //echo $page;exit;

        $data_['alumniList'] = $this->Home_model->AlumniList($config["per_page"], $page);

        $data_["links"] = $this->pagination->create_links();
        //var_dump($data['alumniList']);exit;
        $this->showfrontPage($data_, 'alumnilist');
    }

    public function AlumniRegistration() {

        $this->load->helper('url_helper');
        $this->load->helper('form');
        $insert = $this->input->post('jform');

        $files = $this->input->post('file_name');
        $config['file_name'] = $_FILES['file_name']['name'];
        $config['upload_path'] = '/home/web/bss-admin/html/assets/art_images';
        $config['repository_url'] = 'http://bss-admin.sil.lab/assets/art_images';
        if ($_SERVER['HTTP_HOST'] == 'bss-web.saturn.co.in') {//http://manage.bssgurukulam.com/
            $config['upload_path'] = '/www/sites/bss.saturn.co.in/html/assets/art_images';
            $config['repository_url'] = 'http://bss.saturn.co.in/assets/art_images';
        }else if($_SERVER['HTTP_HOST'] == 'www.bssgurukulam.com'){
            $config['upload_path'] = '/www/sites/manage.bssgurukulam.com/html/assets/art_images';
            $config['repository_url'] = 'http://manage.bssgurukulam.com/assets/art_images';
        }else{
            $config['upload_path'] = 'c:/wamp64/www/bss/assets/art_images';
            $config['repository_url'] = 'http://manage.bssgurukulam.com/assets/art_images';
        }
        $config['allowed_types'] = 'png|gif|jpg|jpeg|JPG|JPEG|PNG|GIF';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file_name')) {
            $upload_data = $this->upload->data();
            $insert['alumni_image_name'] = $upload_data['file_name'];

            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $upload_data['file_name']);
            $new_fl = $withoutExt . '_' . md5(date("Y-m-d_H:i:s"));
            $insert['alumni_image_path'] = '/assets/art_images/' . $insert['alumni_image_name'];
        } else {
            $data['uploadfailed'] = 1;
            /* var_dump($_FILES);
              var_dump($this->upload->display_errors());
              exit; */
        }
        $insert['is_approved'] = 'Registered';
        $data['success'] = $this->Home_model->insert_uploads($insert);
        if ($data['success'] == true) {
            $data['uploadsuccess'] = 1;
        } else {
            $data['uploadfailed'] = 1;
        }
        //$this->load->helper('form');
        // $this->show_display("alumni-form", $data);
        //$password = $this->showfrontPage($data, 'alumnilist');
        redirect('alumni/list/0');
    }

    public function logout_management() {
        $data['active'] = "1";
        //$data['login'] = 1;
        $this->load->helper('form');
        $data['home_submenu'] = $this->Home_model->static_submenu_list(1);
        $this->load->library('session');
        if ($this->session->userdata('isLogin')) {
            $sess_array = array('isLogin' => '');
            $this->session->unset_userdata($sess_array);
            $this->session->set_flashdata('success', 'You have logged out successfully');
            $this->session->sess_destroy();
            //$this->index();
            redirect('Home/index');
        } else {
            $data['notlogin'] = 1;
            $this->show_display("student_login", $data);
        }
    }

    public function change_password() {
        $data['active'] = "1";

        $this->load->helper('form');
        $data['home_submenu'] = $this->Home_model->static_submenu_list(1);
        $this->load->library('session');
        if ($this->session->userdata('isLogin')) {
            $data['change_password'] = 1;
            $this->load->helper('form');
            // $this->show_display("student_login", $data);
            $this->showfrontPage($data, 'student_login');
        } else {
            $data['notlogin'] = 1;
            //$this->show_display("student_login", $data);
            $this->showfrontPage($data, 'student_login');
        }
    }

    public function forgot_password() {
        $data['active'] = "1";
        $this->load->helper('form');
        $data['home_submenu'] = $this->Home_model->static_submenu_list(1);
        // $this->load->library('session');
        $data['forgot_passwordss'] = 1;
        // var_dump($data);exit;
        //$this->show_display("student_login", $data);
        $this->showfrontPage($data, 'student_login');
    }

    function generatePassword($length, $strength) {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }
        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }

    public function forgotPasswordFunction() {
        $this->load->library('session');
        $this->load->helper('form');
        $data['active'] = "1";
        $data['mailId'] = $this->input->post('useraname');
        $emailcheck = $this->Students_model->get_email_details($data);
        if ($emailcheck) {
            $password = $this->generatePassword(7, 7);
            $data['password'] = $password;
            $data['user_id'] = $this->Students_model->update($data);
            if ($data['user_id']) {
                $emailto = htmlspecialchars($data['mailId']);
                $this->load->library('email');
                $config = Array(
                    'protocol' => 'sendmail',
                    'smtp_host' => 'your domain SMTP host',
                    'smtp_port' => 25,
                    'smtp_user' => 'athira@actsinfo.biz',
                    'smtp_pass' => 'actinfo.biz',
                    'smtp_timeout' => '4',
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1'
                );
                $data1 = array(
                    'userName' => $data['mailId'],
                    'password' => $password,
                );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from('bssgurukulam.12@gmail.com ', ' BSS GURUKULAM ');
                $this->email->to($emailto);
                $this->email->subject('Password Reset');
                $message = $this->load->view('email', $data1, TRUE);
                $this->email->message($message);
                $this->email->send();
                $data['forgotpassword_change'] = 1;
                // var_dump($data);exit;
                $data['login'] = 1;
                //$this->show_display("student_login", $data);
                $this->showfrontPage($data, 'student_login');
            }
        } else {
            //$data['page'] = 'emailnot';
            $data['emailnot'] = 1;
            $data['login'] = 1;
            // $this->show_display("student_login", $data);
            $this->showfrontPage($data, 'student_login');
        }
    }

    public function ChangePasswordFunction() {
        $this->load->library('session');
        $data['active'] = "1";
        if ($this->session->userdata('isLogin')) {
            $data['mailId'] = $this->session->userdata('UserName');
            $data['password'] = $this->input->post('currentpass');
            $getDetails = $this->Students_model->Get_User_details($data);
            if ($getDetails) {
                $data['password'] = $this->input->post('newpasss');
                $data['user_id'] = $this->Students_model->update($data);
                if ($data['user_id']) {
                    // echo "<script>alert('Password Reset Successfully!!!');</script>";

                    $data['password_change'] = 1;
                    $data['login'] = 1;
                    // $this->show_display("student_login", $data);
                    $this->showfrontPage($data, 'student_login');
                }
            } else {
                //echo "<script>alert('Password Do not  match ');</script>";
                $data['page'] = 'change_password';
                $data['password_notmatch'] = 1;
                // $this->show_display("student_login", $data);
                $this->showfrontPage($data, 'student_login');
            }
        } else {
            $data['notlogin'] = 1;
            //$this->show_display("student_login", $data);
            $this->showfrontPage($data, 'student_login');
        }
    }

    public function static_school(){
        $data = array();
        $this->show_display('static/about_school',$data);
    }

    public function static_mission_vision(){
       $data = array();
       $this->show_display('static/mission_vision',$data); 
    }
    public function static_system_of_school(){
       $data = array();
       $this->show_display('static/system_of_school',$data); 
    }
    public function static_hall_of_fame(){
       $data = array();
       $this->show_display('static/hall_of_fame',$data); 
    }
    public function static_accolades_awards(){
       $data = array();
       $this->show_display('static/accolades_awards',$data); 
    }    
    public function static_founder(){
       $data = array();
       $this->show_display('static/founder',$data); 
    } 
    public function static_management(){
       $data = array();
       $this->show_display('static/management',$data); 
    } 
    public function static_principal(){
        $data = array();
        $this->show_display('static/principals-message',$data); 
     } 



}
