<main class="page about-page">
<section class="title" style="background-image:url(<?php echo base_url(); ?>/public/img/titleweb.jpg);">

                <div class="container">
                
                <h1>ABOUT</h1>

                </div>

</section>
            <section class="page-content">



                <div class="container-full">
                    <!-- swiper1 -->
                    <div class="tab-section">
                        <div class="tab-nav">
                            <ul class="tabul ">
                                <li class="">
                                    <a  href="<?php echo base_url(); ?>pages/school">School</a>
                                </li>
                                <li class="">
                                    <a  href="<?php echo base_url(); ?>pages/mission-vision">Mission and Vision</a>
                                </li>
                                <li class="">
                                    <a href="<?php echo base_url(); ?>pages/system-of-school">System of School</a>
                                </li>
                                <li class="">
                                    <a href="<?php echo base_url(); ?>pages/hall-of-fame">Hall of Fame</a>
                                </li>
                                <li class="">
                                    <a   href="<?php echo base_url(); ?>pages/accolades-awards">Accolades and Awards</a>
                                </li>
                                <li class="">
                                    <a href="<?php echo base_url(); ?>pages/founder">Founder</a>
                                </li>
                                <!--<li class="">
                                    <a href="gallery.html">Gallery</a>
                                </li>-->
                                <li class="">
                                    <a class="active" href="<?php echo base_url(); ?>pages/management">Management</a>
                                </li>

                            </ul>
                        </div>
                        <div class="mob-tab-nav tabswiper">
                                <ul class="tabul swiper-wrapper">
                                        <li class="swiper-slide">
                                             <a  href="<?php echo base_url(); ?>pages/school">School</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a  href="<?php echo base_url(); ?>pages/mission-vision">Mission and Vision</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a  href="<?php echo base_url(); ?>pages/system-of-school">System of School</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a href="<?php echo base_url(); ?>pages/hall-of-fame">Hall of Fame</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a  href="<?php echo base_url(); ?>pages/accolades-awards">Accolades and Awards</a>
                                        </li>
                                        <li class="swiper-slide swiper-slide-active">
                                             <a href="<?php echo base_url(); ?>pages/founder">Founder</a>
                                        </li>
                                        <!--<li class="swiper-slide">
                                            <a href="gallery.html">Gallery</a>
                                        </li>-->
                                        <li class="swiper-slide">
                                            <aclass="active" href="<?php echo base_url(); ?>pages/management">Management</a>
                                        </li>
                                        <li class="swiper-slide dummyli"></li>
        
                                    </ul>
                        </div>
                        <div class="tab-content mission">


                            <div class="row">
                                <div class="col-md-10 soceity">
                                        <h2>Soceity</h2>

                                    <div class="row">
                                            <div class="col-md-8">
                                                
                                        <p>
                                                BSS Educational Society  is formed by Swami Nirmalananda Yogi in the year 1984,This is a charitable educational society,  under this society four schools are there,  BSS Gurukulam higher Secondary School ( unaided)  ALP school Vanur ( aided) , BSS Higher Secondary School Kollengode ( aided) and Yogini Matha Girls High School Kollengode ( aided)   BSS BEd College Alathur and  GIrls college BSS Vanitha Colege Alathur (100 % free education to the girls who were financially challenged)   are functioning.
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="adrs">
                                                    <h4>Address</h4>
                                                
                                                    <address>
                                                       BSS EDUCATIONAL SOCIETY <br>
           
                                                       BSS GURUKULAM HIGHER SECONDARY SCHOOL  CAMPUS<br>
                                                       
                                                       ALAHTUR. PALAKKAD .<br> KERALA 678541
                                                    </address>
                                            </div>

                                            </div>
                                            <div class="col-md-12">
                                                <h3>Objectives</h3>
                                                <ul>
                                                    <li>To spread education and culture among the people with special  emphasis on the uplift of the illiterate , and other back ward class and women of the state. to achieve this object , the society will develop the educational and cultural institutions.  besides this , society will accept, acquire or purchase properties, construct buildings , take over management of other educational institutions  co-operate with government or any other organisation or association as me be found suitable for the furtherance of the causes of education , culture and morality. </li>
                                                    <li>To instill in the students tthe virtues of moral, goodness, justice, perseverance as also obedience and discipline.  </li>
                                                    <li>To improve the standard of education and enable the students to have broad outlook generally </li>
                                                    <li>To improve upon the students the necessity of showing respect and reverence towards the teachers and parents and to train them to foster a feeling of fraternity  among themselves </li>
                                                    <li>To give the students good quardings , high culture and train them to become worthy citizens . </li>
                                                    <li>To help the students acquire  knowledge for development of their brain and control  of their mind. </li>
                                                    <li>To disseminate sports spirit among  the students </li>
                                                    <li>To disseminate sports spirit among  the students </li>
                                                    <li>To raise fund from public by donation or by arranging benefit performances and collecting fees and other receipts from students for services and also by raising loans from public , banks and other financial institutions  </li>
                                                    
                                                </ul>
                                            </div>
                                            <div class="col-md-12">
                                                <h2>Institutions</h2>
                                                <div class="row between">
                                                    <div class="col-md-6">
                                                        <div class="inst">
                                                                <div class="instt-photo" style="background-image: url(<?php echo base_url(); ?>public/img/insti1.JPG);"></div>
                                                                <p>BSS Gurukulam Higher Secondary School Alathur</p>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                            <div class="inst">
                                                                    <div class="instt-photo" style="background-image: url(<?php echo base_url(); ?>public/img/insti2.JPG);"></div>
                                                                    <p>BSS Gurukulam Higher Secondary School Alathur</p>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                            <div class="inst">
                                                                    <div class="instt-photo" style="background-image: url(<?php echo base_url(); ?>public/img/insti3.JPG);"></div>
                                                                    <p>BSS Gurukulam Higher Secondary School Alathur</p>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                            <div class="inst">
                                                                    <div class="instt-photo" style="background-image: url(<?php echo base_url(); ?>public/img/insti4.JPG);"></div>
                                                                    <p>BSS Gurukulam Higher Secondary School Alathur</p>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                            <div class="inst">
                                                                    <div class="instt-photo" style="background-image: url(<?php echo base_url(); ?>public/img/insti5.JPG);"></div>
                                                                    <p>BSS Gurukulam Higher Secondary School Alathur</p>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>


                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                            
    


                        </div>

                    </div>

                    <!-- swiper2 -->


                </div>
            </section>
        </main>