
<main class="page about-page">
            <section class="title" style="background-image:url(<?php echo base_url(); ?>/public/img/titleweb.jpg);">

                <div class="container">
                    <?php if(isset($active_slug)){ ?> 
						<h1><?php echo $active_name;  ?></h1> 	  
					<?php } ?>
                </div>

            </section>
            <section class="page-content">



                <div class="container-full">

                    <div class="tab-section">
                        <div class="tab-nav ">
                            <ul class="tabul " >

                            	<?php foreach ($sub_menu as $key => $sub) { ?>
								<li class="nav-item">
									<a class="nav-link <?php if ($active_submenu == $sub['menu_id']) { echo 'active'; } ?>"  id="tab--<?php echo $key; ?>" href="<?php echo base_url(); ?>pages/<?php echo $sub['slug']; ?>"><?php echo $sub['menu_name']; ?></a>
								</li>
								<?php }
								if($submenu_id == 54) {
								?>
								<li class="nav-item" role="presentation">
									<a class="nav-link" href="<?php echo base_url(); ?>pages/kg">KG</a>
								</li> 
								<li class="nav-item"  role="presentation">
									<a class="nav-link" href="<?php echo base_url(); ?>pages/primary">PRIMARY</a>
								</li> 
								<li  class="nav-item"  role="presentation">
									<a class="nav-link" href="<?php echo base_url(); ?>pages/upper_primary">UPPER PRIMARY</a>
								</li> 
								<li class="nav-item"  role="presentation">
									<a class="nav-link" href="<?php echo base_url(); ?>pages/secondary">SECONDARY</a>
								</li> 
								<li class="nav-item"  role="presentation">
									<a class="nav-link" href="<?php echo base_url(); ?>pages/senior_secondary"> SENIOR SECONDARY</a>
								</li> 							
							<?php
							}
							?> 

                           </ul>
                            
                        </div>
                        <div class="mob-tab-nav tabswiper">
                                <ul class="tabul swiper-wrapper">

                                        <li class="swiper-slide">
                                            <a class="active" href="about.html">School</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a href="mission.html">Mission and Vision</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a href="system.html">System of School</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a href="hall.html">Hall of Fame</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a href="accolades.html">Accolades and Awards</a>
                                        </li>
                                        <li class="swiper-slide swiper-slide-active">
                                            <a href="founder.html">Founder</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a href="gallery.html">Gallery</a>
                                        </li>
                                        <li class="swiper-slide">
                                            <a href="management.html">Management</a>
                                        </li>
                                        <li class="swiper-slide dummyli"></li>
        
                                    </ul>
                        </div>
                        <div class="tab-content school">


                           

                            <div class="page-row">
                                
                                <div class="page-column column3">
                                    
                                   
                            <?php 
							if (isset($submenu_detail)) {
								 foreach ($submenu_detail as $sub) {?>
									<h2> <?php if($active_submenu_name){ echo $active_submenu_name ; }?></h2> 
									 
									  
								   <?php echo $sub['content'];
								}
							}else if(isset($menu_content)) {?>
								 <h2> <?php if(isset($heading)){ echo $heading ; }?></h2>
								 <?php foreach ($menu_content as $menu) {
									  echo $menu['content'];
								 }
							}?>
                                        
                                   

                                </div>
                               
                            </div>


                        </div>

                    </div>

                    <!-- swiper2 -->


                </div>
            </section>
        </main>

