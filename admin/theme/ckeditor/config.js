﻿/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	
	// %REMOVE_START%
	// The configuration options below are needed when running CKEditor from source files.
	config.plugins = 'dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,button,panelbutton,panel,floatpanel,colordialog,templates,menu,contextmenu,copyformatting,div,resize,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,floatingspace,listblock,richcombo,font,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,justify,menubutton,link,list,liststyle,magicline,maximize,newpage,pagebreak,toolbar,notification,clipboard,pastetext,pastefromword,preview,print,removeformat,save,selectall,showblocks,showborders,sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,tableselection,undo,wsc,lineutils,widgetselection,widget,basewidget,layoutmanager,btgrid,bt_table,youtube';
	config.skin = 'moono';
	// %REMOVE_END%
	config.extraPlugins = 'imageuploader';
	config.extraPlugins = 'imageresponsive';
	config.extraPlugins = 'image2';
	config.extraPlugins = 'bootstrapVisibility';
	config.extraPlugins = 'youtube';
	config.extraPlugins = 'bt_table';
	config.extraPlugins = 'colorbutton';
	
	config.height = 500;
		
	
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    
    
	//the next line add the new font to the combobox in CKEditor
    //config.font_names = '<Cutsom Font Name>/<YourFontName>;' + config.font_names;
    config.contentsCss = 'cfonts/fonts.css';
	config.font_names = 'Open Sans/open_sansregular;' + config.font_names;
    config.font_names = 'Open Sans Light/open_sanslight;' + config.font_names;
    config.font_names = 'Open Sans Semibold/open_sanssemibold;' + config.font_names;
    config.font_names = 'Aileron/aileronregular;' + config.font_names;
    
    
    //config.contentsCss =  ['/ckeditor/CustomFonts/font.css', 'https://fonts.googleapis.com/css?family=Bungee+Inline'];
    //config.font_names = 'Bungee Inline/Bungee Inline;' + config.font_names;
    
    
};
