<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\CharityImages;

/**
 * CharityimageSearch represents the model behind the search form of `admin\models\CharityImages`.
 */
class CharityimageSearch extends CharityImages
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['art_image_id', 'arts_id', 'sort_order', 'uploaded_by'], 'integer'],
            [['art_image_path', 'arts_filename', 'art_content', 'uploaded_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CharityImages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'art_image_id' => $this->art_image_id,
            'arts_id' => $this->arts_id,
            'sort_order' => $this->sort_order,
            'uploaded_by' => $this->uploaded_by,
            'uploaded_on' => $this->uploaded_on,
        ]);

        $query->andFilterWhere(['like', 'art_image_path', $this->art_image_path])
            ->andFilterWhere(['like', 'arts_filename', $this->arts_filename])
            ->andFilterWhere(['like', 'art_content', $this->art_content]);

        return $dataProvider;
    }
}
