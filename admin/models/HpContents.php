<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "homepage_contents".
 *
 * @property int $hc_id
 * @property string $hc_title
 * @property string $hc_content
 * @property string $hc_image
 * @property string $hc_status
 */
class HpContents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'homepage_contents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hc_title', 'hc_content','hc_urls', 'hc_status'], 'required'],
            [['hc_image'], 'required', 'on' => 'create'],
            [['hc_status','hc_urls'], 'string'],
            [['hc_content'], 'string', 'max' => 500],
            [['hc_title'], 'string', 'max' => 255],
            ['hc_image', 'image', 'minWidth' => 230,'maxWidth' => 680, 'minHeight' => 320, 'maxHeight' => 580, 'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 2],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hc_id' => 'ID',
            'hc_title' => 'Title',
            'hc_content' => 'Content',
            'hc_image' => 'Image',
            'hc_urls' => 'Redirect URL',
            'hc_status' => 'Status',
        ];
    }
}
