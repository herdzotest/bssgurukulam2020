<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "facilities".
 *
 * @property int $f_id
 * @property string $f_title
 * @property string $f_desc
 * @property string $f_status
 * @property string $f_slug
 * @property string $f_createdOn
 */
class Facilities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'facilities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['f_title', 'f_desc', 'f_slug'], 'required'],
            [['f_desc', 'f_status'], 'string'],
            [['f_createdOn'], 'safe'],
            [['f_title', 'f_slug'], 'string', 'max' => 155],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'f_id' => 'ID',
            'f_title' => 'Title',
            'f_desc' => 'Description',
            'f_status' => 'Status',
            'f_slug' => 'Slug',
            'f_createdOn' => 'Created On',
        ];
    }
}
