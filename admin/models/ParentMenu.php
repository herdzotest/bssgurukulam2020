<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "parent_menu".
 *
 * @property int $pm_id
 * @property string $pm_name
 * @property string $pm_slug
 * @property string $pm_createdon
 * @property string $pm_status
 */
class ParentMenu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parent_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pm_name', 'pm_status'], 'required'],
            ['pm_slug','match', 'pattern' => '([A-Za-z\_]+)', 'message' => 'Menu Slug can only contain Alphabet and Hyphen only'],
            [['pm_createdon'], 'safe'],
            [['pm_status'], 'string'],
            [['pm_name', 'pm_slug'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pm_id' => 'ID',
            'pm_name' => 'Name',
            'pm_slug' => 'Slug',
            'pm_createdon' => 'Createdon',
            'pm_status' => 'Status',
        ];
    }
}
