<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "mst_designation".
 *
 * @property int $designation_id Id of Designation
 * @property string $designation_name Name of Designation
 * @property int $report_to to the person whom wants to report
 * @property int|null $created_by Created By
 * @property string $created_on Created On 
 * @property string|null $updated_on Updated On 
 * @property int|null $updated_by Updated By
 * @property string|null $status Status of Designation(Active/Deactive)
 */
class Designation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_designation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['designation_id', 'designation_name', 'created_on'], 'required'],
            [['designation_id', 'report_to', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['status'], 'string'],
            [['designation_name'], 'string', 'max' => 100],
            [['designation_name'], 'unique'],
            [['designation_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'designation_id' => 'Designation ID',
            'designation_name' => 'Designation Name',
            'report_to' => 'Report To',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }
}
