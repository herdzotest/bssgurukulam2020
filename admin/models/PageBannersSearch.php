<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\PageBanners;

/**
 * PageBannersSearch represents the model behind the search form of `admin\models\PageBanners`.
 */
class PageBannersSearch extends PageBanners
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pb_id'], 'integer'],
            [['pb_image', 'pb_page_name', 'pb_status', 'pb_createdon'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageBanners::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pb_id' => $this->pb_id,
            'pb_createdon' => $this->pb_createdon,
        ]);

        $query->andFilterWhere(['like', 'pb_image', $this->pb_image])
            ->andFilterWhere(['like', 'pb_page_name', $this->pb_page_name])
            ->andFilterWhere(['like', 'pb_status', $this->pb_status]);

        return $dataProvider;
    }
}
