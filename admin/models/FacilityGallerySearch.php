<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\FacilityGallery;

/**
 * FaciltyGallerySearch represents the model behind the search form of `admin\models\FaciltyGallery`.
 */
class FacilityGallerySearch extends FacilityGallery
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fg_id', 'fg_fid'], 'integer'],
            [['fg_img', 'fg_status', 'fg_createdOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FacilityGallery::find()->select('fg_fid')->distinct();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'fg_id' => $this->fg_id,
            'fg_fid' => $this->fg_fid,
            'fg_createdOn' => $this->fg_createdOn,
        ]);

        $query->andFilterWhere(['like', 'fg_img', $this->fg_img])
            ->andFilterWhere(['like', 'fg_status', $this->fg_status]);

        return $dataProvider;
    }
}
