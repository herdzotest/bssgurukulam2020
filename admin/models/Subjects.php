<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "mst_subjects".
 *
 * @property int $sub_id
 * @property string|null $sub_name Subject of book
 * @property int $created_by
 * @property string $created_on
 * @property int $updated_by
 * @property string $updated_on
 * @property string|null $status book_subject status(0) - inactive.
 * @property int $excel_file_id FK:excel_file_tbl
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_subjects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sub_id', 'created_by', 'created_on', 'updated_by', 'updated_on', 'excel_file_id'], 'required'],
            [['sub_id', 'created_by', 'updated_by', 'excel_file_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['status'], 'string'],
            [['sub_name'], 'string', 'max' => 100],
            [['sub_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sub_id' => 'Sub ID',
            'sub_name' => 'Sub Name',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'status' => 'Status',
            'excel_file_id' => 'Excel File ID',
        ];
    }
}
