<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "charity_gallery".
 *
 * @property int $cg_id
 * @property int $cg_cid
 * @property string $cg_img
 * @property string $cg_status
 * @property string $cg_createdOn
 *
 * @property Charity $cgC
 */
class CharityGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charity_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cg_cid', 'cg_img', 'cg_status'], 'required'],
            [['cg_cid'], 'integer'],
            [['cg_status'], 'string'],
            [['cg_createdOn'], 'safe'],
            //[['cg_img'], 'string', 'max' => 155],
            [['cg_img'], 'image','minWidth' => 595,'maxWidth' => 595,'minHeight' => 585,'maxHeight' => 585,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 2,'maxFiles' => 10],
            [['cg_cid'], 'exist', 'skipOnError' => true, 'targetClass' => Charity::class, 'targetAttribute' => ['cg_cid' => 'ch_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cg_id' => 'ID',
            'cg_cid' => 'Charity Title',
            'cg_img' => 'Gallery Image',
            'cg_status' => 'Gallery Status',
            'cg_createdOn' => 'Created On',
        ];
    }

    public function attributeHints()
    {
        return [
            'cg_img' => 'Upload 595 x 585 image',
        ];
    }

    /**
     * Gets query for [[CgC]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCharity()
    {
        return $this->hasOne(Charity::class, ['ch_id' => 'cg_cid']);
    }
}
