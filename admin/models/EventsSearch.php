<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\Events;

/**
 * EventsSearch represents the model behind the search form of `admin\models\Events`.
 */
class EventsSearch extends Events
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ev_id'], 'integer'],
            [['ev_title', 'ev_slug', 'ev_featuredImg', 'ev_desc', 'ev_date', 'ev_place', 'ev_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Events::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ev_id' => $this->ev_id,
            'ev_date' => $this->ev_date,
        ]);

        $query->andFilterWhere(['like', 'ev_title', $this->ev_title])
            ->andFilterWhere(['like', 'ev_slug', $this->ev_slug])
            ->andFilterWhere(['like', 'ev_featuredImg', $this->ev_featuredImg])
            ->andFilterWhere(['like', 'ev_desc', $this->ev_desc])
            ->andFilterWhere(['like', 'ev_place', $this->ev_place])
            ->andFilterWhere(['like', 'ev_status', $this->ev_status]);

        return $dataProvider;
    }
}
