<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "about_pages".
 *
 * @property int $ap_id
 * @property string $ap_title
 * @property string $ap_content
 * @property string $ap_createdon
 * @property string $ap_status
 */
class AboutPages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ap_title','ap_slug','ap_content','ap_status'], 'required'],
            [['ap_content', 'ap_status'], 'string'],
            [['ap_createdon'], 'safe'],
            [['ap_title'], 'string', 'max' => 155],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ap_id' => 'ID',
            'ap_title' => 'Title',
            'ap_slug' => 'Slug',
            'ap_content' => 'Content',
            'ap_createdon' => 'Createdon',
            'ap_status' => 'Status',
        ];
    }
}
