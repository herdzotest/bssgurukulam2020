<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\AboutPages;

/**
 * AboutPagesSearch represents the model behind the search form of `admin\models\AboutPages`.
 */
class AboutPagesSearch extends AboutPages
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ap_id'], 'integer'],
            [['ap_title', 'ap_slug', 'ap_content', 'ap_createdon', 'ap_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AboutPages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ap_id' => $this->ap_id,
            'ap_createdon' => $this->ap_createdon,
        ]);

        $query->andFilterWhere(['like', 'ap_title', $this->ap_title])
            ->andFilterWhere(['like', 'ap_slug', $this->ap_slug])
            ->andFilterWhere(['like', 'ap_content', $this->ap_content])
            ->andFilterWhere(['like', 'ap_status', $this->ap_status]);

        return $dataProvider;
    }
}
