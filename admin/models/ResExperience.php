<?php

namespace admin\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "res_experience".
 *
 * @property int $re_id
 * @property int $re_rid
 * @property string $re_institute
 * @property string $re_role
 * @property string $re_start_date
 * @property string $re_end_date
 * @property string $re_status
 * @property string $re_updatedOn
 */
class ResExperience extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'res_experience';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['re_institute', 're_role','re_start_date','re_end_date', 're_status'], 'required'],
            [['re_rid'], 'integer'],
            [['re_start_date', 're_end_date', 're_updatedOn'], 'safe'],
            [['re_status'], 'string'],
            [['re_institute'], 'string', 'max' => 155],
            [['re_role'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            're_id' => 'ID',
            're_rid' => 'Rid',
            're_institute' => 'Name of Institute',
            're_role' => 'Role',
            're_start_date' => 'Role Start Date',
            're_end_date' => 'Role End Date',
            're_status' => 'Experience Status',
            're_updatedOn' => 'Updated On',
        ];
    }
    public function getUser($id)
    {
       $userModel = User::find()->where(['id' => $id])->one();
       return $userModel;
    }
}
