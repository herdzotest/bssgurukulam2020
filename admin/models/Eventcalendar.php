<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "event_calender".
 *
 * @property int $event_calender_id
 * @property string $event_name
 * @property string $event_description
 * @property int $event_department_id
 * @property int $event_organizer_id
 * @property string $event_start_date
 * @property string $event_end_date
 * @property string $event_venue
 * @property string $event_calender_status
 * @property string $event_category
 * @property string $event_image_path
 * @property int $created_by
 * @property string $created_on
 * @property int $updated_by
 * @property string $updated_on
 * @property string $event_student_name
 * @property string|null $event_class
 * @property int $event_committee_id
 */
class Eventcalendar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_calender';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_name', 'event_description', 'event_department_id', 'event_organizer_id', 'event_start_date', 'event_end_date', 'event_venue', 'event_calender_status', 'event_image_path', 'created_by', 'created_on', 'updated_by', 'updated_on', 'event_committee_id'], 'required'],
            [['event_description', 'event_calender_status', 'event_category'], 'string'],
            [['event_department_id', 'event_organizer_id', 'created_by', 'updated_by', 'event_committee_id'], 'integer'],
            [['event_start_date', 'event_end_date', 'created_on', 'updated_on'], 'safe'],
            [['event_name', 'event_student_name', 'event_class'], 'string', 'max' => 55],
            [['event_venue'], 'string', 'max' => 66],
            [['event_image_path'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'event_calender_id' => 'Event Calender ID',
            'event_name' => 'Event Name',
            'event_description' => 'Event Description',
            'event_department_id' => 'Event Department ID',
            'event_organizer_id' => 'Event Organizer ID',
            'event_start_date' => 'Event Start Date',
            'event_end_date' => 'Event End Date',
            'event_venue' => 'Event Venue',
            'event_calender_status' => 'Event Calender Status',
            'event_category' => 'Event Category',
            'event_image_path' => 'Event Image Path',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'event_student_name' => 'Event Student Name',
            'event_class' => 'Event Class',
            'event_committee_id' => 'Event Committee ID',
        ];
    }
}
