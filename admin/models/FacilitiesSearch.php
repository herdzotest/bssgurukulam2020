<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\Facilities;

/**
 * FacilitiesSearch represents the model behind the search form of `admin\models\Facilites`.
 */
class FacilitiesSearch extends Facilities
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['f_id'], 'integer'],
            [['f_title', 'f_desc', 'f_status', 'f_slug', 'f_createdOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Facilities::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'f_id' => $this->f_id,
            'f_createdOn' => $this->f_createdOn,
        ]);

        $query->andFilterWhere(['like', 'f_title', $this->f_title])
            ->andFilterWhere(['like', 'f_desc', $this->f_desc])
            ->andFilterWhere(['like', 'f_status', $this->f_status])
            ->andFilterWhere(['like', 'f_slug', $this->f_slug]);

        return $dataProvider;
    }
}
