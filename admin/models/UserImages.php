<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "user_images".
 *
 * @property int $pp_id
 * @property int $pp_uid
 * @property string $pp_image
 * @property string $pp_status
 */
class UserImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pp_image'], 'required', 'on' => 'create'],
            ['pp_image', 'image', 'minWidth' => 150,'maxWidth'=>150,'maxHeight'=>150, 'minHeight' => 150,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 1],
            [['pp_uid'], 'integer'],
            [['pp_status'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pp_id' => 'ID',
            'pp_uid' => 'Uid',
            'pp_image' => 'Profile Image',
            'pp_status' => 'Status',
        ];
    }
}
