<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "alumniProfile".
 *
 * @property int $ap_id
 * @property int $ap_uid
 * @property int $ap_approved_uid
 * @property string $ap_batch_from
 * @property string $ap_batch_to
 * @property string $ap_address
 * @property int $ap_country
 * @property int $ap_state
 * @property int $ap_city
 * @property string $ap_pincode
 * @property string $ap_qualification
 * @property string $ap_linkedin_link
 * @property string $ap_facebook_link
 * @property string $ap_twitter_link
 * @property string $ap_class
 */
class AlumniProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumniProfile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ap_batch_from', 'ap_batch_to', 'ap_address', 'ap_country', 'ap_state', 'ap_city', 'ap_pincode', 'ap_qualification'], 'required'],
            [['ap_uid', 'ap_approved_uid', 'ap_country', 'ap_state', 'ap_city'], 'integer'],
            [['ap_batch_from', 'ap_batch_to'], 'safe'],
            [['ap_address'], 'string'],
            [['ap_pincode'], 'string', 'max' => 15],
            [['ap_qualification'], 'string', 'max' => 155],
            [['ap_linkedin_link', 'ap_facebook_link', 'ap_twitter_link', 'ap_class'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ap_id' => 'ID',
            'ap_uid' => 'Uid',
            'ap_approved_uid' => 'Approved By',
            'ap_batch_from' => 'Batch Started From',
            'ap_batch_to' => 'Batch Ended At',
            'ap_address' => 'Current Communication Address',
            'ap_country' => 'Country',
            'ap_state' => 'State',
            'ap_city' => 'City',
            'ap_pincode' => 'Pincode',
            'ap_qualification' => 'Qualification',
            'ap_linkedin_link' => 'Linkedin Link',
            'ap_facebook_link' => 'Facebook Link',
            'ap_twitter_link' => 'Twitter Link',
            'ap_class' => 'Last Relieved Class',
        ];
    }
    public static function getActive()
    {
        return AlumniProfile::find()->where(['ap_approved_id' => '1'])->all();
    }
    
}
