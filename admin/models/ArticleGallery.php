<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "article_gallery".
 *
 * @property int $ag_id
 * @property int $ag_aid
 * @property string $ag_img
 * @property string $ag_status
 * @property string $ag_createdOn
 *
 * @property Articles $agA
 */
class ArticleGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ag_aid', 'ag_img', 'ag_status'], 'required'],
            [['ag_aid'], 'integer'],
            [['ag_status'], 'string'],
            [['ag_createdOn'], 'safe'],
            [['ag_img'], 'image','minWidth' => 595,'maxWidth' => 595,'minHeight' => 585,'maxHeight' => 585,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 2,'maxFiles' => 10],
            [['ag_aid'], 'exist', 'skipOnError' => true, 'targetClass' => Articles::class, 'targetAttribute' => ['ag_aid' => 'ar_id']],
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ag_id' => 'ID',
            'ag_aid' => 'Article Title',
            'ag_img' => 'Gallery Image',
            'ag_status' => 'Gallery Status',
            'ag_createdOn' => 'Created On',
        ];

    }

    public function attributeHints()
    {
        return [
            'ag_img' => 'Upload 595 x 585 image',
        ];
    }


    /**
     * Gets query for [[AgA]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgA()
    {
        return $this->hasOne(Articles::className(), ['ar_id' => 'ag_aid']);
    }
}
