<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "student_bs_list".
 *
 * @property int $list_id
 * @property int $list_student_id
 * @property string $list_bs_name
 * @property string $list_bs_class
 */
class StudentbsList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_bs_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['list_student_id'], 'integer'],
            [['list_bs_class'], 'string'],
            [['list_bs_name'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'list_id' => 'List ID',
            'list_student_id' => 'List Student ID',
            'list_bs_name' => 'Brother/Sister Name',
            'list_bs_class' => 'Brother/Sister Studying Class',
        ];
    }
}
