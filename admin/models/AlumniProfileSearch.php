<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\AlumniProfile;

/**
 * AlumniProfileSearch represents the model behind the search form of `admin\models\AlumniProfile`.
 */
class AlumniProfileSearch extends AlumniProfile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ap_id', 'ap_uid', 'ap_approved_uid', 'ap_country', 'ap_state', 'ap_city'], 'integer'],
            [['ap_batch_from', 'ap_batch_to', 'ap_address', 'ap_pincode', 'ap_qualification', 'ap_linkedin_link', 'ap_facebook_link', 'ap_twitter_link', 'ap_class'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AlumniProfile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ap_id' => $this->ap_id,
            'ap_uid' => $this->ap_uid,
            'ap_approved_uid' => $this->ap_approved_uid,
            'ap_batch_from' => $this->ap_batch_from,
            'ap_batch_to' => $this->ap_batch_to,
            'ap_country' => $this->ap_country,
            'ap_state' => $this->ap_state,
            'ap_city' => $this->ap_city,
        ]);

        $query->andFilterWhere(['like', 'ap_address', $this->ap_address])
            ->andFilterWhere(['like', 'ap_pincode', $this->ap_pincode])
            ->andFilterWhere(['like', 'ap_qualification', $this->ap_qualification])
            ->andFilterWhere(['like', 'ap_linkedin_link', $this->ap_linkedin_link])
            ->andFilterWhere(['like', 'ap_facebook_link', $this->ap_facebook_link])
            ->andFilterWhere(['like', 'ap_twitter_link', $this->ap_twitter_link])
            ->andFilterWhere(['like', 'ap_class', $this->ap_class]);

        return $dataProvider;
    }
}
