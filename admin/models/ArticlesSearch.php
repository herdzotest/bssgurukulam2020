<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\Articles;

/**
 * ArticlesSearch represents the model behind the search form of `admin\models\Articles`.
 */
class ArticlesSearch extends Articles
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ar_id'], 'integer'],
            [['ar_title', 'ar_featured_image', 'ar_slug', 'ar_content', 'ar_created_by', 'ar_creator_class', 'ar_created_on', 'ar_published_on', 'ar_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Articles::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ar_id' => $this->ar_id,
            'ar_created_on' => $this->ar_created_on,
            'ar_published_on' => $this->ar_published_on,
        ]);

        $query->andFilterWhere(['like', 'ar_title', $this->ar_title])
            ->andFilterWhere(['like', 'ar_featured_image', $this->ar_featured_image])
            ->andFilterWhere(['like', 'ar_slug', $this->ar_slug])
            ->andFilterWhere(['like', 'ar_content', $this->ar_content])
            ->andFilterWhere(['like', 'ar_created_by', $this->ar_created_by])
            ->andFilterWhere(['like', 'ar_creator_class', $this->ar_creator_class])
            ->andFilterWhere(['like', 'ar_status', $this->ar_status]);

        return $dataProvider;
    }
}
