<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "event_gallery".
 *
 * @property int $eg_id
 * @property int $eg_eid
 * @property string $eg_img
 * @property string $eg_status
 * @property string $eg_createdOn
 *
 * @property Events $egE
 */
class EventGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eg_eid', 'eg_img', 'eg_status'], 'required'],
            [['eg_eid'], 'integer'],
            [['eg_status'], 'string'],
            [['eg_createdOn'], 'safe'],
            [['eg_img'], 'image','minWidth' => 595,'maxWidth' => 595,'minHeight' => 585,'maxHeight' => 585,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 2,'maxFiles' => 10],
            [['eg_eid'], 'exist', 'skipOnError' => true, 'targetClass' => Articles::class, 'targetAttribute' => ['eg_eid' => 'ev_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'eg_id' => 'ID',
            'eg_eid' => 'event ID',
            'eg_img' => 'Gallery Image',
            'eg_status' => 'Status',
            'eg_createdOn' => 'Created On',
        ];
    }

    public function attributeHints()
    {
        return [
            'eg_img' => 'Upload 595 x 585 image',
        ];
    }

    /**
     * Gets query for [[EgE]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Events::class, ['ev_id' => 'eg_eid']);
    }
}
