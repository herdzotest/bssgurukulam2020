<?php

namespace admin\models;
 
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $second_name
 * @property string $gender
 * @property string $username
 * @property int $role
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string|null $verification_token
 *
 * @property AlumniCompany[] $alumniCompanies
 * @property ResProDetails[] $resProDetails
 * @property ResoureQualification[] $resoureQualifications
 * @property StudentParentProfile[] $studentParentProfiles
 */
class Students extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'second_name', 'gender', 'username', 'email'], 'required'],
            [['gender'], 'string'],
            [['role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'second_name'], 'string', 'max' => 155],
            ['username','match','pattern'=>'/^(\+|\d)[0-9]{7,10}$/'],
            [['password_hash', 'password_reset_token', 'email', 'verification_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'second_name' => 'Last Name',
            'gender' => 'Gender',
            'username' => 'Mobile No',
            'role' => 'Role',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
        ];
    }

    /**
     * Gets query for [[AlumniCompanies]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlumniCompanies()
    {
        return $this->hasMany(AlumniCompany::class, ['ac_uid' => 'id']);
    }

    /**
     * Gets query for [[ResProDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getResProDetails()
    {
        return $this->hasMany(ResProDetails::class, ['pd_res_id' => 'id']);
    }

    /**
     * Gets query for [[ResoureQualifications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getResoureQualifications()
    {
        return $this->hasMany(ResoureQualification::class, ['rq_rid' => 'id']);
    }

    /**
     * Gets query for [[StudentParentProfiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudentParentProfiles()
    {
        return $this->hasMany(StudentParentProfile::class, ['pp_student_id' => 'id']);
    }
}
