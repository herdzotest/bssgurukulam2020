<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\Achievements;

/**
 * AchievementsSearch represents the model behind the search form of `admin\models\Achievements`.
 */
class AchievementsSearch extends Achievements
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['av_id'], 'integer'],
            [['av_title', 'av_slug', 'av_featuredImage', 'av_desc', 'av_date', 'av_place', 'av_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Achievements::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'av_id' => $this->av_id,
            'av_date' => $this->av_date,
        ]);

        $query->andFilterWhere(['like', 'av_title', $this->av_title])
            ->andFilterWhere(['like', 'av_slug', $this->av_slug])
            ->andFilterWhere(['like', 'av_featuredImage', $this->av_featuredImage])
            ->andFilterWhere(['like', 'av_desc', $this->av_desc])
            ->andFilterWhere(['like', 'av_place', $this->av_place])
            ->andFilterWhere(['like', 'av_status', $this->av_status]);

        return $dataProvider;
    }
}
