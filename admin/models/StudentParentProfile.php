<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "student_parent_profile".
 *
 * @property int $pp_id
 * @property int $pp_student_id
 * @property string $pp_father_name
 * @property string $pp_father_qualification
 * @property string $pp_father_occupation
 * @property string $pp_father_email
 * @property string $pp_father_mobile
 * @property string $pp_mother_name
 * @property string $pp_mother_qualification
 * @property string $pp_mother_occupation
 * @property string $pp_mother_email
 * @property string $pp_mother_mobile
 * @property string $pp_adminission_status
 * @property string $pp_address
 *
 * @property User $ppStudent
 */
class StudentParentProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_parent_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pp_father_name', 'pp_father_qualification', 'pp_father_occupation', 'pp_father_email', 'pp_father_mobile', 'pp_mother_name', 'pp_mother_qualification', 'pp_mother_occupation', 'pp_mother_mobile', 'pp_address'], 'required'],
            [['pp_student_id'], 'integer'],
            [['pp_adminission_status'], 'string'],
            [['pp_father_name', 'pp_mother_name'], 'string', 'max' => 55],
            [['pp_father_qualification', 'pp_father_email', 'pp_mother_qualification', 'pp_mother_email'], 'string', 'max' => 25],
            [['pp_father_occupation','pp_mother_occupation'], 'string', 'max' => 35],
            [['pp_address'], 'string', 'max' => 300],
            ['pp_father_mobile','match','pattern'=>'/^(\+|\d)[0-9]{7,10}$/'],
            ['pp_mother_mobile','match','pattern'=>'/^(\+|\d)[0-9]{7,10}$/'],
            [['pp_student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Students::class, 'targetAttribute' => ['pp_student_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pp_id' => 'Pp ID',
            'pp_student_id' => 'Pp Student ID',
            'pp_father_name' => 'Father Name',
            'pp_father_qualification' => 'Father Qualification',
            'pp_father_occupation' => 'Father Occupation',
            'pp_father_email' => 'Father Email',
            'pp_father_mobile' => 'Father Mobile',
            'pp_mother_name' => 'Mother Name',
            'pp_mother_qualification' => 'Mother Qualification',
            'pp_mother_occupation' => 'Mother Occupation',
            'pp_mother_email' => 'Mother Email',
            'pp_mother_mobile' => 'Mother Mobile',
            'pp_adminission_status' => 'Adminission Status',
            'pp_address' => 'Address',
        ];
    }

    /**
     * Gets query for [[PpStudent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPpStudent()
    {
        return $this->hasOne(Students::class, ['id' => 'pp_student_id']);
    }
}
