<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\ResproDetails;

/**
 * ResproDetailsSearch represents the model behind the search form of `admin\models\ResproDetails`.
 */
class ResproDetailsSearch extends ResproDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pd_id', 'pd_res_id', 'pd_dp_id', 'pd_des_id', 'pd_res_sub'], 'integer'],
            [['pd_res_type', 'pd_status', 'pd_updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ResproDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pd_id' => $this->pd_id,
            'pd_res_id' => $this->pd_res_id,
            'pd_dp_id' => $this->pd_dp_id,
            'pd_des_id' => $this->pd_des_id,
            'pd_res_sub' => $this->pd_res_sub,
            'pd_updated_on' => $this->pd_updated_on,
        ]);

        $query->andFilterWhere(['like', 'pd_res_type', $this->pd_res_type])
            ->andFilterWhere(['like', 'pd_status', $this->pd_status]);

        return $dataProvider;
    }
}
