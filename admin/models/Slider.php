<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property int $s_id
 * @property string $s_image
 * @property string $s_title
 * @property string $s_desc
 * @property string $s_createdon
 * @property string $s_status 0=>Inactive;1=>active
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['s_title', 's_desc', 's_status'], 'required'],
            [['s_createdon'], 'safe'],
            [['s_status'], 'string'],
            [['s_image'], 'required', 'on' => 'create'],
            [['s_desc'], 'string', 'max' => 200],
            ['s_image', 'image', 'minWidth' => 400,'maxWidth' => 680, 'minHeight' => 400, 'maxHeight' => 580, 'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 2],
            [['s_title'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            's_id' => 'ID',
            's_image' => 'Image',
            's_title' => 'Title',
            's_desc' => 'Desc',
            's_createdon' => 'Createdon',
            's_status' => 'Status',
        ];
    }
}
