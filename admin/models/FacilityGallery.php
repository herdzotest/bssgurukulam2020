<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "facilty_gallery".
 *
 * @property int $fg_id
 * @property int $fg_fid
 * @property string $fg_img
 * @property string $fg_status
 * @property string $fg_createdOn
 *
 * @property Facilities $fgF
 */
class FacilityGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'facilty_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fg_img', 'fg_status'], 'required'],
            [['fg_fid'], 'integer'],
            [['fg_status'], 'string'],
            [['fg_createdOn'], 'safe'],
            [['fg_img'], 'image','minWidth' => 1024,'minHeight' => 768,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 2,'maxFiles' => 10],
            [['fg_fid'], 'exist', 'skipOnError' => true, 'targetClass' => Facilities::class, 'targetAttribute' => ['fg_fid' => 'f_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'fg_id' => 'ID',
            'fg_fid' => 'Facility Title',
            'fg_img' => 'Images',
            'fg_status' => 'Status',
            'fg_createdOn' => 'Created On',
        ];
    }

    /**
     * Gets query for [[FgF]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFacility()
    {
        return $this->hasOne(Facilities::class, ['f_id' => 'fg_fid']);
    }
}
