<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\EventGallery;

/**
 * EventGallerySearch represents the model behind the search form of `admin\models\EventGallery`.
 */
class EventGallerySearch extends EventGallery
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eg_id', 'eg_eid'], 'integer'],
            [['eg_img', 'eg_status', 'eg_createdOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventGallery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'eg_id' => $this->eg_id,
            'eg_eid' => $this->eg_eid,
            'eg_createdOn' => $this->eg_createdOn,
        ]);

        $query->andFilterWhere(['like', 'eg_img', $this->eg_img])
            ->andFilterWhere(['like', 'eg_status', $this->eg_status]);

        return $dataProvider;
    }
}
