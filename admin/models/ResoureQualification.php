<?php

namespace admin\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "resoure_qualification".
 *
 * @property int $rq_id
 * @property int $rq_rid
 * @property string $rq_course_name
 * @property string $rq_university
 * @property string $rq_yop
 * @property string $rq_mark
 * @property string $rq_status
 * @property string $rq_updated_on
 *
 * @property User $rqR
 */
class ResoureQualification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'resoure_qualification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rq_course_name', 'rq_university', 'rq_yop', 'rq_mark', 'rq_status'], 'required'],
            [['rq_rid'], 'integer'],
            [['rq_yop', 'rq_updated_on'], 'safe'],
            [['rq_status'], 'string'],
            [['rq_course_name', 'rq_university'], 'string', 'max' => 155],
            [['rq_mark'], 'string', 'max' => 20],
            [['rq_rid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['rq_rid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rq_id' => 'ID',
            'rq_rid' => 'Resource ID',
            'rq_course_name' => 'Course Name',
            'rq_university' => 'University',
            'rq_yop' => 'Year Of Pass',
            'rq_mark' => 'Mark/Grade',
            'rq_status' => 'Status',
            'rq_updated_on' => 'Updated On',
        ];
    }

    /**
     * Gets query for [[RqR]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser($id)
    {
         $userModel = User::find()->where(['id' => $id])->one();
         return $userModel;
    }
}
