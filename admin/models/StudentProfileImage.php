<?php
 
namespace admin\models;

use Yii;

/**
 * This is the model class for table "student_profile_image".
 *
 * @property int $sp_image_id
 * @property int $sp_student_id
 * @property string $sp_file_name
 * @property string $sp_added_on
 */
class StudentProfileImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'student_profile_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sp_file_name'], 'required', 'on' => 'create'],
            ['sp_file_name', 'image','extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 1],
            [['sp_added_on'], 'safe'],
            [['sp_file_name'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sp_image_id' => 'Sp Image ID',
            'sp_student_id' => 'Sp Student ID',
            'sp_file_name' => 'Student Photo',
            'sp_added_on' => 'Sp Added On',
        ];
    }
}
