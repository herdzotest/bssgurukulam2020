<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $second_name
 * @property string $gender
 * @property string $username
 * @property int $role
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string|null $verification_token
 *
 * @property ResProDetails[] $resProDetails
 * @property ResoureQualification[] $resoureQualifications
 */
class Alumni extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'second_name', 'gender', 'username', 'email'], 'required'],
            [['gender'], 'string'],
            [['role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'second_name'], 'string', 'max' => 155],
            [['password_hash', 'password_reset_token', 'email', 'verification_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['username'],'match','pattern'=> '/^[0-9]{10}$/','message'=> 'Username can contain only 10 digit number'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'second_name' => 'Last Name',
            'gender' => 'Gender',
            'username' => 'Mobile No',
            'role' => 'Role',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Login Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
        ];
    }

    /**
     * Gets query for [[ResProDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(AlumniProfile::class, ['ap_uid' => 'id']);
    }
    public function getCompany()
    {
        return $this->hasOne(AlumniCompany::class, ['ac_uid' => 'id']);
    }
    
    public static function getImage($id)
    {
        $imageModel = UserImages::find()->where(['pp_uid' => $id])->one();
        return $imageModel->pp_image;
    }
    public function getCountry($id)
    {
        $countryModel = Countries::find()->where(['id' => $id])->one();
        return $countryModel->name;
    }
    public function getState($id)
    {
        $stateModel = States::find()->where(['id' => $id])->one();
        return $stateModel->name;
    }
    public function getCity($id)
    {
        $cityModel = Cities::find()->where(['id' => $id])->one();
        return $cityModel->name;
    }
}
