<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "admission_achievement".
 *
 * @property int $ach_id
 * @property int $student_id
 * @property string $competition_items
 * @property string $competition_level
 * @property string $competition_position
 * @property string $competition_grade
 */
class AdmissionAchievement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admission_achievement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id'], 'integer'],
            [['competition_level'], 'string'],
            [['competition_items'], 'string', 'max' => 25],
            [['competition_position'], 'string', 'max' => 21],
            [['competition_grade'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ach_id' => 'Ach ID',
            'student_id' => 'Student ID',
            'competition_items' => 'Competition Items',
            'competition_level' => 'Competition Level',
            'competition_position' => 'Competition Position',
            'competition_grade' => 'Competition Grade',
        ];
    }
}
