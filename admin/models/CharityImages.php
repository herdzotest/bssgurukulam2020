<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "art_images".
 *
 * @property int $art_image_id pk
 * @property string $art_image_path image path
 * @property string $arts_filename
 * @property string $art_content
 * @property int $arts_id fk.arts_management
 * @property int $sort_order sort order
 * @property int $uploaded_by user_id
 * @property string $uploaded_on date
 */
class CharityImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'art_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['art_image_path'], 'required'],
            [['arts_id', 'sort_order', 'uploaded_by'], 'integer'],
            [['uploaded_on'], 'safe'],
            [['art_image_path'], 'required', 'on' => 'create'],
            ['art_image_path', 'image','maxFiles' => 5,'mimeTypes' => 'image/jpeg', 'minWidth' => 900, 'minHeight' => 480, 'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 2],
            [['art_content'], 'string', 'max' => 100],
            [['arts_filename'], 'string', 'max' => 55],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'art_image_id' => 'Art Image ID',
            'art_image_path' => 'Charity Images',
            'arts_filename' => 'Arts Filename',
            'art_content' => 'Art Content',
            'arts_id' => 'Arts ID',
            'sort_order' => 'Sort Order',
            'uploaded_by' => 'Uploaded By',
            'uploaded_on' => 'Uploaded On',
        ];
    }
}
