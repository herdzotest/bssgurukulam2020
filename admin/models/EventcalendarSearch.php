<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\Eventcalendar;

/**
 * EventcalendarSearch represents the model behind the search form of `admin\models\Eventcalendar`.
 */
class EventcalendarSearch extends Eventcalendar
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_calender_id', 'event_department_id', 'event_organizer_id', 'created_by', 'updated_by', 'event_committee_id'], 'integer'],
            [['event_name', 'event_description', 'event_start_date', 'event_end_date', 'event_venue', 'event_calender_status', 'event_category', 'event_image_path', 'created_on', 'updated_on', 'event_student_name', 'event_class'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Eventcalendar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'event_calender_id' => $this->event_calender_id,
            'event_department_id' => $this->event_department_id,
            'event_organizer_id' => $this->event_organizer_id,
            'event_start_date' => $this->event_start_date,
            'event_end_date' => $this->event_end_date,
            'created_by' => $this->created_by,
            'created_on' => $this->created_on,
            'updated_by' => $this->updated_by,
            'updated_on' => $this->updated_on,
            'event_committee_id' => $this->event_committee_id,
        ]);

        $query->andFilterWhere(['like', 'event_name', $this->event_name])
            ->andFilterWhere(['like', 'event_description', $this->event_description])
            ->andFilterWhere(['like', 'event_venue', $this->event_venue])
            ->andFilterWhere(['like', 'event_calender_status', $this->event_calender_status])
            ->andFilterWhere(['like', 'event_category', $this->event_category])
            ->andFilterWhere(['like', 'event_image_path', $this->event_image_path])
            ->andFilterWhere(['like', 'event_student_name', $this->event_student_name])
            ->andFilterWhere(['like', 'event_class', $this->event_class]);

        return $dataProvider;
    }
}
