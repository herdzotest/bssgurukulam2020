<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\Charity;

/**
 * CharitySearch represents the model behind the search form of `admin\models\Charity`.
 */
class CharitySearch extends Charity
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ch_id'], 'integer'],
            [['ch_title', 'ch_slug', 'ch_featuredImage', 'ch_desc', 'ch_date', 'ch_place', 'ch_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Charity::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ch_id' => $this->ch_id,
            'ch_date' => $this->ch_date,
        ]);

        $query->andFilterWhere(['like', 'ch_title', $this->ch_title])
            ->andFilterWhere(['like', 'ch_slug', $this->ch_slug])
            ->andFilterWhere(['like', 'ch_featuredImage', $this->ch_featuredImage])
            ->andFilterWhere(['like', 'ch_desc', $this->ch_desc])
            ->andFilterWhere(['like', 'ch_place', $this->ch_place])
            ->andFilterWhere(['like', 'ch_status', $this->ch_status]);

        return $dataProvider;
    }
}
