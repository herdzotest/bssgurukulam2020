<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\AlumniCompany;

/**
 * AlumniCompanySearch represents the model behind the search form of `admin\models\AlumniCompany`.
 */
class AlumniCompanySearch extends AlumniCompany
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ac_id', 'ac_uid'], 'integer'],
            [['ac_postion', 'ac_name', 'ac_joined_date', 'ac_relieved_date', 'ac_location', 'ac_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AlumniCompany::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ac_id' => $this->ac_id,
            'ac_uid' => $this->ac_uid,
            'ac_joined_date' => $this->ac_joined_date,
            'ac_relieved_date' => $this->ac_relieved_date,
        ]);

        $query->andFilterWhere(['like', 'ac_postion', $this->ac_postion])
            ->andFilterWhere(['like', 'ac_name', $this->ac_name])
            ->andFilterWhere(['like', 'ac_location', $this->ac_location])
            ->andFilterWhere(['like', 'ac_status', $this->ac_status]);

        return $dataProvider;
    }
}
