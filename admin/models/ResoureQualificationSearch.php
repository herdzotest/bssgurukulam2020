<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\ResoureQualification;

/**
 * ResoureQualificationSearch represents the model behind the search form of `admin\models\ResoureQualification`.
 */
class ResoureQualificationSearch extends ResoureQualification
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rq_id', 'rq_rid'], 'integer'],
            [['rq_course_name', 'rq_university', 'rq_yop', 'rq_mark', 'rq_status', 'rq_updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ResoureQualification::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rq_id' => $this->rq_id,
            'rq_rid' => $this->rq_rid,
            'rq_yop' => $this->rq_yop,
            'rq_updated_on' => $this->rq_updated_on,
        ]);

        $query->andFilterWhere(['like', 'rq_course_name', $this->rq_course_name])
            ->andFilterWhere(['like', 'rq_university', $this->rq_university])
            ->andFilterWhere(['like', 'rq_mark', $this->rq_mark])
            ->andFilterWhere(['like', 'rq_status', $this->rq_status]);

        return $dataProvider;
    }
}
