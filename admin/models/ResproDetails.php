<?php

namespace admin\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "res_pro_details".
 *
 * @property int $pd_id
 * @property int $pd_res_id
 * @property int $pd_dp_id
 * @property int $pd_des_id
 * @property string $pd_res_type
 * @property int $pd_res_sub
 * @property string $pd_status
 * @property string $pd_updated_on
 *
 * @property User $pdRes
 */
class ResproDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'res_pro_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pd_dp_id', 'pd_des_id', 'pd_res_type', 'pd_res_sub'], 'required'],
            [['pd_res_id', 'pd_dp_id', 'pd_des_id', 'pd_res_sub'], 'integer'],
            [['pd_res_type', 'pd_status'], 'string'],
            [['pd_updated_on'], 'safe'],
            [['pd_res_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['pd_res_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pd_id' => 'ID',
            'pd_res_id' => 'Res ID',
            'pd_dp_id' => 'Department',
            'pd_des_id' => 'Designation',
            'pd_res_type' => 'Resourse Type',
            'pd_res_sub' => 'Subject',
            'pd_status' => 'Status',
            'pd_updated_on' => 'Updated On',
        ];
    }

    /**
     * Gets query for [[PdRes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser($id)
    {
        return $userModel = User::find()->where(['id' => $id])->one();
    }
    public function getDepartment($id)
    {
        return $department = Departments::find()->where(['department_id' => $id])->one();
    }
    public function getDesignation($id)
    {
        return $designation = Designation::find()->where(['designation_id' => $id])->one();
    }
    public function getSubject($id)
    {
        return $subject = Subjects::find()->where(['sub_id' => $id])->one();
    }
    
}
