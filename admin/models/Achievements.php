<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "achievements".
 *
 * @property int $av_id
 * @property string $av_title
 * @property string $av_slug
 * @property string $av_featuredImage
 * @property string $av_desc
 * @property string $av_date
 * @property string $av_place
 * @property string $av_status
 */
class Achievements extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'achievements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['av_title', 'av_slug', 'av_desc', 'av_place', 'av_status'], 'required'],
            [['av_desc', 'av_status'], 'string'],
            [['av_date'], 'safe'],
            [['av_title', 'av_slug'], 'string', 'max' => 155],
            [['av_place'], 'string', 'max' => 55],
            [['av_featuredImage'], 'required', 'on' => 'create'],
            ['av_featuredImage', 'image', 'minWidth' => 260,'maxWidth'=>260,'maxHeight'=>300, 'minHeight' => 300,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'av_id' => 'ID',
            'av_title' => 'Title',
            'av_slug' => 'Slug',
            'av_featuredImage' => 'Featured Image',
            'av_desc' => 'Desc',
            'av_date' => 'Date',
            'av_place' => 'Place',
            'av_status' => 'Status',
        ];
    }

    public function attributeHints()
    {
        return [
            'av_featuredImage' => 'Upload 260 x 300 image',
        ];
    }

    public static function getImage($id)
    {
        $imageModel = Achievements::find()->where(['av_id' => $id])->one();
        return $imageModel->av_featuredImage;
    }

}
