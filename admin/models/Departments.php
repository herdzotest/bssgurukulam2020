<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "mst_department".
 *
 * @property int $department_id Department id - primary key ,autoincreament 
 * @property string $department_name Name of Department 
 * @property string|null $status Indicates whether the Department is Enable or Disable
 * @property string|null $created_on Created On
 * @property string|null $updated_on Updated on 
 */
class Departments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['department_id', 'department_name'], 'required'],
            [['department_id'], 'integer'],
            [['status'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['department_name'], 'string', 'max' => 100],
            [['department_name'], 'unique'],
            [['department_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'department_id' => 'Department ID',
            'department_name' => 'Department Name',
            'status' => 'Status',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
        ];
    }
}
