<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $second_name
 * @property string $gender
 * @property string $username
 * @property int $role
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string|null $verification_token
 */
class Faculty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'second_name', 'gender', 'username', 'role', 'auth_key', 'password_hash', 'email',], 'required'],
            [['gender'], 'string'],
            [['role', 'username', 'status', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'second_name'], 'string', 'max' => 155],
            [['password_hash', 'password_reset_token', 'email', 'verification_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'second_name' => 'Last Name',
            'gender' => 'Gender',
            'username' => 'Mobile No',
            'role' => 'Role',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
            'type' => 'Faculty Type',
            'department' => 'Department',
        ];
    }
    public function getFaculty()
    {
        return $this->hasOne(ResproDetails::className(), ['pd_res_id' => 'id']);
    }

    public static function getfacultyName($id)
    {
        $facultyModel = Faculty::find()->where(['id' => $id])->one();
        return $facultyModel->first_name.' '.$facultyModel->second_name;
    }

    public static function getfacultySubject($id)
    {
        $facultyModel = ResproDetails::find()->where(['pd_res_id' => $id])->one();
        $facultySubject = Subjects::find()->where(['sub_id' => $facultyModel->pd_res_sub])->one();
        return $facultySubject->sub_name;
    }

    public static function getfacultyEmail($id)
    {
        $facultyModel = Faculty::find()->where(['id' => $id])->one();
        return $facultyModel->email;
    }

    public static function getfacultyQualification($id)
    {
        $facultyModel = ResoureQualification::find()->where(['rq_rid' => $id, 'rq_status' =>'Active'])->one();
        if($facultyModel == NULL){
            return "No Data Available";
        }else{
            return $facultyModel->rq_course_name;
        }
    }

    public static function getfacultyExperience($id)
    {
        $facultyModel = ResExperience::find()->where(['re_rid' => $id])->all();

        return $facultyModel;
    }

    public function getType() {
        return $this->faculty->pd_res_type;
    }
    public function getDepartment()
    {
        $department_id = $this->faculty->pd_dp_id;
        $deparmentModel = Departments::find()->where(['department_id' => $department_id])->one();
        return $deparmentModel->department_name;
    }
    public static function getImage($id)
    {
        $imageModel = UserImages::find()->where(['pp_uid' => $id])->one();
        return $imageModel->pp_image;
    }

}
