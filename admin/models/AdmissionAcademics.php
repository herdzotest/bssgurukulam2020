<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "admission_academics".
 *
 * @property int $ac_id
 * @property int $ac_student_id
 * @property string $admission_class
 * @property string $student_last_school
 * @property string $student_brother_sisters
 * @property string $student_bs_name
 * @property string $student_class
 */
class AdmissionAcademics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admission_academics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admission_class', 'student_last_school', 'student_brother_sisters'], 'required'],
            [['ac_student_id'], 'integer'],
            [['admission_class', 'student_brother_sisters'], 'string'],
            [['student_last_school'], 'string', 'max' => 35],
            [['ac_application_no'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ac_id' => 'Ac ID',
            'ac_student_id' => 'Ac Student ID',
            'admission_class' => 'Admission Class',
            'student_last_school' => 'Student Last School',
            'student_brother_sisters' => 'Student Brother Sisters',
            'ac_application_no' => 'Application Number',
        ];
    }
}
