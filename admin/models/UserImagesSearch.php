<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\UserImages;

/**
 * UserImagesSearch represents the model behind the search form of `admin\models\UserImages`.
 */
class UserImagesSearch extends UserImages
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pp_id', 'pp_uid'], 'integer'],
            [['pp_image', 'pp_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserImages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pp_id' => $this->pp_id,
            'pp_uid' => $this->pp_uid,
        ]);

        $query->andFilterWhere(['like', 'pp_image', $this->pp_image])
            ->andFilterWhere(['like', 'pp_status', $this->pp_status]);

        return $dataProvider;
    }
}
