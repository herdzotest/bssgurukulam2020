<?php

namespace admin\models;
 
use Yii;

/**
 * This is the model class for table "articles".
 *
 * @property int $ar_id
 * @property string $ar_title
 * @property string $ar_featured_image
 * @property string $ar_content
 * @property string $ar_created_by
 * @property string $ar_creator_class
 * @property string $ar_created_on
 * @property string $ar_published_on
 * @property string $ar_status
 */
class Articles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ar_title','ar_slug', 'ar_content', 'ar_created_by', 'ar_creator_class', 'ar_status'], 'required'],
            [['ar_content','ar_slug', 'ar_creator_class', 'ar_status'], 'string'],
            [['ar_created_on', 'ar_published_on'], 'safe'],
            [['ar_title', 'ar_created_by'], 'string', 'max' => 155],
            [['ar_featured_image'], 'required', 'on' => 'create'],
            ['ar_featured_image', 'image', 'minWidth' => 260,'maxWidth'=>260,'maxHeight'=>300, 'minHeight' => 300,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ar_id' => 'ID',
            'ar_title' => 'Title',
            'ar_featured_image' => 'Featured Image',
            'ar_content' => 'Content',
            'ar_slug' => 'Slug',
            'ar_created_by' => 'Created By',
            'ar_creator_class' => 'Class',
            'ar_created_on' => 'Created On',
            'ar_published_on' => 'Published On',
            'ar_status' => 'Status',
        ];
    }

    public function attributeHints()
    {
        return [
            'ar_featured_image' => 'Upload 260 x 300 image',
        ];
    }
    public static function getImage($id)
    {
        $imageModel = Articles::find()->where(['ar_id' => $id])->one();
        return $imageModel->ar_featured_image;
    }


}
