<?php

namespace admin\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "alumniCompany".
 *
 * @property int $ac_id
 * @property int $ac_uid
 * @property string $ac_postion
 * @property string $ac_name
 * @property string $ac_joined_date
 * @property string $ac_relieved_date
 * @property string $ac_location
 * @property string $ac_status
 *
 * @property User $acU
 */
class AlumniCompany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumniCompany';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ac_uid', 'ac_postion', 'ac_name', 'ac_joined_date', 'ac_relieved_date', 'ac_location', 'ac_status'], 'required'],
            [['ac_uid'], 'integer'],
            [['ac_joined_date', 'ac_relieved_date'], 'safe'],
            [['ac_location', 'ac_status'], 'string'],
            [['ac_postion', 'ac_name'], 'string', 'max' => 155],
            [['ac_uid'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['ac_uid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ac_id' => 'ID',
            'ac_uid' => 'user_id',
            'ac_postion' => 'Your Postion',
            'ac_name' => 'Company Name',
            'ac_joined_date' => 'Company Joined Date',
            'ac_relieved_date' => 'Company Relieved Date',
            'ac_location' => 'Company Location',
            'ac_status' => 'Current Company',
        ];
    }

    /**
     * Gets query for [[AcU]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcU()
    {
        return $this->hasOne(User::class, ['id' => 'ac_uid']);
    }
}
