<?php

namespace admin\models;
 
use Yii;

/**
 * This is the model class for table "visitor_comments".
 *
 * @property int $vc_id
 * @property string $vc_image
 * @property string $vc_comment
 * @property string $vc_name
 * @property string $vc_created_on
 * @property string $vc_status
 */
class VisitorComments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visitor_comments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vc_comment', 'vc_name', 'vc_status'], 'required'],
            [['vc_comment','vc_status'], 'string'],
            [['vc_created_on'], 'safe'],
            [['vc_name'], 'string', 'max' => 155],
            [['vc_image'], 'required', 'on' => 'create'],
            ['vc_image', 'image', 'minWidth' => 150,'maxWidth'=>150,'maxHeight'=>150, 'minHeight' => 150,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vc_id' => 'Visitor ID',
            'vc_image' => 'Visitor Image',
            'vc_comment' => 'Visitor Comments',
            'vc_name' => 'Visitor Name',
            'vc_created_on' => 'Created On',
            'vc_status' => 'Status',
        ];
    }

    public function attributeHints()
    {
        return [
            'vc_image' => 'Upload 150 x 150 image',
        ];
    }
    
}
