<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\VisitorComments;

/**
 * VisitorCommentsSearch represents the model behind the search form of `admin\models\VisitorComments`.
 */
class VisitorCommentsSearch extends VisitorComments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vc_id'], 'integer'],
            [['vc_image', 'vc_comment', 'vc_name', 'vc_created_on', 'vc_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VisitorComments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vc_id' => $this->vc_id,
            'vc_created_on' => $this->vc_created_on,
        ]);

        $query->andFilterWhere(['like', 'vc_image', $this->vc_image])
            ->andFilterWhere(['like', 'vc_comment', $this->vc_comment])
            ->andFilterWhere(['like', 'vc_name', $this->vc_name])
            ->andFilterWhere(['like', 'vc_status', $this->vc_status]);

        return $dataProvider;
    }
}
