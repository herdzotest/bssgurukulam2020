<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "charity".
 *
 * @property int $ch_id
 * @property string $ch_title
 * @property string $ch_slug
 * @property string $ch_featuredImage
 * @property string $ch_desc
 * @property string $ch_date
 * @property string $ch_place
 * @property string $ch_status
 */
class Charity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ch_title', 'ch_slug', 'ch_desc', 'ch_place', 'ch_status'], 'required'],
            [['ch_desc', 'ch_status'], 'string'],
            [['ch_date'], 'safe'],
            [['ch_title', 'ch_slug'], 'string', 'max' => 155],
            [['ch_place'], 'string', 'max' => 55],
            [['ch_featuredImage'], 'required', 'on' => 'create'],
            ['ch_featuredImage', 'image', 'minWidth' => 260,'maxWidth'=>260,'maxHeight'=>300, 'minHeight' => 300,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ch_id' => 'ID',
            'ch_title' => 'Title',
            'ch_slug' => 'Slug',
            'ch_featuredImage' => 'Featured Image',
            'ch_desc' => 'Descripiton',
            'ch_date' => 'Date',
            'ch_place' => 'Place',
            'ch_status' => 'Status',
        ];
    }

    public function attributeHints()
    {
        return [
            'ch_featuredImage' => 'Upload 260 x 300 image',
        ];
    }

    public static function getImage($id)
    {
        $imageModel = Charity::find()->where(['ch_id' => $id])->one();
        return $imageModel->ch_featuredImage;
    }

}
