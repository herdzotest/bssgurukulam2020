<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "page_banners".
 *
 * @property int $pb_id
 * @property string $pb_image
 * @property string $pb_page_name
 * @property string $pb_status
 * @property string $pb_createdon
 */
class PageBanners extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_banners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pb_image', 'pb_page_name', 'pb_status'], 'required'],
            [['pb_page_name', 'pb_status'], 'string'],
            [['pb_createdon'], 'safe'],
            [['pb_image'], 'required', 'on' => 'create'],
            ['pb_image', 'image', 'minWidth' => 1400, 'minHeight' => 350,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pb_id' => 'ID',
            'pb_image' => 'Image',
            'pb_page_name' => 'Page Name',
            'pb_status' => 'Status',
            'pb_createdon' => 'Createdon',
        ];
    }
}
