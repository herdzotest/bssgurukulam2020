<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\ParentMenu;

/**
 * ParentMenuSearch represents the model behind the search form of `admin\models\ParentMenu`.
 */
class ParentMenuSearch extends ParentMenu
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pm_id'], 'integer'],
            [['pm_name', 'pm_slug', 'pm_createdon', 'pm_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ParentMenu::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pm_id' => $this->pm_id,
            'pm_createdon' => $this->pm_createdon,
        ]);

        $query->andFilterWhere(['like', 'pm_name', $this->pm_name])
            ->andFilterWhere(['like', 'pm_slug', $this->pm_slug])
            ->andFilterWhere(['like', 'pm_status', $this->pm_status]);

        return $dataProvider;
    }
}
