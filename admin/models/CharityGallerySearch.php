<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\CharityGallery;

/**
 * CharityGallerySearch represents the model behind the search form of `admin\models\CharityGallery`.
 */
class CharityGallerySearch extends CharityGallery
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cg_id', 'cg_cid'], 'integer'],
            [['cg_img', 'cg_status', 'cg_createdOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CharityGallery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cg_id' => $this->cg_id,
            'cg_cid' => $this->cg_cid,
            'cg_createdOn' => $this->cg_createdOn,
        ]);

        $query->andFilterWhere(['like', 'cg_img', $this->cg_img])
            ->andFilterWhere(['like', 'cg_status', $this->cg_status]);

        return $dataProvider;
    }
}
