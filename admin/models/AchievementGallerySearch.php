<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\AchievementGallery;

/**
 * AchievementGallerySearch represents the model behind the search form of `admin\models\AchievementGallery`.
 */
class AchievementGallerySearch extends AchievementGallery
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ag_id', 'ag_aid'], 'integer'],
            [['ag_img', 'ag_status', 'ag_createdOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AchievementGallery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ag_id' => $this->ag_id,
            'ag_aid' => $this->ag_aid,
            'ag_createdOn' => $this->ag_createdOn,
        ]);

        $query->andFilterWhere(['like', 'ag_img', $this->ag_img])
            ->andFilterWhere(['like', 'ag_status', $this->ag_status]);

        return $dataProvider;
    }
}
