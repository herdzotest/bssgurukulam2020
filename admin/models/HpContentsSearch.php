<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\HpContents;

/**
 * HpContentsSearch represents the model behind the search form of `admin\models\HpContents`.
 */
class HpContentsSearch extends HpContents
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hc_id'], 'integer'],
            [['hc_title', 'hc_content', 'hc_image', 'hc_urls', 'hc_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HpContents::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'hc_id' => $this->hc_id,
        ]);

        $query->andFilterWhere(['like', 'hc_title', $this->hc_title])
            ->andFilterWhere(['like', 'hc_content', $this->hc_content])
            ->andFilterWhere(['like', 'hc_image', $this->hc_image])
            ->andFilterWhere(['like', 'hc_urls', $this->hc_urls])
            ->andFilterWhere(['like', 'hc_status', $this->hc_status]);

        return $dataProvider;
    }
}