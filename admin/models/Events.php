<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property int $ev_id
 * @property string $ev_title
 * @property string $ev_slug
 * @property string $ev_featuredImg
 * @property string $ev_desc
 * @property string $ev_date
 * @property string $ev_place
 * @property string $ev_status
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ev_title', 'ev_slug', 'ev_desc', 'ev_place', 'ev_status'], 'required'],
            [['ev_desc', 'ev_status'], 'string'],
            [['ev_date'], 'safe'],
            [['ev_title', 'ev_slug'], 'string', 'max' => 155],
            [['ev_place'], 'string', 'max' => 55],
            [['ev_featuredImg'], 'required', 'on' => 'create'],
            ['ev_featuredImg', 'image', 'minWidth' => 260,'maxWidth'=>260,'maxHeight'=>300, 'minHeight' => 300,'extensions' => 'jpg, jpeg, png', 'maxSize' => 1024 * 1024 * 1],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ev_id' => 'ID',
            'ev_title' => 'Title',
            'ev_slug' => 'Slug',
            'ev_featuredImg' => 'Featured Img',
            'ev_desc' => 'Desc',
            'ev_date' => 'Date',
            'ev_place' => 'Place',
            'ev_status' => 'Status',
        ];
    }

    public function attributeHints()
    {
        return [
            'ev_featuredImg' => 'Upload 260 x 300 image',
        ];
    }


    public static function getImage($id)
    {
        $imageModel = Events::find()->where(['ev_id' => $id])->one();
        return $imageModel->ev_featuredImage;
    }

}
