<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use admin\models\ResExperience;

/**
 * ResExperienceSearch represents the model behind the search form of `admin\models\ResExperience`.
 */
class ResExperienceSearch extends ResExperience
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['re_id', 're_rid'], 'integer'],
            [['re_institute', 're_role', 're_start_date', 're_end_date', 're_status', 're_updatedOn'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ResExperience::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            're_id' => $this->re_id,
            're_rid' => $this->re_rid,
            're_start_date' => $this->re_start_date,
            're_end_date' => $this->re_end_date,
            're_updatedOn' => $this->re_updatedOn,
        ]);

        $query->andFilterWhere(['like', 're_institute', $this->re_institute])
            ->andFilterWhere(['like', 're_role', $this->re_role])
            ->andFilterWhere(['like', 're_status', $this->re_status]);

        return $dataProvider;
    }
}
