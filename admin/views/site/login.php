<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Admin || Login';
?>
<!-- login page  -->
<!-- ============================================================== -->
<div class="splash-container">
    <div class="card ">
        <div class="card-header text-center"><a href="../index.html"><img class="logo-img" src="<?= Yii::$app->homeUrl; ?>theme/assets/images/logo.png" alt="logo"></a><span class="splash-description">Please enter the admin credentials.</span></div>
        <div class="card-body">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true,'class' => 'form-control form-control-lg', 'placeholder'=>'Username'])->label(false) ?>

            <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control form-control-lg', 'placeholder'=>'Password'])->label(false)?>

            <?= $form->field($model, 'rememberMe')->checkbox(['class'=>'ustom-control-input']) ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-lg btn-block', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="card-footer bg-white p-0  ">
            <div class="card-footer-item card-footer-item-bordered">
                <a href="#" class="footer-link">Forgot Password</a>
            </div>
        </div>
    </div>
</div>

<!-- ============================================================== -->
<!-- end login page  -->