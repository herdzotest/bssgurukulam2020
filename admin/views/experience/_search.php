<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\ResExperienceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="res-experience-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 're_id') ?>

    <?= $form->field($model, 're_rid') ?>

    <?= $form->field($model, 're_institute') ?>

    <?= $form->field($model, 're_role') ?>

    <?= $form->field($model, 're_start_date') ?>

    <?php // echo $form->field($model, 're_end_date') ?>

    <?php // echo $form->field($model, 're_status') ?>

    <?php // echo $form->field($model, 're_updatedOn') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
