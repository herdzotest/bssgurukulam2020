<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\ResoureQualification */

$this->title = 'Resource Experience';
$this->params['breadcrumbs'][] = ['label' => 'Faculty', 'url' => ['faculty/index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = $model->getUser($_GET['id'])->first_name;
?>
<div class="resoure-qualification-create view-block">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
