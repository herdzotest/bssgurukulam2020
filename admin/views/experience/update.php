<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\ResExperience */

$this->title = 'Experience: ' . $model->getUser($model->re_rid)->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Faculties', 'url' => ['faculty/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = ['label' => $model->getUser($model->re_rid)->first_name, 'url' => ['faculty/view', 'id' => $model->re_rid]];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="res-experience-update view-block">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
