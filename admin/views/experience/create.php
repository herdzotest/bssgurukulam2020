<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\ResExperience */

$this->title = 'Faulty | Experience Details';
$this->params['breadcrumbs'][] = ['label' => 'Faculty', 'url' => ['faculty/index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = $model->getUser($_GET['id'])->first_name;
?>
<div class="res-experience-create view-block">

<h2>Experience Details [<?= $model->getUser($_GET['id'])->first_name;?>] </h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
