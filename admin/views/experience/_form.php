<?php

use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\ResExperience */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="res-experience-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 're_institute')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 're_role')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 're_start_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'dd-MM-yyyy',
                'options' => ['class' => 'form-control'],
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 're_end_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'dd-MM-yyyy',
                'options' => ['class' => 'form-control'],
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 're_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive',], ['prompt' => '---Select Status---']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save & Continue', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>