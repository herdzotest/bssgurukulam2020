<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\AlumniProfile */

$this->title = 'Create Alumni Profile';
$this->params['breadcrumbs'][] = ['label' => 'Alumni Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumni-profile-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
