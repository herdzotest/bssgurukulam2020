<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\AlumniProfile */

$this->title = 'Update Alumni Profile: ' . $model->ap_id;
$this->params['breadcrumbs'][] = ['label' => 'Alumni Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ap_id, 'url' => ['view', 'id' => $model->ap_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alumni-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
