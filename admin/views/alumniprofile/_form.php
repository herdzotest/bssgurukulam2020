<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\AlumniProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alumni-profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ap_uid')->textInput() ?>

    <?= $form->field($model, 'ap_approved_uid')->textInput() ?>

    <?= $form->field($model, 'ap_batch_from')->textInput() ?>

    <?= $form->field($model, 'ap_batch_to')->textInput() ?>

    <?= $form->field($model, 'ap_address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ap_country')->textInput() ?>

    <?= $form->field($model, 'ap_state')->textInput() ?>

    <?= $form->field($model, 'ap_city')->textInput() ?>

    <?= $form->field($model, 'ap_pincode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ap_qualification')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ap_linkedin_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ap_facebook_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ap_twitter_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ap_class')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
