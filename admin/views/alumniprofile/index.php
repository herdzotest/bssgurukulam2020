<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\AlumniProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumni Profiles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumni-profile-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alumni Profile', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ap_id',
            'ap_uid',
            'ap_approved_uid',
            'ap_batch_from',
            'ap_batch_to',
            //'ap_address:ntext',
            //'ap_country',
            //'ap_state',
            //'ap_city',
            //'ap_pincode',
            //'ap_qualification',
            //'ap_linkedin_link',
            //'ap_facebook_link',
            //'ap_twitter_link',
            //'ap_class',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
