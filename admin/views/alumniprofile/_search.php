<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\AlumniProfileSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alumni-profile-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ap_id') ?>

    <?= $form->field($model, 'ap_uid') ?>

    <?= $form->field($model, 'ap_approved_uid') ?>

    <?= $form->field($model, 'ap_batch_from') ?>

    <?= $form->field($model, 'ap_batch_to') ?>

    <?php // echo $form->field($model, 'ap_address') ?>

    <?php // echo $form->field($model, 'ap_country') ?>

    <?php // echo $form->field($model, 'ap_state') ?>

    <?php // echo $form->field($model, 'ap_city') ?>

    <?php // echo $form->field($model, 'ap_pincode') ?>

    <?php // echo $form->field($model, 'ap_qualification') ?>

    <?php // echo $form->field($model, 'ap_linkedin_link') ?>

    <?php // echo $form->field($model, 'ap_facebook_link') ?>

    <?php // echo $form->field($model, 'ap_twitter_link') ?>

    <?php // echo $form->field($model, 'ap_class') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
