<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\AlumniProfile */

$this->title = $model->ap_id;
$this->params['breadcrumbs'][] = ['label' => 'Alumni Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alumni-profile-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ap_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ap_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ap_id',
            'ap_uid',
            'ap_approved_uid',
            'ap_batch_from',
            'ap_batch_to',
            'ap_address:ntext',
            'ap_country',
            'ap_state',
            'ap_city',
            'ap_pincode',
            'ap_qualification',
            'ap_linkedin_link',
            'ap_facebook_link',
            'ap_twitter_link',
            'ap_class',
        ],
    ]) ?>

</div>
