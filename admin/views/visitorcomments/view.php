<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\VisitorComments */

$this->title = $model->vc_name;
$this->params['breadcrumbs'][] = ['label' => 'Visitor Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="visitor-comments-view">
    <h1><?= Html::encode($this->title) ?>
        <span class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->vc_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->vc_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </span>
    </h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                [
                    'attribute' => 'vc_image',
                    'value' => Yii::$app->homeUrl . '../theme/img/' . $model->vc_image,
                    'format' => ['image', ['width' => '150', 'height' => '150']],
                ],
            'vc_comment:html',
            'vc_name',
            'vc_created_on:date',
            'vc_status',
        ],
    ]) ?>

</div>
