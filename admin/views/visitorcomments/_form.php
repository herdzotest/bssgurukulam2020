<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model admin\models\VisitorComments */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="visitor-comments-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'vc_image')->widget(FileInput::class, [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
            'showUpload' => true,
            'initialPreview' => [
                $model->vc_image ? Html::img(Yii::$app->homeUrl . '../theme/img/' . $model->vc_image) : null, // checks the models to display the preview
            ],
            'overwriteInitial' => false,
        ],
    ]); ?>
    <?= $form->field($model, 'vc_comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'vc_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vc_created_on')->widget(\yii\jui\DatePicker::class, [
        'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control', 'placeholder' => '-- Select Date --'],
    ]) ?>

    <?= $form->field($model, 'vc_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive',], ['prompt' => '-- Select Status--']) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript" src="<?= Yii::$app->homeUrl ?>theme/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('visitorcomments-vc_comment', {
        extraPlugins: 'bt_table',
        // extraPlugins : 'btgrid',
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?type=images",
        filebrowserBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=files",
        filebrowserImageBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=images",
        filebrowserFlashBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=flash",
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=files",
        filebrowserImageUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=images",
        filebrowserFlashUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=flash",
    });

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].on("instanceReady", function() {
            //set keyup event
            this.document.on("keyup", function() {
                CKEDITOR.instances[instance].updateElement();
            });
            //and paste event
            this.document.on("paste", function() {
                CKEDITOR.instances[instance].updateElement();
            });

        });
    }
</script>
