<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\VisitorComments */

$this->title = 'Create Visitor Comments';
$this->params['breadcrumbs'][] = ['label' => 'Visitor Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visitor-comments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
