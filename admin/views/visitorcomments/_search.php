<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\VisitorCommentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="visitor-comments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'vc_id') ?>

    <?= $form->field($model, 'vc_image') ?>

    <?= $form->field($model, 'vc_comment') ?>

    <?= $form->field($model, 'vc_name') ?>

    <?= $form->field($model, 'vc_created_on') ?>

    <?php // echo $form->field($model, 'vc_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
