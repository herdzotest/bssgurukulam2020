<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\VisitorComments */

$this->title = 'Update Visitor Comments: ' . $model->vc_name;
$this->params['breadcrumbs'][] = ['label' => 'Visitor Comments', 'url' => ['index']];
$this->params['breadcrumbs'][] = '> ';
$this->params['breadcrumbs'][] = ['label' => $model->vc_name, 'url' => ['view', 'id' => $model->vc_id]];
$this->params['breadcrumbs'][] = '> ';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="visitor-comments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
