<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\EventsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ev_id') ?>

    <?= $form->field($model, 'ev_title') ?>

    <?= $form->field($model, 'ev_slug') ?>

    <?= $form->field($model, 'ev_featuredImg') ?>

    <?= $form->field($model, 'ev_desc') ?>

    <?php // echo $form->field($model, 'ev_date') ?>

    <?php // echo $form->field($model, 'ev_place') ?>

    <?php // echo $form->field($model, 'ev_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
