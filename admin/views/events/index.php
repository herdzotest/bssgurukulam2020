<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index">

    <h1><?= Html::encode($this->title) ?>

 
        <?= Html::a('Create Events', ['create'], ['class' => 'btn btn-success pull-right']) ?>

    </h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ev_title',
            // 'ev_desc:ntext',
            'ev_date:date',
            //'ev_place',
            'ev_status',

            //['class' => 'yii\grid\ActionColumn'],

            [
                'headerOptions' => ['width' => '250'],
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view} {update} {delete} {gallery}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-danger fa fa-trash',
                            'title' => Yii::t('app', 'Delete'), 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this event?'), 'data-method' => 'post'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-primary fas fa-pencil-alt',
                            'title' => Yii::t('app', 'Update Event')
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-info fas fa-eye',
                            'title' => Yii::t('app', 'View Event')
                        ]);
                    }, 
                    'gallery' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-warning fas fa-image',
                            'title' => Yii::t('app', 'Add Gallery')
                        ]);
                    },     
                ],

                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url = Yii::$app->homeUrl . 'events/' . $model->ev_id;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url = Yii::$app->homeUrl . 'events/update/' . $model->ev_id;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url = Yii::$app->homeUrl . 'events/delete/' . $model->ev_id;
                        return $url;
                    }
                    if ($action === 'gallery') {
                        $url = Yii::$app->homeUrl . 'eventgallery/images/' . $model->ev_id;
                        return $url;
                    }
                }

            ],

        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
