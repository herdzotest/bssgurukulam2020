<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Events */

$this->title = 'Update Events: ' . $model->ev_title;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = ['label' => $model->ev_title, 'url' => ['view', 'id' => $model->ev_id]];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="events-update view-block">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
