<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model admin\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ev_title')->textInput(['maxlength' => true]) ?>

    <div class="slug">
        <?= $form->field($model, 'ev_slug')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
    </div>

    <?= $form->field($model, 'ev_featuredImg')->widget(FileInput::class, [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
            'showUpload' => true,
            'initialPreview' => [
                $model->ev_featuredImg ? Html::img(Yii::$app->homeUrl.'../theme/img/events/featuredImages/'.$model->ev_featuredImg) : null, // checks the models to display the preview
            ],
            'overwriteInitial' => false,
        ],
    ]); ?>

    <?= $form->field($model, 'ev_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ev_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control', 'placeholder' => '-- Select Date --'],
    ]) ?>

    <?= $form->field($model, 'ev_place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ev_status')->dropDownList(['Published' => 'Published', 'Draft' => 'Draft', 'Deleted' => 'Deleted', ], ['prompt' => '-- Select Event Status--']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(document).ready(function() {
        $(".slug").hide();
    });
    $("#events-ev_title").focusout(function() {
        var title = $("#events-ev_title").val().toLowerCase();
        var title = title.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        var res = title.replace(/ /g, "-");
        var res = res.replace("--", "-", res);
        $("#events-ev_slug").val(res);
        $(".slug").show();

    });
</script>
<script type="text/javascript" src="<?= Yii::$app->homeUrl ?>theme/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('events-ev_desc', {
        extraPlugins: 'bt_table',
        // extraPlugins : 'btgrid',
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?type=images",
        filebrowserBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=files",
        filebrowserImageBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=images",
        filebrowserFlashBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=flash",
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=files",
        filebrowserImageUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=images",
        filebrowserFlashUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=flash",
    });

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].on("instanceReady", function() {
            //set keyup event
            this.document.on("keyup", function() {
                CKEDITOR.instances[instance].updateElement();
            });
            //and paste event
            this.document.on("paste", function() {
                CKEDITOR.instances[instance].updateElement();
            });

        });
    }
</script>
