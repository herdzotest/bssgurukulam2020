<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\Events */

$this->title = $model->ev_title;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="events-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <span class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->ev_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ev_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </span>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ev_title',
            'ev_slug',
                [
                    'attribute' => 'ev_featuredImg',
                    'value' => Yii::$app->homeUrl . '../theme/img/events/featuredImages/' . $model->ev_featuredImg,
                    'format' => ['image', ['width' => '200', 'height' => '200']],
                ],
            'ev_desc:html',
            'ev_date:date',
            'ev_place',
            'ev_status',
        ],
    ]) ?>

</div>
