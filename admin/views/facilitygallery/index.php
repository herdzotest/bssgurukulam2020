<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\FaciltyGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Facility Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facilty-gallery-index">

    <h1><?= Html::encode($this->title) ?>

            <?= Html::a('Add Facilty Gallery', ['create'], ['class' => 'btn btn-success pull-right']) ?>

    </h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'facility.f_title',

            [
                'headerOptions' => ['width' => '250'],
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-info fas fa-eye',
                            'title' => Yii::t('app', 'View Facility')
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {

                    if ($action === 'view') {
                        $url = Yii::$app->homeUrl . 'facilitygallery/images/' . $model->fg_fid;
                        return $url;
                    }
                }

            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>