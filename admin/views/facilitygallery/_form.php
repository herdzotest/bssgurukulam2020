<?php

use admin\models\Facilities;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model admin\models\FaciltyGallery */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="facilty-gallery-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'] ]); ?>

    <?php if (isset($_GET['id'])) {
        $facility_id = $_GET['id'];
        $facilityModel = Facilities::find()->where(['f_id' => $facility_id])->all();
        $listData = ArrayHelper::map($facilityModel, 'f_id', 'f_title');
        echo $form->field($model, 'fg_fid')->dropDownList($listData);
    } else {
        $facilityModel = Facilities::find()->all();
        $listData = ArrayHelper::map($facilityModel, 'f_id', 'f_title');
        echo $dropdown = $form->field($model, 'fg_fid')->dropDownList($listData, ['prompt' => 'Select Facility']);
    }
    ?>

    <?php echo $form->field($model, 'fg_img[]')->widget(FileInput::class, [
        'options' => [
            'accept' => 'image/*',
            'multiple' => true
        ],
        'pluginOptions' => [
            'maxFileCount' => 10
        ]
    ]);
    ?>

    <?= $form->field($model, 'fg_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive'], ['prompt' => 'Select Image Status']) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>