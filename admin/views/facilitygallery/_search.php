<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\FaciltyGallerySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="facilty-gallery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'fg_id') ?>

    <?= $form->field($model, 'fg_fid') ?>

    <?= $form->field($model, 'fg_img') ?>

    <?= $form->field($model, 'fg_status') ?>

    <?= $form->field($model, 'fg_createdOn') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
