<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\FaciltyGallery */

$this->title = 'Add Facilty Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Facilty Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facilty-gallery-create view-block">

    <h1><?php // Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
