<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\FaciltyGallery */

$this->title = 'Update Facilty Gallery: ' . $model->fg_id;
$this->params['breadcrumbs'][] = ['label' => 'Facilty Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fg_id, 'url' => ['view', 'id' => $model->fg_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="facilty-gallery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
