<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\FaciltyGallery */

$f_title = '';
$f_id = '';
foreach ($model as $gallery) {
    $f_title = $gallery->facility->f_title;
    $f_id = $gallery->fg_fid;
}

$this->title = $f_title;
$this->params['breadcrumbs'][] = ['label' => 'Facilty Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="facilty-gallery-view">

    <h1><?= Html::encode($this->title) ?>

    <span class="pull-right">
        <?= Html::a('Delete', ['delete', 'id' => $f_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this Album?',
                'method' => 'post',
            ],
        ]) ?>
    </span>
    </h1>
    <div class="row">
        <?php foreach ($model as $gallery) { ?>
            <div class="col-md-3">
                <div class="gallery-img">
                    <img src="<?= Yii::$app->homeUrl; ?>../theme/img/gallery/facility/<?= $gallery->fg_img; ?>">
                    <a data-confirm="Are you sure to delete this item?" href="<?= Yii::$app->homeUrl;?>facilitygallery/del/<?=$gallery->fg_id;?>"><button class="btn btn-danger gallery-del"><i class="fa fa-trash" style="margin-right:5px"></i>Delete Image</button></a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>