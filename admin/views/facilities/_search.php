<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\FacilitiesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="facilites-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'f_id') ?>

    <?= $form->field($model, 'f_title') ?>

    <?= $form->field($model, 'f_desc') ?>

    <?= $form->field($model, 'f_status') ?>

    <?= $form->field($model, 'f_slug') ?>

    <?php // echo $form->field($model, 'f_createdOn') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
