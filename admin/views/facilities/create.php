<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Facilites */

$this->title = 'Add Facilites';
$this->params['breadcrumbs'][] = ['label' => 'Facilites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="facilites-create view-block">

    <h1><?php // Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
