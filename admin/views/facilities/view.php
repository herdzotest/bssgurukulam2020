<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\Facilites */

$this->title = $model->f_title;
$this->params['breadcrumbs'][] = ['label' => 'Facilites', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="facilites-view view-block">

    <h1><?= Html::encode($this->title) ?>

        <span class="pull-right">
            <?= Html::a('Add Gallery', ['facilitygallery/create', 'id' => $model->f_id], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Update', ['update', 'id' => $model->f_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->f_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </span>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'f_title',
            'f_desc:html',
            'f_slug',
            'f_status',
            'f_createdOn:date',
        ],
    ]) ?>

</div>