<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Facilites */

$this->title = 'Update Facilites: ' . $model->f_title;
$this->params['breadcrumbs'][] = ['label' => 'Facilites', 'url' => ['index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = ['label' => $model->f_title, 'url' => ['view', 'id' => $model->f_id]];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="facilites-update view-block">

    <h1><?php // Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
