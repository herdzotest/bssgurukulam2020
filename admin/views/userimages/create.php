<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\UserImages */

$this->title = 'Create User Images';
$this->params['breadcrumbs'][] = ['label' => 'User Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-images-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
