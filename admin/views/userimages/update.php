<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\UserImages */

$this->title = 'Update User Images: ' . $model->pp_id;
$this->params['breadcrumbs'][] = ['label' => 'User Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pp_id, 'url' => ['view', 'id' => $model->pp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-images-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
