<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\UserImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-images-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pp_uid')->textInput() ?>

    <?= $form->field($model, 'pp_image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pp_status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
