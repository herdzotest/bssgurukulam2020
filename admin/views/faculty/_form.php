<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\Faculty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faculty-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?php echo $form->field($profilePic, 'pp_image')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true, 'placeholder' => 'Enter The First Name']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'second_name')->textInput(['maxlength' => true, 'placeholder' => 'Enter The Last Name']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'username')->input('integer', ['placeholder' => 'Enter the 10 digit Mobile No']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'email')->input('email', ['placeholder' => 'Enter the Email Id']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'gender')->dropDownList(['Male' => 'Male', 'Female' => 'Female', 'Transgender' => 'Transgender',], ['prompt' => 'Select Gender']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save & Continue', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>