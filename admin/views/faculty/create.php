<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Faculty */

$this->title = 'Add Faculty';
$this->params['breadcrumbs'][] = ['label' => 'Faculties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faculty-create view-block">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'profilePic' => $profilePic,
    ]) ?>

</div>
