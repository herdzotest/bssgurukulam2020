<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model admin\models\Faculty */
// echo '<pre>';print_r($professionalModel);die();
$this->title = $model->first_name . ' ' . $model->second_name;
$this->params['breadcrumbs'][] = ['label' => 'Faculties', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="faculty-view">

    <h1><?= Html::encode($this->title) ?>

        <span class="pull-right">
            
        </span>
    </h1>
    <div class="row">
        <div class="col-md-12 view-block">
            <h4>Resource Profile Details
                <span class="pull-right">
                    <?= Html::a('Update Personal Profile', ['update', 'id' => $model->id], ['class' => 'btn-sm btn-primary']) ?>
                </span>
            </h4>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    [
                        'attribute' => 'Status',
                        'label' => 'Profile Image',
                        'value' => Yii::$app->homeUrl . '../theme/img/pro_images/faculty/' . $model->getImage($model->id),
                        'format' => ['image', ['width' => '100', 'height' => '100']],
                    ],
                    'first_name',
                    'second_name',
                    'gender',
                    'username',
                    'email:email',
                    /*[
                        'attribute' => 'status',
                        'label' => 'Status',
                        'value' => $faculty_status,
                    ], */
                    'created_at:date',
                    'updated_at:date',
                ],
            ]) ?>
        </div>
        <div class="col-md-12 view-block">
            <h4>Resource Professional Details
                <span class="pull-right">
                    <?= Html::a('Update Professional Profile', ['resprodetails/update', 'id' => $professionalModel->pd_id], ['class' => 'btn-sm btn-primary']) ?>
                </span>
            </h4>
            <?= DetailView::widget([
                'model' => $professionalModel,
                'attributes' => [
                    [
                        'attribute' => 'pd_dp_id',
                        'label' => 'Department',
                        'value' => function ($professionalModel) {
                            return $professionalModel->getDepartment($professionalModel->pd_dp_id)->department_name;
                        },
                    ],
                    [
                        'attribute' => 'pd_des_id',
                        'label' => 'Designation',
                        'value' => function ($professionalModel) {
                            return $professionalModel->getDesignation($professionalModel->pd_des_id)->designation_name;
                        },
                    ],
                    'pd_res_type',
                    [
                        'attribute' => 'pd_res_sub',
                        'label' => 'Subject',
                        'value' => function ($professionalModel) {
                            return $professionalModel->getSubject($professionalModel->pd_res_sub)->sub_name;
                        },
                    ],
                    'pd_status',
                    'pd_updated_on:date',
                ],
            ]) ?>
        </div>
        <div class="col-md-12 view-block">
            <div style="margin-top:30px;">
                <h4>Resource Qualification Details
                <span class="pull-right">
                    <?= Html::a('Add Qualifications', ['qualification/add', 'id' => $model->id], ['class' => 'btn-sm btn-success']) ?>
                </span>
                </h4>
                <?= GridView::widget([
                    'dataProvider' => $qualificationModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'rq_course_name',
                        'rq_university',
                        'rq_yop',
                        'rq_mark',
                        'rq_status',
                        'rq_updated_on:date',

                        [
                            'headerOptions' => ['width' => '175'],
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Actions',
                            'template' =>  '{update} {delete}',
                            'buttons' => [
                                'delete' => function ($url, $qualificationModel) {
                                    return Html::a('', $url, [
                                        'class' => 'btn btn-danger fa fa-trash',
                                        'title' => Yii::t('app', 'Delete'), 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this record?'), 'data-method' => 'post'
                                    ]);
                                },
                                'update' => function ($url, $qualificationModel) {
                                    return Html::a('', $url, [
                                        'class' => 'btn btn-primary fas fa-pencil-alt',
                                        'title' => Yii::t('app', 'Update Pages')
                                    ]);
                                },
                            ],
                            'urlCreator' => function ($action, $qualificationModel, $key, $index) {
                  
                                if ($action === 'update') {
                                    $url = Yii::$app->homeUrl . 'qualification/update/' . $qualificationModel->rq_id;
                                    return $url;
                                }
                                if ($action === 'delete') {
                                    $url = Yii::$app->homeUrl . 'qualification/delete/' . $qualificationModel->rq_id;
                                    return $url;
                                }
                            }

                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="col-md-12 view-block">
            <div style="margin-top:30px;">
                <h4>Resource Experience Details
                <span class="pull-right">
                    <?= Html::a('Add Experience', ['experience/add', 'id' => $model->id], ['class' => 'btn-sm btn-success']) ?>
                </span>
                </h4>
                <?= GridView::widget([
                    'dataProvider' => $experienceModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        're_institute',
                        're_role',
                        're_start_date:date',
                        're_end_date:date',
                        're_status',
                        're_updatedOn:date',

                        [
                            'headerOptions' => ['width' => '175'],
                            'class' => 'yii\grid\ActionColumn',
                            'header' => 'Actions',
                            'template' =>  '{update} {delete}',
                            'buttons' => [
                                'delete' => function ($url, $experienceModel) {
                                    return Html::a('', $url, [
                                        'class' => 'btn btn-danger fa fa-trash',
                                        'title' => Yii::t('app', 'Delete'), 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this record?'), 'data-method' => 'post'
                                    ]);
                                },
                                'update' => function ($url, $experienceModel) {
                                    return Html::a('', $url, [
                                        'class' => 'btn btn-primary fas fa-pencil-alt',
                                        'title' => Yii::t('app', 'Update Pages')
                                    ]);
                                },
                            ],
                            'urlCreator' => function ($action, $experienceModel, $key, $index) {
                  
                                if ($action === 'update') {
                                    $url = Yii::$app->homeUrl . 'experience/update/' . $experienceModel->re_id;
                                    return $url;
                                }
                                if ($action === 'delete') {
                                    $url = Yii::$app->homeUrl . 'experience/delete/' . $experienceModel->re_id;
                                    return $url;
                                }
                            }

                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>