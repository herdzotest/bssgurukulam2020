<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\FacultySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Faculties';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faculty-index">

    <h1><?= Html::encode($this->title) ?>


        <?= Html::a('Add New', ['create'], ['class' => 'btn btn-success pull-right']) ?>

    </h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'first_name',
                'format' => 'html',
                'label' => 'Name of the Faculty',
                'value' => function ($data) {
                    return $data->first_name . ' ' . $data->second_name;
                },
            ],
            'username',
            'email:email',
            'type',
            'department',

            [
                'headerOptions' => ['width' => '175'],
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view} {update} {status}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-danger fa fa-trash',
                            'title' => Yii::t('app', 'Delete'), 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'), 'data-method' => 'post'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-primary fas fa-pencil-alt',
                            'title' => Yii::t('app', 'Update Pages')
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-info fas fa-eye',
                            'title' => Yii::t('app', 'View Pages')
                        ]);
                    },
                    'status' => function ($url, $model) {
                        return $model->faculty['pd_status'] == 'Inactive' ?
                        Html::a('', $url, [
                            'class' => 'btn btn-success fas fa-check',
                            'title' => Yii::t('app', ' Activate Faculty')
                        ]) : Html::a('', $url, [
                            'class' => 'btn btn-warning fas fa-window-close',
                            'title' => Yii::t('app', 'Deactivate Faculty')
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                  
                    if ($action === 'view') {
                        $url = Yii::$app->homeUrl . 'faculty/view/' . $model->id;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url = Yii::$app->homeUrl . 'faculty/update/' . $model->id;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url = Yii::$app->homeUrl . 'faculty/delete/' . $model->id;
                        return $url;
                    }
                    if ($action === 'status') {
                        $url = Yii::$app->homeUrl . 'faculty/status/' . $model->id;
                        return $url;
                    }
                }

            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>