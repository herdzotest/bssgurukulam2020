<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Faculty */

$this->title = 'Update Faculty: ' . $model->first_name.' '.$model->second_name;
$this->params['breadcrumbs'][] = ['label' => 'Faculties', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = ['label' => $model->first_name.' '.$model->second_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="faculty-update view-block">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'profilePic' => $profilePic,
    ]) ?>

</div>
