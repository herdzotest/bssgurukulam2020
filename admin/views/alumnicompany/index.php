<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\AlumniCompanySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumni Companies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumni-company-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alumni Company', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ac_id',
            'ac_uid',
            'ac_postion',
            'ac_name',
            'ac_joined_date',
            //'ac_relieved_date',
            //'ac_location:ntext',
            //'ac_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
