<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\AlumniCompany */

$this->title = 'Create Alumni Company';
$this->params['breadcrumbs'][] = ['label' => 'Alumni Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumni-company-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
