<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\AlumniCompany */

$this->title = 'Update Alumni Company: ' . $model->ac_id;
$this->params['breadcrumbs'][] = ['label' => 'Alumni Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ac_id, 'url' => ['view', 'id' => $model->ac_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alumni-company-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
