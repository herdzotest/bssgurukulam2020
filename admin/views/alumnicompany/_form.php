<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\AlumniCompany */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alumni-company-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ac_uid')->textInput() ?>

    <?= $form->field($model, 'ac_postion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ac_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ac_joined_date')->textInput() ?>

    <?= $form->field($model, 'ac_relieved_date')->textInput() ?>

    <?= $form->field($model, 'ac_location')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ac_status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', 'Deleted' => 'Deleted', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
