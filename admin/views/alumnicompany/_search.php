<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\AlumniCompanySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alumni-company-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ac_id') ?>

    <?= $form->field($model, 'ac_uid') ?>

    <?= $form->field($model, 'ac_postion') ?>

    <?= $form->field($model, 'ac_name') ?>

    <?= $form->field($model, 'ac_joined_date') ?>

    <?php // echo $form->field($model, 'ac_relieved_date') ?>

    <?php // echo $form->field($model, 'ac_location') ?>

    <?php // echo $form->field($model, 'ac_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
