<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\AchievementsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Achievements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievements-index">
    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Create Achievements', ['create'], ['class' => 'btn btn-success pull-right']) ?>
        </h1>
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'av_title',
            'av_date:date',
            //'ch_place',
            'av_status',

            //['class' => 'yii\grid\ActionColumn'],
            [
                'headerOptions' => ['width' => '250'],
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view} {update} {delete} {gallery}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-danger fa fa-trash',
                            'title' => Yii::t('app', 'Delete'), 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this achievement?'), 'data-method' => 'post'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-primary fas fa-pencil-alt',
                            'title' => Yii::t('app', 'Update Achievement')
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-info fas fa-eye',
                            'title' => Yii::t('app', 'View Achievement')
                        ]);
                    }, 
                    'gallery' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-warning fas fa-image',
                            'title' => Yii::t('app', 'Add Gallery')
                        ]);
                    },     
                ],

                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url = Yii::$app->homeUrl . 'achievements/' . $model->av_id;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url = Yii::$app->homeUrl . 'achievements/update/' . $model->av_id;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url = Yii::$app->homeUrl . 'achievements/delete/' . $model->av_id;
                        return $url;
                    }
                    if ($action === 'gallery') {
                        $url = Yii::$app->homeUrl . 'achievementgallery/images/' . $model->av_id;
                        return $url;
                    }
                }

            ],

        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
