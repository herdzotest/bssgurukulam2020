<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Achievements */

$this->title = 'Update Achievements: ' . $model->av_title;
$this->params['breadcrumbs'][] = ['label' => 'Achievements', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = ['label' => $model->av_title, 'url' => ['view', 'id' => $model->av_id]];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="achievements-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
