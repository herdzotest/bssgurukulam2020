<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\AchievementsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievements-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'av_id') ?>

    <?= $form->field($model, 'av_title') ?>

    <?= $form->field($model, 'av_slug') ?>

    <?= $form->field($model, 'av_featuredImage') ?>

    <?= $form->field($model, 'av_desc') ?>

    <?php // echo $form->field($model, 'av_date') ?>

    <?php // echo $form->field($model, 'av_place') ?>

    <?php // echo $form->field($model, 'av_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
