<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\Achievements */

$this->title = $model->av_title;
$this->params['breadcrumbs'][] = ['label' => 'Achievements', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="achievements-view">

    <h1><?= Html::encode($this->title) ?>
        <span class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->av_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->av_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </span>
    </h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'av_title',
            'av_slug',
                [
                    'attribute' => 'av_featuredImage',
                    'value' => Yii::$app->homeUrl . '../theme/img/achievements/featuredImages/' . $model->av_featuredImage,
                    'format' => ['image', ['width' => '200', 'height' => '200']],
                ],
            'av_desc:html',
            'av_date:date',
            'av_place',
            'av_status',
        ],
    ]) ?>

</div>
