<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Eventcalendar */

$this->title = 'Update Eventcalendar: ' . $model->event_calender_id;
$this->params['breadcrumbs'][] = ['label' => 'Eventcalendars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->event_calender_id, 'url' => ['view', 'id' => $model->event_calender_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="eventcalendar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
