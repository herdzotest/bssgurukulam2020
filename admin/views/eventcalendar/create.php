<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Eventcalendar */

$this->title = 'Create Eventcalendar';
$this->params['breadcrumbs'][] = ['label' => 'Eventcalendars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eventcalendar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
