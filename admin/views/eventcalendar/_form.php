<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\Eventcalendar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eventcalendar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'event_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'event_department_id')->textInput() ?>

    <?= $form->field($model, 'event_organizer_id')->textInput() ?>

    <?= $form->field($model, 'event_start_date')->textInput() ?>

    <?= $form->field($model, 'event_end_date')->textInput() ?>

    <?= $form->field($model, 'event_venue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_calender_status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'event_category')->dropDownList([ 'News' => 'News', 'Bloomin Buds' => 'Bloomin Buds', 'Articles' => 'Articles', 'Events' => 'Events', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'event_image_path')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'created_on')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'updated_on')->textInput() ?>

    <?= $form->field($model, 'event_student_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_class')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'event_committee_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
