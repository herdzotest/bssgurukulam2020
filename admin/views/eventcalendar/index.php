<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\EventcalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eventcalendar-index">

    <h1><?= Html::encode($this->title) ?>


        <?= Html::a('Create New Event', ['create'], ['class' => 'btn btn-success pull-right']) ?>

        </h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'event_calender_id',
            'event_name',
            // 'event_description:html',
            // 'event_department_id',
            // 'event_organizer_id',
            'event_start_date:date',
            'event_end_date:date',
            //'event_venue',
            //'event_calender_status',
            // 'event_category',
            //'event_image_path',
            //'created_by',
            //'created_on',
            //'updated_by',
            //'updated_on',
            //'event_student_name',
            //'event_class',
            //'event_committee_id',

            [
                'headerOptions' => ['width' => '150'],
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-danger icon-trash',
                            'title' => Yii::t('app', 'Delete'), 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this image?'), 'data-method' => 'post'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-primary icon-pencil',
                            'title' => Yii::t('app', 'Update Charity')
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-info icon-eye-open',
                            'title' => Yii::t('app', 'View Charity')
                        ]);
                    },
                ],

            ],
        ],
    ]); ?>


</div>
