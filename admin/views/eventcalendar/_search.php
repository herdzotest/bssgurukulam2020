<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\EventcalendarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eventcalendar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'event_calender_id') ?>

    <?= $form->field($model, 'event_name') ?>

    <?= $form->field($model, 'event_description') ?>

    <?= $form->field($model, 'event_department_id') ?>

    <?= $form->field($model, 'event_organizer_id') ?>

    <?php // echo $form->field($model, 'event_start_date') ?>

    <?php // echo $form->field($model, 'event_end_date') ?>

    <?php // echo $form->field($model, 'event_venue') ?>

    <?php // echo $form->field($model, 'event_calender_status') ?>

    <?php // echo $form->field($model, 'event_category') ?>

    <?php // echo $form->field($model, 'event_image_path') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_on') ?>

    <?php // echo $form->field($model, 'event_student_name') ?>

    <?php // echo $form->field($model, 'event_class') ?>

    <?php // echo $form->field($model, 'event_committee_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
