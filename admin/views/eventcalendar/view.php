<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\Eventcalendar */

$this->title = $model->event_calender_id;
$this->params['breadcrumbs'][] = ['label' => 'Eventcalendars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="eventcalendar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->event_calender_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->event_calender_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'event_calender_id',
            'event_name',
            'event_description:ntext',
            'event_department_id',
            'event_organizer_id',
            'event_start_date',
            'event_end_date',
            'event_venue',
            'event_calender_status',
            'event_category',
            'event_image_path',
            'created_by',
            'created_on',
            'updated_by',
            'updated_on',
            'event_student_name',
            'event_class',
            'event_committee_id',
        ],
    ]) ?>

</div>
