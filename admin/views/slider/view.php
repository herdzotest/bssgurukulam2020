<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */

$this->title = $model->s_title;
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ' / ' . $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="slider-view">

    <h1><?= Html::encode($this->title) ?>
        <span class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->s_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->s_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </span>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 's_id',
            // 's_image',
            [
                'attribute' => 's_image',
                'value' => Yii::$app->homeUrl . '../theme/img/main-slider/' . $model->s_image,
                'format' => ['image', ['width' => '100', 'height' => '100']],
            ],
            's_title',
            's_desc',
            's_createdon:date',
            // 's_status',
            [
                'attribute' => 's_status',
                'value' => function ($model) {
                    return $model->s_status == '0' ? 'Inactive' : 'Active';
                },
            ],
        ],
    ]) ?>

</div>