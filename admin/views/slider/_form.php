<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 's_title')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 's_image')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 's_desc')->textarea(['row' => '6']) ?>

    <?= $form->field($model, 's_createdon')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 's_status')->dropDownList(['1' => 'Active', '0' => 'Inactive'], ['prompt' => 'Select Status']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
