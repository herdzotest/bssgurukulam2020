<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Home Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <h1><?= Html::encode($this->title) ?>

        <?= Html::a('Create Slider', ['create'], ['class' => 'btn btn-success pull-right']) ?>

    </h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            's_title',
            [
                'attribute' => 's_image',
                'format' => 'html',
                'label' => 'IMAGES',
                'value' => function ($data) {
                    return Html::img(
                        Yii::$app->homeUrl . '../theme/img/main-slider/' . $data['s_image'],
                        ['width' => '150px']
                    );
                },
            ],
            's_createdon:date',
            [
                'attribute' => 's_status',
                'value' => function ($data){
                    return $data['s_status'] == 0 ? 'Inactive' : 'Active';
                },
            ],

            [
                'headerOptions' => ['width' => '250'],
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view} {update} {delete} {status}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-danger fa fa-trash',
                            'title' => Yii::t('app', 'Delete'), 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this image?'), 'data-method' => 'post'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-primary fas fa-pencil-alt',
                            'title' => Yii::t('app', 'Update Slider')
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-info fas fa-eye',
                            'title' => Yii::t('app', 'View Slider')
                        ]);
                    },
                    'status' => function ($url, $model) {
                        return $model->s_status == 0 ?
                        Html::a('', $url, [
                            'class' => 'btn btn-success fas fa-check',
                            'title' => Yii::t('app', ' Active Slider')
                        ]) : Html::a('', $url, [
                            'class' => 'btn btn-warning fas fa-window-close',
                            'title' => Yii::t('app', 'Inactive Slider')
                        ]);
                    },
                ],

            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>