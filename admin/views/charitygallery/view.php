<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\CharityGallery */

$ch_title = $charityModel->ch_title;

$this->title = $ch_title;
$this->params['breadcrumbs'][] = ['label' => 'Charities', 'url' => ['charity/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="charity-gallery-view view-block">

    <h3><?= Html::encode($this->title) ?>

    <span class="pull-right">
       <a href="<?= Yii::$app->homeUrl;?>charitygallery/create/<?= $charityModel->ch_id;?>" class="btn btn-warning">Add New Charity Image</a>
    </span>
    </h3>
    <?php if (!empty($model)) {?>
    <div class="row">
        <?php foreach ($model as $gallery) { ?>
            <div class="col-md-3">
                <div class="gallery-img">
                    <img src="<?= Yii::$app->homeUrl; ?>../theme/img/gallery/charity/<?= $gallery->cg_img; ?>">
                    <a data-confirm="Are you sure to delete this item?" href="<?= Yii::$app->homeUrl;?>charitygallery/del/<?=$gallery->cg_id;?>"><button class="btn btn-danger gallery-del"><i class="fa fa-trash" style="margin-right:5px"></i>Delete Image</button></a>
                </div>
            </div>
        <?php } ?>
    </div>
        <?php }else{ ?>
        <?= "No Images available";?>
        <?php } ?>
</div>
