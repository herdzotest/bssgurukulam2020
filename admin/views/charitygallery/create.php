<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\CharityGallery */

$this->title = 'Create Charity Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Charities', 'url' => ['charity/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charity-gallery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
