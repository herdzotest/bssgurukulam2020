<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\CharityGallerySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charity-gallery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'cg_id') ?>

    <?= $form->field($model, 'cg_cid') ?>

    <?= $form->field($model, 'cg_img') ?>

    <?= $form->field($model, 'cg_status') ?>

    <?= $form->field($model, 'cg_createdOn') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
