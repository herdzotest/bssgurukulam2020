<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\CharityGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Charity Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charity-gallery-index">

    <h1><?= Html::encode($this->title) ?>

        <?= Html::a('Create Charity Gallery', ['create'], ['class' => 'btn btn-success pull-right']) ?>

    </h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cg_id',
            'cg_cid',
            'cg_img',
            'cg_status',
            'cg_createdOn',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
