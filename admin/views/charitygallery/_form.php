<?php
use admin\models\Charity;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model admin\models\CharityGallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charity-gallery-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'] ]); ?>

    <?php if (isset($_GET['id'])) {
        $charity_id = $_GET['id'];
        $charityModel = Charity::find()->where(['ch_id' => $charity_id])->all();
        $listData = ArrayHelper::map($charityModel, 'ch_id', 'ch_title');
        echo $form->field($model, 'cg_cid')->dropDownList($listData);
    } else {
        $charityModel = Charity::find()->all();
        $listData = ArrayHelper::map($charityModel, 'ch_id', 'ch_title');
        echo $dropdown = $form->field($model, 'cg_cid')->dropDownList($listData, ['prompt' => 'Select Charity']);
    }
    ?>

    <?php echo $form->field($model, 'cg_img[]')->widget(FileInput::class, [
        'options' => [
            'accept' => 'image/*',
            'multiple' => true
        ],
        'pluginOptions' => [
            'maxFileCount' => 10
        ]
    ]);
    ?>

    <?= $form->field($model, 'cg_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive'], ['prompt' => 'Select Image Status']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
