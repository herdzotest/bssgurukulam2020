<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\HpContents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hp-contents-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hc_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hc_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'hc_image')->fileInput() ?>

    <?= $form->field($model, 'hc_urls')->dropDownList([
        'system-of-school' => 'System-of-school', 'principal-message' => 'Principal-message',
        'founder' => 'Founder', 'about-us' => 'About-Us', 'charity' => 'Charity',
    ], ['prompt' => 'Select Redirection URL']) ?>

    <?php if (Yii::$app->controller->action->id == 'create') { ?>

        <?= $form->field($model, 'hc_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive',], ['prompt' => 'Select Status']) ?>

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Back', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>