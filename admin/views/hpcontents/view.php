<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\HpContents */

$this->title = $model->hc_title;
$this->params['breadcrumbs'][] = ['label' => 'Hp Contents >', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="hp-contents-view">

    <h1><?= Html::encode($this->title) ?>
        <span class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->hc_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->hc_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </span>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'hc_id',
            'hc_title',
            'hc_content:ntext',
            [
                'attribute' => 'hc_image',
                'value' => Yii::$app->homeUrl . '../theme/img/hc_images/' . $model->hc_image,
                'format' => ['image', ['width' => '100', 'height' => '100']],
            ],
            [
                'attribute' => 'hc_urls',
                'value' => Html::a('bssgurukulam.com/' . $model->hc_urls),
                'format' => 'raw',
            ],
            'hc_status',
        ],
    ]) ?>

</div>