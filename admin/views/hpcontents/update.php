<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\HpContents */

$this->title = 'Update ' . $model->hc_title;
$this->params['breadcrumbs'][] = ['label' => 'Home Page Contents > ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->hc_title.' > ', 'url' => ['view', 'id' => $model->hc_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hp-contents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
