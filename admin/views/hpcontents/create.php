<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\HpContents */

$this->title = 'Create Hp Contents';
$this->params['breadcrumbs'][] = ['label' => 'Hp Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hp-contents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
