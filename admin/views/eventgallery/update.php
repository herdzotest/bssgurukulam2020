<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\EventGallery */

$this->title = 'Update Event Gallery: ' . $model->eg_id;
$this->params['breadcrumbs'][] = ['label' => 'Event Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->eg_id, 'url' => ['view', 'id' => $model->eg_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-gallery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
