<?php
use admin\models\Events;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model admin\models\EventGallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-gallery-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'] ]); ?>

    <?php if (isset($_GET['id'])) {
        $event_id = $_GET['id'];
        $eventModel = Events::find()->where(['ev_id' => $event_id])->all();
        $listData = ArrayHelper::map($eventModel, 'ev_id', 'ev_title');
        echo $form->field($model, 'eg_eid')->dropDownList($listData);
    } else {
        $eventModel = Events::find()->all();
        $listData = ArrayHelper::map($eventModel, 'ev_id', 'ev_title');
        echo $dropdown = $form->field($model, 'eg_eid')->dropDownList($listData, ['prompt' => 'Select Event']);
    }
    ?>

    <?php echo $form->field($model, 'eg_img[]')->widget(FileInput::class, [
        'options' => [
            'accept' => 'image/*',
            'multiple' => true
        ],
        'pluginOptions' => [
            'maxFileCount' => 10
        ]
    ]);
    ?>

    <?= $form->field($model, 'eg_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive'], ['prompt' => 'Select Image Status']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
