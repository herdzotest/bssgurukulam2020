<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\EventGallerySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-gallery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'eg_id') ?>

    <?= $form->field($model, 'eg_eid') ?>

    <?= $form->field($model, 'eg_img') ?>

    <?= $form->field($model, 'eg_status') ?>

    <?= $form->field($model, 'eg_createdOn') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
