<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\EventGallery */

$this->title = 'Create Event Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['events/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-gallery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
