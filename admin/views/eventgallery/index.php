<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\EventGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-gallery-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Event Gallery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'eg_id',
            'eg_eid',
            'eg_img',
            'eg_status',
            'eg_createdOn',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
