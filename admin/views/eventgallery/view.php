<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\EventGallery */

//$this->title = $model->eg_id;
$this->title = $eventModel->ev_title;
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['events/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="event-gallery-view view-block">

    <h3><?= Html::encode($this->title) ?>
        <span class="pull-right">
            <a href="<?= Yii::$app->homeUrl;?>eventgallery/create/<?= $eventModel->ev_id;?>" class="btn btn-warning">Add New Event Image</a>
        </span>
    </h3>

    
    <?php if (!empty($model)) {?>
    <div class="row">
        <?php foreach ($model as $gallery) { ?>
            <div class="col-md-3">
                <div class="gallery-img">
                    <img src="<?= Yii::$app->homeUrl; ?>../theme/img/gallery/events/<?= $gallery->eg_img; ?>">
                    <a data-confirm="Are you sure to delete this item?" href="<?= Yii::$app->homeUrl;?>eventgallery/del/<?=$gallery->eg_id;?>"><button class="btn btn-danger gallery-del"><i class="fa fa-trash" style="margin-right:5px"></i>Delete Image</button></a>
                </div>
            </div>
        <?php } ?>
    </div>
        <?php }else{ ?>
        <?= "No Images available";?>
        <?php } ?>

</div>
