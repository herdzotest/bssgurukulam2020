<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\ArticleGallery */

$this->title = 'Create Article Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['articles/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-gallery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
