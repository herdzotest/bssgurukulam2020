<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\ArticleGallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Article Galleries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-gallery-index">
    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Create Article Gallery', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ag_id',
            'ag_aid',
            'ag_img',
            'ag_status',
            'ag_createdOn',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
