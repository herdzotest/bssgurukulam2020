<?php
use admin\models\Articles;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model admin\models\ArticleGallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-gallery-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'] ]); ?>
    <?php if (isset($_GET['id'])) {
        $article_id = $_GET['id'];
        $articleModel = Articles::find()->where(['ar_id' => $article_id])->all();
        $listData = ArrayHelper::map($articleModel, 'ar_id', 'ar_title');
        echo $form->field($model, 'ag_aid')->dropDownList($listData);
    } else {
        $articleModel = Articles::find()->all();
        $listData = ArrayHelper::map($articleModel, 'ar_id', 'ar_title');
        echo $dropdown = $form->field($model, 'ag_aid')->dropDownList($listData, ['prompt' => 'Select Article']);
    }
    ?>
    <?php echo $form->field($model, 'ag_img[]')->widget(FileInput::class, [
        'options' => [
            'accept' => 'image/*',
            'multiple' => true
        ],
        'pluginOptions' => [
            'maxFileCount' => 10
        ]
    ]);
    ?>
    <?= $form->field($model, 'ag_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive'], ['prompt' => 'Select Image Status']) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
