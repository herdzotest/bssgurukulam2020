<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\ArticleGallery */

$ar_title = $articleModel->ar_title;
$this->title = $ar_title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['articles/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="article-gallery-view view-block">
    <h3><?= Html::encode($this->title) ?>
    <span class="pull-right">
       <a href="<?= Yii::$app->homeUrl;?>articlegallery/create/<?= $articleModel->ar_id;?>" class="btn btn-warning">Add New Article Image</a>
    </span>
    </h3>
    <?php if (!empty($model)) {?>
    <div class="row">
        <?php foreach ($model as $gallery) { ?>
            <div class="col-md-3">
                <div class="gallery-img">
                    <img src="<?= Yii::$app->homeUrl; ?>../theme/img/gallery/articles/<?= $gallery->ag_img; ?>">
                    <a data-confirm="Are you sure to delete this item?" href="<?= Yii::$app->homeUrl;?>articlegallery/del/<?=$gallery->ag_id;?>"><button class="btn btn-danger gallery-del"><i class="fa fa-trash" style="margin-right:5px"></i>Delete Image</button></a>
                </div>
            </div>
        <?php } ?>
    </div>
        <?php }else{ ?>
        <?= "No Images available";?>
        <?php } ?>
</div>
