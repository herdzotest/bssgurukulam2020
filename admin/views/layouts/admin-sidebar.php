      <!-- ============================================================== -->
      <!-- left sidebar -->
      <!-- ============================================================== -->
      <div class="nav-left-sidebar sidebar-dark">
          <div class="menu-list">
              <nav class="navbar navbar-expand-lg navbar-light">
                  <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarNav">
                      <ul class="navbar-nav flex-column">
                          <li class="nav-divider">
                              Admin Menus
                          </li>
                          <li class="nav-item ">
                              <a class="nav-link <?php if (Yii::$app->controller->id == 'site') {
                                                        echo 'active';
                                                    } ?>" href="<?= Yii::$app->homeUrl; ?>"><i class="fa fa-fw fa-user-circle"></i>Dashboard</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link  <?php if (Yii::$app->controller->id == 'slider' || Yii::$app->controller->id == 'hpcontents') {
                                                        echo 'active';
                                                    } ?>" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-2" aria-controls="submenu-2"><i class="far fa-images"></i>Manage Home Page</a>
                              <div id="submenu-2" class="collapse submenu  <?php if (Yii::$app->controller->id == 'slider' || Yii::$app->controller->id == 'hpcontents') {
                                                                                echo 'show';
                                                                            } ?>" style="">
                                  <ul class="nav flex-column">
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'slider') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>slider">Manage Home Slider</a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'hpcontents') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>hpcontents">Manage Home Page Contents</a>
                                      </li>
                                  </ul>
                              </div>
                          </li>
                          <li class="nav-item ">
                              <a class="nav-link <?php if (Yii::$app->controller->id == 'pagebanners') {
                                                        echo 'active';
                                                    } ?>" href="<?= Yii::$app->homeUrl; ?>pagebanners"><i class="fas fa-image"></i>Manage Page Banners</a>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link  <?php if (Yii::$app->controller->id == 'aboutpages') {
                                                        echo 'active';
                                                    } ?>" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-anchor"></i>Manage About Page</a>
                              <div id="submenu-3" class="collapse submenu  <?php if (Yii::$app->controller->id == 'aboutpages') {
                                                                                echo 'show';
                                                                            } ?>" style="">
                                  <ul class="nav flex-column">
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'aboutpages' && Yii::$app->controller->action->id == 'index') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>aboutpages">List About Pages</a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'aboutpages' && Yii::$app->controller->action->id == 'create') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>aboutpages/create">Add About Page</a>
                                      </li>
                                  </ul>
                              </div>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link  
                              <?php if (Yii::$app->controller->id == 'faculty' || Yii::$app->controller->id == 'resprodetails' ||  Yii::$app->controller->id == 'qualification' ||  Yii::$app->controller->id == 'experience') {
                                    echo 'active';
                                } ?>" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4" aria-controls="submenu-4">
                                  <i class="fas fa-users"></i>Manage Faculties</a>
                              <div id="submenu-4" class="collapse submenu
                              <?php if (Yii::$app->controller->id == 'faculty' || Yii::$app->controller->id == 'resprodetails' ||  Yii::$app->controller->id == 'qualification' ||  Yii::$app->controller->id == 'experience') {
                                    echo 'show';
                                } ?>" style="">
                                  <ul class="nav flex-column">
                                      <li class="nav-item">
                                          <a class="nav-link 
                                          <?php if (Yii::$app->controller->id == 'faculty' && Yii::$app->controller->action->id == 'view') {
                                                echo 'active';
                                            } ?>" href="<?= Yii::$app->homeUrl; ?>faculty">List Faculties</a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link 
                                          <?php if (Yii::$app->controller->id == 'faculty' || Yii::$app->controller->id == 'resprodetails' ||  Yii::$app->controller->id == 'qualification' ||  Yii::$app->controller->id == 'experience' && Yii::$app->controller->action->id == 'create') {
                                                echo 'active';
                                            } ?>" href="<?= Yii::$app->homeUrl; ?>faculty/create">Add New Faculty</a>
                                      </li>
                                  </ul>
                              </div>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link  
                              <?php if (Yii::$app->controller->id == 'facilities' || Yii::$app->controller->id == 'facilitygallery') {
                                    echo 'active';
                                } ?>" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-5" aria-controls="submenu-5">
                                  <i class="fas fa-cubes"></i>Manage Facilities</a>
                              <div id="submenu-5" class="collapse submenu
                              <?php if (Yii::$app->controller->id == 'facilities' || Yii::$app->controller->id == 'facilitygallery') {
                                    echo 'show';
                                } ?>" style="">
                                  <ul class="nav flex-column">
                                      <li class="nav-item">
                                          <a class="nav-link 
                                          <?php if (Yii::$app->controller->id == 'facilities' && Yii::$app->controller->action->id == 'index') {
                                                echo 'active';
                                            } ?>" href="<?= Yii::$app->homeUrl; ?>facilities">List Facilities</a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link 
                                          <?php if (Yii::$app->controller->id == 'facilities'  && Yii::$app->controller->action->id == 'create') {
                                                echo 'active';
                                            } ?>" href="<?= Yii::$app->homeUrl; ?>facilities/create">Add New Facilty</a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link 
                                          <?php if (Yii::$app->controller->id == 'facilitygallery') {
                                                echo 'active';
                                            } ?>" href="<?= Yii::$app->homeUrl; ?>facilitygallery">Manage Facility Gallery</a>
                                      </li>
                                  </ul>
                              </div>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link  <?php if (Yii::$app->controller->id == 'alumni') {
                                                        echo 'active';
                                                    } ?>" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-6" aria-controls="submenu-6"><i class="fas fa-graduation-cap"></i>Manage Alumnies</a>
                              <div id="submenu-6" class="collapse submenu  <?php if (Yii::$app->controller->id == 'alumni') {
                                                                                echo 'show';
                                                                            } ?>" style="">
                                  <ul class="nav flex-column">
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'alumni' && Yii::$app->controller->action->id == 'index') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>alumni">List Alumnies</a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'alumni' && Yii::$app->controller->action->id == 'create') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>alumni/create">Add Alumni</a>
                                      </li>
                                  </ul>
                              </div>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link  <?php if (Yii::$app->controller->id == 'charity' || Yii::$app->controller->id == 'events' || Yii::$app->controller->id == 'achievements' || Yii::$app->controller->id == 'articles' || Yii::$app->controller->id == 'charitygallery' || Yii::$app->controller->id == 'eventgallery' || Yii::$app->controller->id == 'achievementgallery' || Yii::$app->controller->id == 'articlegallery') {
                                                        echo 'active';
                                                    } ?>" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-7" aria-controls="submenu-7"><i class="fas fa-child"></i>Manage Life @ BSS</a>
                              <div id="submenu-7" class="collapse submenu  <?php if (Yii::$app->controller->id == 'charity' || Yii::$app->controller->id == 'events' || Yii::$app->controller->id == 'achievements' || Yii::$app->controller->id == 'articles' || Yii::$app->controller->id == 'charitygallery' || Yii::$app->controller->id == 'eventgallery' || Yii::$app->controller->id == 'achievementgallery' || Yii::$app->controller->id == 'articlegallery') {
                                                                                echo 'show';
                                                                            } ?>" style="">
                                  <ul class="nav flex-column">
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'charity' || Yii::$app->controller->id == 'charitygallery') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>charity">Manage Charity</a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'events' || Yii::$app->controller->id == 'eventgallery') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>events">Manage Events </a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'achievements' || Yii::$app->controller->id == 'achievementgallery') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>achievements">Manage Achievements </a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'articles' || Yii::$app->controller->id == 'articlegallery') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>articles">Manage Articles</a>
                                      </li>
                                  </ul>
                              </div>
                          </li>
                          <li class="nav-item">
                              <a class="nav-link  <?php if (Yii::$app->controller->id == 'visitorcomments') {
                                                        echo 'active';
                                                    } ?>" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-8" aria-controls="submenu-8"><i class="fas fa-comments"></i>Manage Visitor Comments</a>
                              <div id="submenu-8" class="collapse submenu  <?php if (Yii::$app->controller->id == 'visitorcomments') {
                                                                                echo 'show';
                                                                            } ?>" style="">
                                  <ul class="nav flex-column">
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'visitorcomments' && Yii::$app->controller->action->id == 'visitorcomments') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>visitorcomments">List Visitor Comments</a>
                                      </li>
                                      <li class="nav-item">
                                          <a class="nav-link <?php if (Yii::$app->controller->id == 'visitorcomments' && Yii::$app->controller->action->id == 'create') {
                                                                    echo 'active';
                                                                } ?>" href="<?= Yii::$app->homeUrl; ?>visitorcomments/create">Add Visitor Comments</a>
                                      </li>
                                  </ul>
                              </div>
                          </li>
                      </ul>
                  </div>
              </nav>
          </div>
      </div>
      <!-- ============================================================== -->
      <!-- end left sidebar -->
      <!-- ============================================================== -->