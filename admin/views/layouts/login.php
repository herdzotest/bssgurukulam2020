<?php

/* @var $this \yii\web\View */
/* @var $content string */

use admin\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= Yii::$app->homeUrl;?>theme/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="<?= Yii::$app->homeUrl;?>theme/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= Yii::$app->homeUrl;?>theme/assets/libs/css/style.css">
    <link rel="stylesheet" href="<?= Yii::$app->homeUrl;?>theme/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
    html,
    body {
        height: 100%;
    }

    body {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
    </style>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>


    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="<?= Yii::$app->homeUrl;?>theme/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="<?= Yii::$app->homeUrl;?>theme/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
