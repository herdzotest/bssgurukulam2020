<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\CharityimageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Charity Images';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charity-images-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Charity Images', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'art_image_id',
            'art_image_path',
            'arts_filename',
            'art_content',
            'arts_id',
            //'sort_order',
            //'uploaded_by',
            //'uploaded_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
