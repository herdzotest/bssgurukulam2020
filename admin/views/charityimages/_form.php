<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\CharityImages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charity-images-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'art_image_path[]')->fileInput(['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?> <?= Html::a('Back', ['/charity/'.$_GET['id']], ['class'=>'btn btn-warning ']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
