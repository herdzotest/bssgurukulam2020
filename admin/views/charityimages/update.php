<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\CharityImages */

$this->title = 'Update Charity Images: ' . $model->art_image_id;
$this->params['breadcrumbs'][] = ['label' => 'Charity Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->art_image_id, 'url' => ['view', 'id' => $model->art_image_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="charity-images-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
