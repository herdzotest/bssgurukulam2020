<?php

use admin\models\CharityImages;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model admin\models\CharityImages */

$this->title = 'Add Charity Images';
$this->params['breadcrumbs'][] = ['label' => 'Charity Images', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .img-div {
        padding: 15px;
        border: 1px solid;
        margin: 10px;
        text-align: center;
        border-radius: 15px;
    }

    .img-div img {
        width: 100%;
        padding: 5px;
    }
</style>
<div class="charity-images-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
<div class="img-grid">
    <?php
    $id = $_GET['id'];
    if (isset($id)) {
        $query = (new \yii\db\Query())
            ->select(['art_image_id','art_image_path'])
            ->from('art_images')
            ->where(['arts_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'art_image_path',
                    'format' => 'html',
                    'label' => 'IMAGES',
                    'value' => function ($data) {
                        return Html::img(
                            Yii::$app->homeUrl . $data['art_image_path'],
                            ['width' => '150px']
                        );
                    },

                ],
                [
                    'headerOptions' => ['width' => '250'],
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            return Html::a('', $url, [
                                'class' => 'btn btn-danger icon-trash',
                                'title' => Yii::t('app', 'Delete'), 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this image?'), 'data-method' => 'post'
                            ]);
                        },
                    ],
                    'urlCreator' => function ($action, $data, $key, $index) {
                            if ($action === 'delete') {
                                $url = Yii::$app->urlManager->createUrl(['charityimages/delete', 'id' => $data['art_image_id']]);
                                return $url;
                            }

                    }

                ],
            ]
        ]);
    } else {
        echo 'No Image Available';
    }
    ?>
</div>