<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\AboutPagesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-pages-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ap_id') ?>

    <?= $form->field($model, 'ap_title') ?>

    <?= $form->field($model, 'ap_slug') ?>

    <?= $form->field($model, 'ap_content') ?>

    <?= $form->field($model, 'ap_createdon') ?>

    <?php // echo $form->field($model, 'ap_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
