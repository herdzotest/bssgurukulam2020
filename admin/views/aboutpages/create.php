<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\AboutPages */

$this->title = 'Add New Pages';
$this->params['breadcrumbs'][] = ['label' => 'About Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-pages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
