<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\AboutPages */

$this->title = $model->ap_title;
$this->params['breadcrumbs'][] = ['label' => 'About Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="about-pages-view">

    <h1><?= Html::encode($this->title) ?>

        <span class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->ap_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->ap_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </span>
    </h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ap_slug',
            'ap_content:html',
            'ap_createdon:date',
            'ap_status',
        ],
    ]) ?>

</div>