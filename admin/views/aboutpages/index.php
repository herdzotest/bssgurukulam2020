<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\AboutPagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'About Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-pages-index">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a('Add About Pages', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'ap_title',
            'ap_slug',
            'ap_createdon:date',
            'ap_status',

            [
                'headerOptions' => ['width' => '175'],
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view} {update} {status}',
                'buttons' => [
                    'status' => function ($url, $model) {
                        return $model->ap_status == 'Inactive' ?
                            Html::a('', $url, [
                                'class' => 'btn btn-success fas fa-check',
                                'title' => Yii::t('app', ' Active Slider')
                            ]) : Html::a('', $url, [
                                'class' => 'btn btn-warning fas fa-window-close',
                                'title' => Yii::t('app', 'Inactive Slider')
                            ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-primary fas fa-pencil-alt',
                            'title' => Yii::t('app', 'Update Pages')
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-info fas fa-eye',
                            'title' => Yii::t('app', 'View Pages')
                        ]);
                    },
                ],

            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>