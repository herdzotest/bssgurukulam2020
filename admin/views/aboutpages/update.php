<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\AboutPages */

$this->title = $model->ap_title;
$this->params['breadcrumbs'][] = ['label' => 'About Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = ['label' => $model->ap_title, 'url' => ['view', 'id' => $model->ap_id]];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="about-pages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
