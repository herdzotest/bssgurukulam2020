<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\AboutPages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ap_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ap_slug')->textInput(['readonly' => true]) ?>

    <?= $form->field($model, 'ap_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ap_status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => 'Select Page Status']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $("#aboutpages-ap_title").focusout(function() {
        var title = $("#aboutpages-ap_title").val().toLowerCase();
        var title = title.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        var res = title.replace(/ /g, "-");
        var res = res.replace("--", "-", res);
        $("#aboutpages-ap_slug").val(res);
    });
</script>
<script type="text/javascript" src="<?= Yii::$app->homeUrl ?>theme/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('aboutpages-ap_content', {
        extraPlugins: 'bt_table',
        // extraPlugins : 'btgrid',
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?type=images",
        filebrowserBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=files",
        filebrowserImageBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=images",
        filebrowserFlashBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=flash",
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=files",
        filebrowserImageUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=images",
        filebrowserFlashUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=flash",
    });

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].on("instanceReady", function() {
            //set keyup event
            this.document.on("keyup", function() {
                CKEDITOR.instances[instance].updateElement();
            });
            //and paste event
            this.document.on("paste", function() {
                CKEDITOR.instances[instance].updateElement();
            });

        });
    }
</script>