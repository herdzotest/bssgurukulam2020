<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\AchievementGallery */

$this->title = 'Create Achievement Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Achievements', 'url' => ['achievements/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="achievement-gallery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
