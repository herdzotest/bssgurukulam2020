<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\AchievementGallery */

$this->title = 'Update Achievement Gallery: ' . $model->ag_id;
$this->params['breadcrumbs'][] = ['label' => 'Achievement Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ag_id, 'url' => ['view', 'id' => $model->ag_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="achievement-gallery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
