<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\AchievementGallerySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="achievement-gallery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ag_id') ?>

    <?= $form->field($model, 'ag_aid') ?>

    <?= $form->field($model, 'ag_img') ?>

    <?= $form->field($model, 'ag_status') ?>

    <?= $form->field($model, 'ag_createdOn') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
