<?php
use admin\models\Achievements;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model admin\models\AchievementGallery */
/* @var $form yii\widgets\ActiveForm */
?>
 
<div class="achievement-gallery-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'] ]); ?>

    <?php if (isset($_GET['id'])) {
        $achievement_id = $_GET['id'];
        $achievementModel = Achievements::find()->where(['av_id' => $achievement_id])->all();
        $listData = ArrayHelper::map($achievementModel, 'av_id', 'av_title');
        echo $form->field($model, 'ag_aid')->dropDownList($listData);
    } else {
        $achievementModel = Achievements::find()->all();
        $listData = ArrayHelper::map($achievementModel, 'av_id', 'av_title');
        echo $dropdown = $form->field($model, 'ag_aid')->dropDownList($listData, ['prompt' => 'Select Achievement']);
    }
    ?>

    <?php echo $form->field($model, 'ag_img[]')->widget(FileInput::class, [
        'options' => [
            'accept' => 'image/*',
            'multiple' => true
        ],
        'pluginOptions' => [
            'maxFileCount' => 10
        ]
    ]);
    ?>

    <?= $form->field($model, 'ag_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive'], ['prompt' => 'Select Image Status']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
