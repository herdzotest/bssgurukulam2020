<?php
 
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\AchievementGallery */

$av_title = $achievementModel->av_title;
$this->title = $av_title;
$this->params['breadcrumbs'][] = ['label' => 'Achievements', 'url' => ['achievements/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="achievement-gallery-view view-block">

    <h3><?= Html::encode($this->title) ?>

    <span class="pull-right">
       <a href="<?= Yii::$app->homeUrl;?>achievementgallery/create/<?= $achievementModel->av_id;?>" class="btn btn-warning">Add New Achievement Image</a>
    </span>
    </h3>
    <?php if (!empty($model)) {?>
    <div class="row">
        <?php foreach ($model as $gallery) { ?>
            <div class="col-md-3">
                <div class="gallery-img">
                    <img src="<?= Yii::$app->homeUrl; ?>../theme/img/gallery/achievements/<?= $gallery->ag_img; ?>">
                    <a data-confirm="Are you sure to delete this item?" href="<?= Yii::$app->homeUrl;?>achievementgallery/del/<?=$gallery->ag_id;?>"><button class="btn btn-danger gallery-del"><i class="fa fa-trash" style="margin-right:5px"></i>Delete Image</button></a>
                </div>
            </div>
        <?php } ?>
    </div>
        <?php }else{ ?>
        <?= "No Images available";?>
        <?php } ?>
</div>
