<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Articles */

$this->title = 'Update Articles: ' . $model->ar_title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = ['label' => $model->ar_title, 'url' => ['view', 'id' => $model->ar_id]];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="articles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
