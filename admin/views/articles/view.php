<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
 
/* @var $this yii\web\View */
/* @var $model admin\models\Articles */

$this->title = $model->ar_title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="articles-view">
    <h1><?= Html::encode($this->title) ?>
        <span class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->ar_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->ar_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </span>
    </h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ar_title',
            'ar_slug',
                [
                    'attribute' => 'ar_featured_image',
                    'value' => Yii::$app->homeUrl . '../theme/img/articles/featuredImage/' . $model->ar_featured_image,
                    'format' => ['image', ['width' => '200', 'height' => '200']],
                ],
            'ar_content:html',
            'ar_creator_class',
            'ar_created_by',
            'ar_created_on:date',
            'ar_published_on:date',
            'ar_status',
        ],
    ]) ?>

</div>
