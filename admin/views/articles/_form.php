<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model admin\models\Articles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ar_title')->textInput(['maxlength' => true]) ?>
    <div class="slug">
        <?= $form->field($model, 'ar_slug')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
    </div>
    <?= $form->field($model, 'ar_featured_image')->widget(FileInput::class, [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
            'showUpload' => true,
            'initialPreview' => [
                $model->ar_featured_image ? Html::img(Yii::$app->homeUrl . '../theme/img/articles/featuredImage/' . $model->ar_featured_image) : null, // checks the models to display the preview
            ],
            'overwriteInitial' => false,
        ],
    ]); ?>

    <?= $form->field($model, 'ar_content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ar_created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ar_creator_class')->dropDownList(['I' => 'I', 'II' => 'II', 'III' => 'III', 'IV' => 'IV', 'V' => 'V', 'VI' => 'VI', 'VII' => 'VII', 'VIII' => 'VIII', 'IX' => 'IX', 'X' => 'X', 'XI' => 'XI', 'XII' => 'XII',], ['prompt' => '-- Select Class--']) ?>

    <?= $form->field($model, 'ar_created_on')->widget(\yii\jui\DatePicker::class, [
        'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control', 'placeholder' => '-- Select Date --'],
    ]) ?>

    <?= $form->field($model, 'ar_published_on')->widget(\yii\jui\DatePicker::class, [
        'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control', 'placeholder' => '-- Select Date --'],
    ]) ?>

    <?= $form->field($model, 'ar_status')->dropDownList(['Published' => 'Publish', 'Draft' => 'Save as Draft'], ['prompt' => '-- Select Article Status--']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(document).ready(function() {
        $(".slug").hide();
    });
    $("#articles-ar_title").focusout(function() {
        var title = $("#articles-ar_title").val().toLowerCase();
        var title = title.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        var res = title.replace(/ /g, "-");
        var res = res.replace("--", "-", res);
        $("#articles-ar_slug").val(res);
        $(".slug").show();

    });
</script>
<script type="text/javascript" src="<?= Yii::$app->homeUrl ?>theme/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('articles-ar_content', {
        extraPlugins: 'bt_table',
        // extraPlugins : 'btgrid',
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?type=images",
        filebrowserBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=files",
        filebrowserImageBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=images",
        filebrowserFlashBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=flash",
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=files",
        filebrowserImageUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=images",
        filebrowserFlashUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=flash",
    });

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].on("instanceReady", function() {
            //set keyup event
            this.document.on("keyup", function() {
                CKEDITOR.instances[instance].updateElement();
            });
            //and paste event
            this.document.on("paste", function() {
                CKEDITOR.instances[instance].updateElement();
            });

        });
    }
</script>