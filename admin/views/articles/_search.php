<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\ArticlesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ar_id') ?>

    <?= $form->field($model, 'ar_title') ?>

    <?= $form->field($model, 'ar_featured_image') ?>

    <?= $form->field($model, 'ar_content') ?>

    <?= $form->field($model, 'ar_created_by') ?>

    <?php // echo $form->field($model, 'ar_creator_class') ?>

    <?php // echo $form->field($model, 'ar_created_on') ?>

    <?php // echo $form->field($model, 'ar_published_on') ?>

    <?php // echo $form->field($model, 'ar_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
