<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model admin\models\Charity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ch_title')->textInput(['maxlength' => true]) ?>

    <div class="slug">
        <?= $form->field($model, 'ch_slug')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
    </div>

    <?= $form->field($model, 'ch_featuredImage')->widget(FileInput::class, [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'bmp'],
            'showUpload' => true,
            'initialPreview' => [
                $model->ch_featuredImage ? Html::img(Yii::$app->homeUrl.'../theme/img/charity/featuredImages/'.$model->ch_featuredImage) : null, // checks the models to display the preview
            ],
            'overwriteInitial' => false,
        ],
    ]); ?>

    <?= $form->field($model, 'ch_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ch_date')->widget(\yii\jui\DatePicker::class, [
        //'language' => 'ru',
        'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control', 'placeholder' => '-- Select Date --'],
    ]) ?>

    <?= $form->field($model, 'ch_place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ch_status')->dropDownList(['Published' => 'Publish', 'Draft' => 'Save as Draft'], ['prompt' => '-- Select Charity Status--']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $(document).ready(function() {
        $(".slug").hide();
    });
    $("#charity-ch_title").focusout(function() {
        var title = $("#charity-ch_title").val().toLowerCase();
        var title = title.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        var res = title.replace(/ /g, "-");
        var res = res.replace("--", "-", res);
        $("#charity-ch_slug").val(res);
        $(".slug").show();

    });
</script>
<script type="text/javascript" src="<?= Yii::$app->homeUrl ?>theme/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('charity-ch_desc', {
        extraPlugins: 'bt_table',
        // extraPlugins : 'btgrid',
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?type=images",
        filebrowserBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=files",
        filebrowserImageBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=images",
        filebrowserFlashBrowseUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/browse.php?opener=ckeditor&type=flash",
        filebrowserUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=files",
        filebrowserImageUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=images",
        filebrowserFlashUploadUrl: "<?= Yii::$app->homeUrl ?>theme/kcfinder/upload.php?opener=ckeditor&type=flash",
    });

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].on("instanceReady", function() {
            //set keyup event
            this.document.on("keyup", function() {
                CKEDITOR.instances[instance].updateElement();
            });
            //and paste event
            this.document.on("paste", function() {
                CKEDITOR.instances[instance].updateElement();
            });

        });
    }
</script>