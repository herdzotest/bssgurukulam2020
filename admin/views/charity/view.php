<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\Charity */

$this->title = $model->ch_title;
$this->params['breadcrumbs'][] = ['label' => 'Charities', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="charity-view">

    <h1><?= Html::encode($this->title) ?>
        <span class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->ch_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->ch_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </span>
    </h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ch_title',
            'ch_slug',
                [
                    'attribute' => 'ch_featuredImage',
                    'value' => Yii::$app->homeUrl . '../theme/img/charity/featuredImages/' . $model->ch_featuredImage,
                    'format' => ['image', ['width' => '200', 'height' => '200']],
                ],
            'ch_desc:html',
            'ch_date:date',
            'ch_place',
            'ch_status',
        ],
    ]) ?>

</div>