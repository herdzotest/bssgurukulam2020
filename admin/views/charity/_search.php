<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\CharitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charity-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ch_id') ?>

    <?= $form->field($model, 'ch_title') ?>

    <?= $form->field($model, 'ch_slug') ?>

    <?= $form->field($model, 'ch_featuredImage') ?>

    <?= $form->field($model, 'ch_desc') ?>

    <?php // echo $form->field($model, 'ch_date') ?>

    <?php // echo $form->field($model, 'ch_place') ?>

    <?php // echo $form->field($model, 'ch_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
