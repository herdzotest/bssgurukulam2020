<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Charity */

$this->title = 'Add New Charity';
$this->params['breadcrumbs'][] = ['label' => 'Charities', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charity-create view-block">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
