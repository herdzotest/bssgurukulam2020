<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Charity */

$this->title = 'Update Charity: ' . $model->ch_title;
$this->params['breadcrumbs'][] = ['label' => 'Charities', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = ['label' => $model->ch_title, 'url' => ['view', 'id' => $model->ch_id]];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="charity-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
