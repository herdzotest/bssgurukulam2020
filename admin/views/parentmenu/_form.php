<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\ParentMenu */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="parent-menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pm_name')->textInput(['maxlength' => true,'class' => 'col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 form-control']) ?>

    <?= $form->field($model, 'pm_slug')->textInput(['readonly' => true,'class' => 'col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 form-control']) ?>

    <?= $form->field($model, 'pm_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive',], ['prompt' => 'Select Menu Status','class' => 'col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 form-control']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $("#parentmenu-pm_name").focusout(function() {
        var title = $("#parentmenu-pm_name").val().toLowerCase();
        var title = title.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
        var res = title.replace(/ /g, "-");
        var res = res.replace("--", "-", res);
        $("#parentmenu-pm_slug").val(res);
        $(".field-casestudy-cs_slug").show();
    });
</script>