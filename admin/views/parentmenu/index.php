<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\ParentMenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Parent Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-menu-index">

    <h1><?= Html::encode($this->title) ?>

        <?= Html::a('Add Parent Menu', ['create'], ['class' => 'btn btn-success pull-right']) ?>

    </h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pm_id',
            'pm_name',
            'pm_slug',
            'pm_createdon',
            'pm_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>