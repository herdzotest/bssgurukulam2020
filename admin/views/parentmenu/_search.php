<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\ParentMenuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parent-menu-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'pm_id') ?>

    <?= $form->field($model, 'pm_name') ?>

    <?= $form->field($model, 'pm_slug') ?>

    <?= $form->field($model, 'pm_createdon') ?>

    <?= $form->field($model, 'pm_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
