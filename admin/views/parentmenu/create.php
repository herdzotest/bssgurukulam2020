<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\ParentMenu */

$this->title = 'Add Menu';
$this->params['breadcrumbs'][] = ['label' => 'Parent Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-menu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
