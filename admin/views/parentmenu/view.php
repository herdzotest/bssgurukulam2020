<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\ParentMenu */

$this->title = $model->pm_name;
$this->params['breadcrumbs'][] = ['label' => 'Parent Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="parent-menu-view">
    <h1><?= Html::encode($this->title) ?>

  <span class="pull-right">

        <?= Html::a('Update', ['update', 'id' => $model->pm_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pm_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
</span>
    </h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'pm_name',
            'pm_slug',
            'pm_createdon:date',
            'pm_status',
        ],
    ]) ?>

</div>
