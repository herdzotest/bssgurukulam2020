<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\ParentMenu */

$this->title = 'Edit Menu: ' . $model->pm_name;
$this->params['breadcrumbs'][] = ['label' => 'Parent Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = ['label' => $model->pm_name, 'url' => ['view', 'id' => $model->pm_id]];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="parent-menu-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
