<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\PageBanners */

$this->title = $model->pb_page_name;
$this->params['breadcrumbs'][] = ['label' => 'Page Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="page-banners-view">

    <h1><?= Html::encode($this->title) ?>

        <span class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->pb_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->pb_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>

    </h1>
    </span>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'pb_image',
                'value' => Yii::$app->homeUrl . '../theme/img/page_banners/' . $model->pb_image,
                'format' => ['image', ['width' => '200', 'height' => '100']],
            ],
            'pb_page_name',
            'pb_status',
            'pb_createdon:date',
        ],
    ]) ?>

</div>