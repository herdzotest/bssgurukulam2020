<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\PageBanners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-banners-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pb_image')->fileInput() ?>
    
    <div class="form-group">
        <?= $form->field($model, 'pb_page_name')->dropDownList(['About' => 'About', 'Faculty' => 'Faculty', 'Facilities' => 'Facilities', 'Admission' => 'Admission', 'Contact' => 'Contact', 'Life@BSS' => 'Life@BSS', 'Alumni' => 'Alumni',], ['prompt' => 'Please select the page', 'class' => 'col-md-6 form-control']) ?>

        <?= $form->field($model, 'pb_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive',], ['prompt' => 'Select Status', 'class' => 'col-md-6 form-control']) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>