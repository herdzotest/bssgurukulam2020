<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\PageBanners */

$this->title = 'Add Page Banners';
$this->params['breadcrumbs'][] = ['label' => 'Page Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-banners-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
