<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\PageBanners */

$this->title = 'Update Banners | ' . $model->pb_page_name;
$this->params['breadcrumbs'][] = ['label' => 'Page Banners', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="page-banners-update view-block">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
