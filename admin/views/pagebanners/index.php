<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\PageBannersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Page Banners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-banners-index ">

<h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pb_page_name',
            [
                'attribute' => 'pb_image',
                'format' => 'html',
                'label' => 'Banner Images',
                'value' => function ($data) {
                    return Html::img(
                        Yii::$app->homeUrl . '../theme/img/page_banners/' . $data['pb_image'],
                        ['width' => '200', 'height' => '100']
                    );
                },
            ],
            'pb_status',
            'pb_createdon:date',

            [
                'headerOptions' => ['width' => '175'],
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-primary fas fa-pencil-alt',
                            'title' => Yii::t('app', 'Update Page Banner')
                        ]);
                    },
                ],

            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>