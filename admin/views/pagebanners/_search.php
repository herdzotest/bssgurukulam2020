<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\PageBannersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-banners-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'pb_id') ?>

    <?= $form->field($model, 'pb_image') ?>

    <?= $form->field($model, 'pb_page_name') ?>

    <?= $form->field($model, 'pb_status') ?>

    <?= $form->field($model, 'pb_createdon') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
