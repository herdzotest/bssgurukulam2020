<?php

use admin\models\Departments;
use admin\models\Designation;
use admin\models\Subjects;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model admin\models\ResproDetails */
/* @var $form yii\widgets\ActiveForm */

$departments = Departments::find()->all();
$designation = Designation::find()->all();
$subjects = Subjects::find()->all();
$listData = ArrayHelper::map($departments, 'department_id', 'department_name');
$desData = ArrayHelper::map($designation, 'designation_id', 'designation_name');
$subData = ArrayHelper::map($subjects, 'sub_id', 'sub_name');
?>

<div class="respro-details-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'pd_res_type')->dropDownList(['Teaching' => 'Teaching', 'Non-Teaching' => 'Non-Teaching',], ['prompt' => '---Select Resource Type---']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'pd_dp_id')->dropDownList(
                $listData,
                ['prompt' => '---Select Department---']
            );
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'pd_des_id')->dropDownList(
                $desData,
                ['prompt' => '---Select Designation---']
            );
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'pd_res_sub')->dropDownList(
                $subData,
                ['prompt' => '---Select Subject---']
            );
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save & Continue', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>