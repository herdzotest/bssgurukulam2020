<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\ResproDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Respro Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="respro-details-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Respro Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'pd_id',
            'pd_res_id',
            'pd_dp_id',
            'pd_des_id',
            'pd_res_type',
            //'pd_res_sub',
            //'pd_status',
            //'pd_updated_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
