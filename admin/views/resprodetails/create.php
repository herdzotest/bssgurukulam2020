<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\ResproDetails */
$this->title = 'Faculty | Professional Details';
$this->params['breadcrumbs'][] = ['label' => 'Faculty', 'url' => ['faculty/index']];
$this->params['breadcrumbs'][] = ' > ';
$this->params['breadcrumbs'][] = $model->getUser($_GET['id'])->first_name;
?>
<div class="respro-details-create view-block">

    <h2>Professional Profile Details [<?= $model->getUser($_GET['id'])->first_name;?>] </h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
