<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\ResproDetails */

$this->title = 'Update Professional Details: ' . $model->getUser($model->pd_res_id)->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Faculty', 'url' => ['faculty/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = ['label' => $model->getUser($model->pd_res_id)->first_name,  'url' => ['faculty/view', 'id' => $model->pd_res_id]];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="respro-details-update view-block">

    <h3><?= 'Professional Details : '.$model->getUser($model->pd_res_id)->first_name; ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
