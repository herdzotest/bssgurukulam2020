<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\ResproDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="respro-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'pd_id') ?>

    <?= $form->field($model, 'pd_res_id') ?>

    <?= $form->field($model, 'pd_dp_id') ?>

    <?= $form->field($model, 'pd_des_id') ?>

    <?= $form->field($model, 'pd_res_type') ?>

    <?php // echo $form->field($model, 'pd_res_sub') ?>

    <?php // echo $form->field($model, 'pd_status') ?>

    <?php // echo $form->field($model, 'pd_updated_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
