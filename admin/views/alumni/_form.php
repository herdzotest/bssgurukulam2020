<?php

use admin\models\Countries;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model admin\models\Alumni */
/* @var $form yii\widgets\ActiveForm */

$Country = Countries::find()->all();
$listData = ArrayHelper::map($Country, 'id', 'name');
?>

<div class="alumni-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?php echo $form->field($profilePic, 'pp_image')->widget(FileInput::class, [
                'options' => ['accept' => 'image/*'],
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'second_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'gender')->dropDownList(['Male' => 'Male', 'Female' => 'Female', 'Transgender' => 'Transgender',], ['prompt' => '-- Select Gender --']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-12">
            <h5>Personal Profile Details</h5>
        </div>
        <div class="col-md-12">
            <?= $form->field($alumniProfile, 'ap_address')->textarea(['rows' => 1]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($alumniProfile, 'ap_country')->dropDownList($listData, ['id' => 'country-id', 'prompt' => '--Choose Country--']); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($alumniProfile, 'ap_state')->widget(DepDrop::class, [
                'options' => ['id' => 'state-id'],
                'pluginOptions' => [
                    'depends' => ['country-id'],
                    'placeholder' => '--Choose State--',
                    'url' => Url::to(['/alumni/state'])
                ]
            ]); ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($alumniProfile, 'ap_city')->widget(DepDrop::class, [
                'pluginOptions' => [
                    'depends' => ['state-id'],
                    'placeholder' => '--Choose City--',
                    'url' => Url::to(['/alumni/city'])
                ]
            ]); ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($alumniProfile, 'ap_pincode')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($alumniProfile, 'ap_batch_from')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control'],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($alumniProfile, 'ap_batch_to')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control'],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($alumniProfile, 'ap_qualification')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($alumniProfile, 'ap_linkedin_link')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($alumniProfile, 'ap_facebook_link')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($alumniProfile, 'ap_twitter_link')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-12">
            <h5>Company Profile Details</h5>
        </div>
        <div class="col-md-6">
            <?= $form->field($alumniCompany, 'ac_postion')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($alumniCompany, 'ac_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($alumniCompany, 'ac_joined_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control'],
            ]) ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($alumniCompany, 'ac_relieved_date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control'],
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($alumniCompany, 'ac_location')->textInput() ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($alumniCompany, 'ac_status')->dropDownList(['Active' => 'Yes', 'Inactive' => 'No'], ['prompt' => '--Select Status--']) ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>