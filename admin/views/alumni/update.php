<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Alumni */

$this->title = 'Update: ' . $model->first_name.' '.$model->second_name;
$this->params['breadcrumbs'][] = ['label' => 'Alumnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = ['label' => $model->first_name.' '.$model->second_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alumni-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'alumniProfile' => $alumniProfile,
        'profilePic' => $profilePic,
        'alumniCompany' => $alumniCompany,
    ]) ?>

</div>
