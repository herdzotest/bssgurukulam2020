<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Alumni */

$this->title = 'Add New Alumni';
$this->params['breadcrumbs'][] = ['label' => 'Alumnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumni-create view-block">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'alumniProfile' => $alumniProfile,
        'profilePic' => $profilePic,
        'alumniCompany' => $alumniCompany,
    ]) ?>

</div>