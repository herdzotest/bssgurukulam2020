<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\AlumniSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alumnis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumni-index">

    <h1><?= Html::encode($this->title) ?>

        <?= Html::a('Add Alumni', ['create'], ['class' => 'btn btn-success pull-right']) ?>

    </h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'first_name',
                'format' => 'html',
                'label' => 'Name',
                'value' => function ($data) {
                    return $data->first_name . ' ' . $data->second_name;
                },
            ],
            'username',
            //'role',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            // 'status',
            'created_at:date',
            //'updated_at',
            //'verification_token',

            [
                'headerOptions' => ['width' => '250'],
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view} {update} {delete} {status}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-danger fa fa-trash',
                            'title' => Yii::t('app', 'Delete'), 'data-confirm' => Yii::t('app', 'Are you sure you want to delete this image?'), 'data-method' => 'post'
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-primary fas fa-pencil-alt',
                            'title' => Yii::t('app', 'Update Slider')
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('', $url, [
                            'class' => 'btn btn-info fas fa-eye',
                            'title' => Yii::t('app', 'View Slider')
                        ]);
                    },
                    'status' => function ($url, $model) {
                        return $model->profile['ap_approved_uid'] == 0 ?
                        Html::a('', $url, [
                            'class' => 'btn btn-success fas fa-check',
                            'title' => Yii::t('app', ' Approve Alumni')
                        ]) : Html::a('', $url, [
                            'class' => 'btn btn-warning fas fa-window-close',
                            'title' => Yii::t('app', 'Reject Alumni')
                        ]);
                    },
                    
                ],

            ],
        ],
    ]);
    ?>

    <?php Pjax::end(); ?>

</div>