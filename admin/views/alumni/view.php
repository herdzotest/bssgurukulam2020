<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model admin\models\Alumni */

$this->title = $model->first_name . ' ' . $model->second_name;
$this->params['breadcrumbs'][] = ['label' => 'Alumnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alumni-view">

    <h1><?= Html::encode($this->title) ?>

        <span class="pull-right">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </span>
    </h1>
    <div class="row">
        <div class="col-lg-12 view-block">
            <h4>Alumni Profile Details</h4>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    [
                        'attribute' => 'Status',
                        'label' => 'Profile Image',
                        'value' => Yii::$app->homeUrl . '../theme/img/pro_images/alumni/' . $model->getImage($model->id),
                        'format' => ['image', ['width' => '100', 'height' => '100']],
                    ],
                    'first_name',
                    'second_name',
                    'gender',
                    'username',
                    'email:email',
                    [
                        'attribute' => 'status',
                        'value' => function ($model) {
                            return $model->status = '9' ? 'Inactive' : 'Active';
                        },
                    ],
                    'created_at:date',
                    'updated_at:date',
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 view-block">
            <h4>Alumni Communication Details</h4>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'profile.ap_batch_from:date',
                    'profile.ap_batch_to:date',
                    'profile.ap_address',
                    [
                        'attribute' => 'Country',
                        'value' => $model->getCountry($model->profile->ap_country),
                    ],
                    [
                        'attribute' => 'State',
                        'value' => $model->getState($model->profile->ap_state),
                    ],
                    [
                        'attribute' => 'City',
                        'value' => $model->getCity($model->profile->ap_city),
                    ],
                    'profile.ap_pincode',
                    'profile.ap_qualification',
                    [
                        'attribute' => 'ap_linkedin_link',
                        'label' => 'LinkedIn Link',
                        'value' => function ($model) {
                            return $model->profile->ap_linkedin_link == '' ? 'Not Available' : $model->profile->ap_linkedin_link;
                        },
                    ],
                    [
                        'attribute' => 'ap_facebook_link',
                        'label' => 'Facebook Link',
                        'value' => function ($model) {
                            return $model->profile->ap_facebook_link == '' ? 'Not Available' : $model->profile->ap_facebook_link;
                        },
                    ],
                    [
                        'attribute' => 'ap_twitter_link',
                        'label' => 'Twitter Link',
                        'value' => function ($model) {
                            return $model->profile->ap_twitter_link == '' ? 'Not Available' : $model->profile->ap_twitter_link;
                        },
                    ],
                ],
            ]) ?>
        </div>
        <div class="col-md-12 view-block">
            <h4>Alumni Company Details</h4>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'company.ac_postion',
                    'company.ac_name',
                    'company.ac_joined_date:date',
                    'company.ac_relieved_date:date',
                    'company.ac_location',
                    [
                        'attribute' => 'company.ac_status',
                        'label' => 'Current Company',
                        'value' => function ($model) {
                            return $model->company->ac_status = 'Active' ? 'Yes' : 'No';
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>