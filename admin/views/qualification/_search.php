<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\ResoureQualificationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="resoure-qualification-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'rq_id') ?>

    <?= $form->field($model, 'rq_rid') ?>

    <?= $form->field($model, 'rq_course_name') ?>

    <?= $form->field($model, 'rq_university') ?>

    <?= $form->field($model, 'rq_yop') ?>

    <?php // echo $form->field($model, 'rq_mark') ?>

    <?php // echo $form->field($model, 'rq_status') ?>

    <?php // echo $form->field($model, 'rq_updated_on') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
