<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\ResoureQualification */

$this->title = 'Qualification: ' . $model->getUser($model->rq_rid)->first_name;
$this->params['breadcrumbs'][] = ['label' => 'Faculty', 'url' => ['faculty/index']];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = ['label' => $model->getUser($model->rq_rid)->first_name, 'url' => ['faculty/view', 'id' => $model->rq_rid]];
$this->params['breadcrumbs'][] = '>';
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="resoure-qualification-update view-block">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
