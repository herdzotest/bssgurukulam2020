<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\ResoureQualification */
/* @var $form yii\widgets\ActiveForm */

$years = range(date('Y'), 1900);

?>

<div class="resoure-qualification-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'rq_course_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'rq_university')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'rq_yop')->dropDownList(array_combine($years, $years)); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'rq_mark')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
        <?= $form->field($model, 'rq_status')->dropDownList(['Active' => 'Active', 'Inactive' => 'Inactive',], ['prompt' => '--Select Status--']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save & Continue', ['class' => 'btn btn-success']) ?>
        <?= Html::a('Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>