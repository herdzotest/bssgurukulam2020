<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel admin\models\ResoureQualificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resoure Qualifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="resoure-qualification-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Resoure Qualification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'rq_id',
            'rq_rid',
            'rq_course_name',
            'rq_university',
            'rq_yop',
            //'rq_mark',
            //'rq_status',
            //'rq_updated_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
