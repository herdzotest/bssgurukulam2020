<?php

namespace admin\controllers;
 
use Yii;
use admin\models\Articles;
use admin\models\ArticlesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * ArticlesController implements the CRUD actions for Articles model.
 */
class ArticlesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Articles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticlesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Articles model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Articles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Articles();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 'ar_featured_image');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->ar_title);
                $model->ar_featured_image = $title . '_' . $uploadedFile;
            }
            $model->ar_created_on = date("Y-m-d", strtotime($model->ar_created_on));
            $model->ar_published_on = date("Y-m-d", strtotime($model->ar_published_on));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/articles/featuredImage/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                }
                \Yii::$app->getSession()->setFlash('success', 'Article Added successfully');
                return $this->redirect(['view', 'id' => $model->ar_id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing Articles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    { 
        $model = $this->findModel($id);
        $oldimage = $model->ar_featured_image;
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 'ar_featured_image');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->ar_title);
                $model->ar_featured_image = $title . '_' . $uploadedFile;
            } else {
                $model->ar_featured_image = $oldimage;
            }
            $model->ar_created_on = date("Y-m-d", strtotime($model->ar_created_on));
            $model->ar_published_on = date("Y-m-d", strtotime($model->ar_published_on));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/articles/featuredImage/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                    if ($oldimage != '') {
                        unlink('../theme/img/articles/featuredImage/' . $oldimage);
                    }
                }
                \Yii::$app->getSession()->setFlash('success', 'Article Page Updated successfully');
                return $this->redirect(['view', 'id' => $model->ar_id]);
            } else {
                return $this->redirect(['update', 'id' => $model->ar_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }

    }


    /**
     * Deletes an existing Articles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Articles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Articles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Articles::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
