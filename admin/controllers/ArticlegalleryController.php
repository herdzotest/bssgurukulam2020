<?php
 
namespace admin\controllers;

use admin\models\Articles;
use Yii;
use admin\models\ArticleGallery;
use admin\models\ArticleGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * ArticlegalleryController implements the CRUD actions for ArticleGallery model.
 */
class ArticlegalleryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ArticleGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ArticleGallery model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionImages($id)
    {
        $galleryModel = ArticleGallery::find()->where(['ag_aid' => $id,'ag_status' => 'Active'])->all();
        $articleModel = Articles::find()->where(['ar_id' => $id])->one();
        return $this->render('view', [
            'model' => $galleryModel,
            'articleModel' => $articleModel,
        ]);
    }

    /**
     * Creates a new ArticleGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ArticleGallery();

        if ($model->load(Yii::$app->request->post())) {
            $imageFiles = UploadedFile::getInstances($model, 'ag_img');
            foreach ($imageFiles as $gallery) {
                $galleryModel = new ArticleGallery();
                $galleryModel->load(Yii::$app->request->post());
                $galleryImageName = $galleryModel->ag_aid . '_' . $gallery->baseName . '.' . $gallery->extension;
                $galleryModel->ag_img = $galleryImageName;
                if ($galleryModel->save(false)) {
                    $gallery->saveAs('../theme/img/gallery/articles/'  . $galleryImageName);
                }
            }
            \Yii::$app->getSession()->setFlash('success', 'Slider Created successfully');
            return $this->redirect(['images', 'id' => $model->ag_aid]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ArticleGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ag_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ArticleGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDel($id)
    {
        $model = ArticleGallery::find()->where(['ag_id' => $id])->one();
        $model->ag_status = 'Deleted';
        $model->save(false);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the ArticleGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticleGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticleGallery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
