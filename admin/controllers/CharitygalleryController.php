<?php

namespace admin\controllers;

use admin\models\Charity;
use Yii;
use admin\models\CharityGallery;
use admin\models\CharityGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * CharitygalleryController implements the CRUD actions for CharityGallery model.
 */
class CharitygalleryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CharityGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CharityGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CharityGallery model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionImages($id)
    {
        $galleryModel = CharityGallery::find()->where(['cg_cid' => $id,'cg_status' => 'Active'])->all();
        $charityModel = Charity::find()->where(['ch_id' => $id])->one();
        return $this->render('view', [
            'model' => $galleryModel,
            'charityModel' => $charityModel,
        ]);
    }

    /**
     * Creates a new CharityGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new CharityGallery();

        if ($model->load(Yii::$app->request->post())) {
            $imageFiles = UploadedFile::getInstances($model, 'cg_img');
            foreach ($imageFiles as $gallery) {
                $galleryModel = new CharityGallery();
                $galleryModel->load(Yii::$app->request->post());
                $galleryImageName = $galleryModel->cg_cid . '_' . $gallery->baseName . '.' . $gallery->extension;
                $galleryModel->cg_img = $galleryImageName;
                if ($galleryModel->save(false)) {
                    $gallery->saveAs('../theme/img/gallery/charity/'  . $galleryImageName);
                }
            }
            \Yii::$app->getSession()->setFlash('success', 'Slider Created successfully');
            return $this->redirect(['images', 'id' => $model->cg_cid]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CharityGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cg_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CharityGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDel($id)
    {
        $model = CharityGallery::find()->where(['cg_id' => $id])->one();
        $model->cg_status = 'Deleted';
        $model->save(false);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the CharityGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CharityGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CharityGallery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
