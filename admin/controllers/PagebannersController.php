<?php

namespace admin\controllers;

use Yii;
use admin\models\PageBanners;
use admin\models\PageBannersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;


/**
 * PagebannersController implements the CRUD actions for PageBanners model.
 */
class PagebannersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
{
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['index', 'create', 'update','view','delete','status'],
            'rules' => [
                [
                    'actions' =>  ['index', 'create', 'update','view','delete','status'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                // 'delete' => ['POST'],
            ],
        ],
    ];
}

    /**
     * Lists all PageBanners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageBannersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PageBanners model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PageBanners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PageBanners();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 'pb_image');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->pb_page_name);
                $model->pb_image = $title . '_' . $uploadedFile;
            }
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/page_banners/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                }
                \Yii::$app->getSession()->setFlash('success', 'Slider Created successfully');
                return $this->redirect(['view', 'id' => $model->pb_id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PageBanners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pb_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PageBanners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PageBanners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PageBanners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PageBanners::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
