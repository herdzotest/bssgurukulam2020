<?php

namespace admin\controllers;
 
use Yii;
use admin\models\VisitorComments;
use admin\models\VisitorCommentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * VisitorcommentsController implements the CRUD actions for VisitorComments model.
 */
class VisitorcommentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all VisitorComments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VisitorCommentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VisitorComments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VisitorComments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    public function actionCreate()
    {
        $model = new VisitorComments();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 'vc_image');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->vc_name);
                $model->vc_image = $title . '_' . $uploadedFile;
            }
            $model->vc_created_on = date("Y-m-d", strtotime($model->vc_created_on));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                }
                \Yii::$app->getSession()->setFlash('success', 'Visitor Comment Added successfully');
                return $this->redirect(['view', 'id' => $model->vc_id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VisitorComments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    { 
        $model = $this->findModel($id);
        $oldimage = $model->vc_image;
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 'vc_image');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->vc_name);
                $model->vc_image = $title . '_' . $uploadedFile;
            } else {
                $model->vc_image = $oldimage;
            }
            $model->vc_created_on = date("Y-m-d", strtotime($model->vc_created_on));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                    if ($oldimage != '') {
                        unlink('../theme/img/' . $oldimage);
                    }
                }
                \Yii::$app->getSession()->setFlash('success', 'Visitor Comment Updated successfully');
                return $this->redirect(['view', 'id' => $model->vc_id]);
            } else {
                return $this->redirect(['update', 'id' => $model->vc_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Deletes an existing VisitorComments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VisitorComments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VisitorComments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VisitorComments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
