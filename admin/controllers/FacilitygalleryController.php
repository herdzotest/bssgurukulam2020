<?php

namespace admin\controllers;

use admin\models\FacilityGallery;
use Yii;
use admin\models\FacilityGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;


/**
 * FaciltygalleryController implements the CRUD actions for FaciltyGallery model.
 */
class FacilitygalleryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FaciltyGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FacilityGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FacilityGallery model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionImages($id)
    {
        $galleryModel = FacilityGallery::find()->where(['fg_fid' => $id,'fg_status' => 'Active'])->all();
        return $this->render('view', [
            'model' => $galleryModel,
        ]);
    }

    /**
     * Creates a new FacilityGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FacilityGallery();

        if ($model->load(Yii::$app->request->post())) {
            $imageFiles = UploadedFile::getInstances($model, 'fg_img');
            foreach ($imageFiles as $gallery) {
                $galleryModel = new FacilityGallery();
                $galleryModel->load(Yii::$app->request->post());
                $galleryImageName = $galleryModel->fg_fid . '_' . $gallery->baseName . '.' . $gallery->extension;
                $galleryModel->fg_img = $galleryImageName;
                if ($galleryModel->save(false)) {
                    $gallery->saveAs('../theme/img/gallery/facility/'  . $galleryImageName);
                }
            }
            \Yii::$app->getSession()->setFlash('success', 'Slider Created successfully');
            return $this->redirect(['images', 'id' => $model->fg_fid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FacilityGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->fg_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FacilityGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = FacilityGallery::find()->where(['fg_fid' => $id])->all();
        foreach ($model as $images) {
            unlink('../theme/img/gallery/facility/' . $images['fg_img']);
        }
        if (FacilityGallery::deleteAll(['fg_fid' => $id])) {
            return $this->redirect(['index']);
        }
    }
    public function actionDel($id)
    {
        $model = FacilityGallery::find()->where(['fg_id' => $id])->one();
        $model->fg_status = 'Deleted';
        $model->save(false);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the FacilityGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FacilityGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FacilityGallery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
