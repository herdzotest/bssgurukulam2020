<?php

namespace admin\controllers;

use Yii;
use admin\models\CharityImages;
use admin\models\CharityimageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
/**
 * CharityimageController implements the CRUD actions for CharityImages model.
 */
class CharityimagesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CharityImages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CharityimageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CharityImages model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CharityImages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new CharityImages();
        $rndtime = time();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstances($model, 'art_image_path');
            if (isset($uploadedFile)){
                foreach ($uploadedFile as $file){
                    $model = new CharityImages();
                    $model->art_image_path = 'assets/arts_image/gallery/' . $rndtime . '_' . $file->name;
                    $model->arts_filename = $file->name;
                    $model->arts_id = $id;
                    $model->uploaded_by = Yii::$app->user->id;
                    $model->uploaded_on = date('Y-m-d H:m:s');
                    $file->saveAs('assets/arts_image/gallery/' . $rndtime . '_' . $file->name);
                    $model->save();
                }
                \Yii::$app->getSession()->setFlash('success', 'Image Added Successfully');
                return $this->redirect(['charityimages/create/'.$id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CharityImages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->art_image_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CharityImages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        \Yii::$app->getSession()->setFlash('danger', 'Image Deleted Successfully');
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the CharityImages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CharityImages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CharityImages::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
