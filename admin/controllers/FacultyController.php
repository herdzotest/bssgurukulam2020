<?php

namespace admin\controllers;

use Yii;
use admin\models\Faculty;
use admin\models\FacultySearch;
use admin\models\ResExperience;
use admin\models\ResoureQualification;
use admin\models\ResoureQualificationSearch;
use admin\models\ResproDetails;
use admin\models\UserImages;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * FacultyController implements the CRUD actions for Faculty model.
 */
class FacultyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all Faculty models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FacultySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faculty model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
            'professionalModel' => $this->professionalModel($id),
            'qualificationModel' => $this->qualificationModel($id),
            'experienceModel' => $this->experienceModel($id),
        ]);
    }

    /**
     * Creates a new Faculty model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Faculty();
        $profilePic = new UserImages();
        $profilePic->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $model->role = '20';
            $model->auth_key = Yii::$app->security->generateRandomString();
            $password = '@Faculty123';
            $model->password_hash = Yii::$app->security->generatePasswordHash($password);
            //$model->status = '9';
            $model->created_at = strtotime(date('Y-m-d H:i:s'));
            $model->updated_at = strtotime(date('Y-m-d H:i:s'));
            $uploadedFile = UploadedFile::getInstance($profilePic, 'pp_image');
            if (!empty($uploadedFile)) {
                $profileName = preg_replace('/\s+/', '', $model->first_name);
                $profileImg = $profileName . '_' . $uploadedFile;
            } else {
                $profileImg = 'user.png';
            }
            if ($model->save()) {
                if (!empty($profileImg)) {
                    $profilePic->pp_uid = $model->id;
                    $profilePic->pp_image = $profileImg;
                    $profilePic->pp_status = 'Active';
                    if ($profilePic->save(false)) {
                        if (!empty($uploadedFile)) {
                            $uploadedFile->saveAs('../theme/img/pro_images/faculty/' . $profileImg);
                        }
                    }
                }
                return $this->redirect(Yii::$app->urlManager->createUrl(['resprodetails/create/' . $model->id]));
                // return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'profilePic' => $profilePic,
                ]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'profilePic' => $profilePic,
        ]);
    }

    /**
     * Updates an existing Faculty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $profilePic = UserImages::find()->where(['pp_uid' => $id])->one();
        $old_image = $profilePic->pp_image;
        $uploadedFile = UploadedFile::getInstance($profilePic, 'pp_image');
        if (!empty($uploadedFile)) {
            $profileName = preg_replace('/\s+/', '', $model->first_name);
            $profileImg = $profileName . '_' . $uploadedFile;
        } else {
            $profileImg = $old_image;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!empty($profileImg)) {
                $profilePic->pp_uid = $id;
                $profilePic->pp_image = $profileImg;
                $profilePic->pp_status = 'Active';
                if ($profilePic->save(false)) {
                    if (!empty($uploadedFile)) {
                        $uploadedFile->saveAs('../theme/img/pro_images/faculty/' . $profileImg);
                    }
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'profilePic' => $profilePic,
        ]);
    }

    /**
     * Deletes an existing Faculty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function ProfessionalModel($id)
    {
        $professionalModel = ResproDetails::find()->where(['pd_res_id' => $id])->one();
        return $professionalModel;
    }

    public function QualificationModel($id)
    {
        $query = ResoureQualification::find()->where(['rq_rid' => $id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }

    public function experienceModel($id)
    {
        $query = ResExperience::find()->where(['re_rid' => $id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
    public function actionStatus($id)
    {
        $model = ResproDetails::find()->where(['pd_res_id' => $id])->one();
        if ($model->pd_status == 'Active') {
            $model->pd_status = 'Inactive';
        } else {
            $model->pd_status = 'Active';
        }
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Faculty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faculty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faculty::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
