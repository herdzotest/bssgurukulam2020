<?php

namespace admin\controllers;

use Yii;
use admin\models\Events;
use admin\models\EventsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    public function actionCreate()
    {
        $model = new Events();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 'ev_featuredImg');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->ev_title);
                $model->ev_featuredImg = $title . '_' . $uploadedFile;
            }
            $model->ev_date = date("Y-m-d", strtotime($model->ev_date));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/events/featuredImages/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                }
                \Yii::$app->getSession()->setFlash('success', 'Event Added successfully');
                return $this->redirect(['view', 'id' => $model->ev_id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldimage = $model->ev_featuredImg;
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 'ev_featuredImg');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->ev_title);
                $model->ev_featuredImg = $title . '_' . $uploadedFile;
            } else {
                $model->ev_featuredImg = $oldimage;
            }
            $model->ev_date = date("Y-m-d", strtotime($model->ev_date));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/events/featuredImages/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                    if ($oldimage != '') {
                        unlink('../theme/img/events/featuredImages/' . $oldimage);
                    }
                }
                \Yii::$app->getSession()->setFlash('success', 'Event Page Updated successfully');
                return $this->redirect(['view', 'id' => $model->ev_id]);
            } else {
                return $this->redirect(['update', 'id' => $model->ev_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }

    }


    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
