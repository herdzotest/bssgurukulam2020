<?php

namespace admin\controllers;

use admin\models\Events;
use Yii;
use admin\models\EventGallery;
use admin\models\EventGallerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * EventgalleryController implements the CRUD actions for EventGallery model.
 */
class EventgalleryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EventGallery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventGallerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EventGallery model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EventGallery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
     public function actionImages($id)
    {
        $galleryModel = EventGallery::find()->where(['eg_eid' => $id,'eg_status' => 'Active'])->all();
        $eventModel = Events::find()->where(['ev_id' => $id])->one();
        return $this->render('view', [
            'model' => $galleryModel,
            'eventModel' => $eventModel,
        ]);
    }

    public function actionCreate()
    {
        $model = new EventGallery();

        if ($model->load(Yii::$app->request->post())) {
            $imageFiles = UploadedFile::getInstances($model, 'eg_img');
            foreach ($imageFiles as $gallery) {
                $galleryModel = new EventGallery();
                $galleryModel->load(Yii::$app->request->post());
                $galleryImageName = $galleryModel->eg_eid . '_' . $gallery->baseName . '.' . $gallery->extension;
                $galleryModel->eg_img = $galleryImageName;
                if ($galleryModel->save(false)) {
                    $gallery->saveAs('../theme/img/gallery/events/'  . $galleryImageName);
                }
            }
            \Yii::$app->getSession()->setFlash('success', 'Slider Created successfully');
            return $this->redirect(['images', 'id' => $model->eg_eid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EventGallery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->eg_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EventGallery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDel($id)
    {
        $model = EventGallery::find()->where(['eg_id' => $id])->one();
        $model->eg_status = 'Deleted';
        $model->save(false);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the EventGallery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventGallery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventGallery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
