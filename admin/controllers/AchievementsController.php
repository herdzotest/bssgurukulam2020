<?php

namespace admin\controllers;

use Yii;
use admin\models\Achievements;
use admin\models\AchievementsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * AchievementsController implements the CRUD actions for Achievements model.
 */
class AchievementsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Achievements models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AchievementsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Achievements model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Achievements model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Achievements();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 'av_featuredImage');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->av_title);
                $model->av_featuredImage = $title . '_' . $uploadedFile;
            }
            $model->av_date = date("Y-m-d", strtotime($model->av_date));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/achievements/featuredImages/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                }
                \Yii::$app->getSession()->setFlash('success', 'Achievement Added successfully');
                return $this->redirect(['view', 'id' => $model->av_id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Achievements model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldimage = $model->av_featuredImage;
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 'av_featuredImage');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->av_title);
                $model->av_featuredImage = $title . '_' . $uploadedFile;
            } else {
                $model->av_featuredImage = $oldimage;
            }
            $model->av_date = date("Y-m-d", strtotime($model->av_date));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/achievements/featuredImages/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                    if ($oldimage != '') {
                        unlink('../theme/img/achievements/featuredImages/' . $oldimage);
                    }
                }
                \Yii::$app->getSession()->setFlash('success', 'Achievement Page Updated successfully');
                return $this->redirect(['view', 'id' => $model->av_id]);
            } else {
                return $this->redirect(['update', 'id' => $model->av_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Deletes an existing Achievements model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Achievements model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Achievements the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Achievements::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
