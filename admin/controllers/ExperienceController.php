<?php

namespace admin\controllers;

use Yii;
use admin\models\ResExperience;
use admin\models\ResExperienceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ExperienceController implements the CRUD actions for ResExperience model.
 */
class ExperienceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update', 'view', 'delete', 'status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update', 'view', 'delete', 'status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ResExperience models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ResExperienceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ResExperience model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ResExperience model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new ResExperience();

        if ($model->load(Yii::$app->request->post())) {
            $model->re_rid = $id;
            $model->re_start_date = date("Y-m-d", strtotime($model->re_start_date));
            $model->re_end_date = date("Y-m-d", strtotime($model->re_end_date));
            $model->save();
            return $this->redirect(['faculty/view', 'id' => $model->re_rid]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionAdd($id)
    {
        $model = new ResExperience();

        if ($model->load(Yii::$app->request->post())) {
            $model->re_rid = $id;
            $model->re_start_date = date("Y-m-d", strtotime($model->re_start_date));
            $model->re_end_date = date("Y-m-d", strtotime($model->re_end_date));
            $model->save();
            return $this->redirect(['faculty/view', 'id' => $model->re_rid]);
        }

        return $this->render('add', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ResExperience model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->re_start_date = date("Y-m-d", strtotime($model->re_start_date));
            $model->re_end_date = date("Y-m-d", strtotime($model->re_end_date));
            $model->save();
            return $this->redirect(['faculty/view', 'id' => $model->re_rid]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ResExperience model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->re_status = 'Inactive';
        $model->save(false);
        return $this->redirect(['faculty/view', 'id' => $model->re_rid]);
    }
    /**
     * Finds the ResExperience model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ResExperience the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ResExperience::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
