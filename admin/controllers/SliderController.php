<?php

namespace admin\controllers;

use Yii;
use admin\models\Slider;
use admin\models\SliderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;


/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'create', 'update','view','delete','status'],
                'rules' => [
                    [
                        'actions' =>  ['index', 'create', 'update','view','delete','status'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = 'admin';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slider(); 
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 's_image');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->s_title);
                $model->s_image = $title . '_' . $uploadedFile;
            }
            $now = date("Y-m-d h:i:s");
            $model->s_createdon = date("Y-m-d h:i:s", strtotime($now));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/main-slider/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                }
                \Yii::$app->getSession()->setFlash('success', 'Slider Created successfully');
                return $this->redirect(['view', 'id' => $model->s_id]);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldimage = $model->s_image;

        if ($model->load(Yii::$app->request->post())) {
            $uploadedFile = UploadedFile::getInstance($model, 's_image');
            if ($uploadedFile != '') {
                $title = preg_replace('/\s+/', '', $model->s_title);
                $model->s_image = $title . '_' . $uploadedFile;
            } else {
                $model->s_image = $oldimage;
            }
            $now = date("Y-m-d h:i:s");
            $model->s_createdon = date("Y-m-d h:i:s", strtotime($now));
            if ($model->save()) {
                if ($uploadedFile != '') {
                    $uploadedFile->saveAs('../theme/img/main-slider/' . $title . '_' . $uploadedFile->baseName . '.' . $uploadedFile->extension);
                    unlink('../theme/img/main-slider/' . $oldimage);
                }
                \Yii::$app->getSession()->setFlash('success', 'Service Page Updated successfully');
                return $this->redirect(['view', 'id' => $model->s_id]);
            } else {
                return $this->redirect(['update', 'id' => $model->s_id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionStatus($id)
    {
        $model = $this->findModel($id);
        if ($model->s_status == '0') {
            $model->s_status = '1';
        } else {
            $model->s_status = '0';
        }
        $model->save();
        return $this->redirect(['index']);
    }
    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
